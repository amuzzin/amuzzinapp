from fabric.api import *
import time

env.roledefs = {
    'alpha': ['amuzzin@alpha.amuzz.in'],
    'prod': ['amuzzin@ssh.amuzz.in']
} 

def set_rvm():
    run('rvm use 1.9.3')

def start_other_services():
    with cd('www'):
        run ('RAILS_ENV=production bundle exec rake ts:configure')
        run ('RAILS_ENV=production bundle exec rake ts:index')
        run ('RAILS_ENV=production bundle exec rake ts:restart')
        run ('RAILS_ENV=production script/delayed_job restart')
        #with settings(warn_only=True):
            #run ('/etc/init.d/redis_6379 stop')
        #run ('/etc/init.d/redis_6379 start')

def start_passenger():
    with cd('www'):
        run ('touch tmp/restart.txt')

def start_servers():
    start_other_services()
    start_passenger()

def stop_dev_servers():
    with cd('www'):
        with settings(warn_only=True):
            run ('rvmsudo thin stop')

def start_dev_servers():
    with cd('www'):
        run ('rvmsudo thin start -e production -p 80 -threaded -d')

def restart_forever():
    with cd('www/node.js'):
        with settings(warn_only=True):
            run ('./node_modules/streamline/bin/_node -c server._js')
            run ('./node_modules/streamline/bin/_node -c worker._js')
            run ('./node_modules/forever/bin/forever stop server.js')
            run ('./node_modules/forever/bin/forever stop worker.js')
            run ('./node_modules/forever/bin/forever start server.js')
            run ('./node_modules/forever/bin/forever start worker.js')

def run_migrations():
    with cd('www'):
        run ('bundle exec rake db:migrate RAILS_ENV="production"')

def run_bundle():
    with cd('www'):
        run ('bundle install')

def precompile_assets():
   with cd('www'):
       run ('bundle exec rake assets:precompile')
    
def load_assets_to_github_cdn():
  with cd('/home/amuzzin/repos/amuzzin.github.io'):
    with settings(warn_only=True):
      run('git rm -r assets')
    run('cp -r /home/amuzzin/www/public/assets/ .')
    with settings(warn_only=True):    
        run('git add assets')
        run('git commit -m "Updated assets to latest compilation. Deployment Complete."')
        run('git push origin master') 

def clean_up_assets():
  with cd('www'):
    run ('bundle exec rake tmp:cache:clear')
    run ('bundle exec rake assets:clean')

def recreate_repo(source):
    with cd('/home/amuzzin/repos'):
        run('rm -rf amuzzinapp')
        run('git clone git@bitbucket.org:%s/amuzzinapp.git' % source)
        with cd('amuzzinapp'):
            run ('bundle install')


def export_repo():
    run ('cp -R repos/amuzzinapp/* www/')
    run ('rm -rf www/.git')
    run ('ln -sf /home/amuzzin/audio_files/ www/public/audios')
    run ('ln -sf /home/amuzzin/carrierwave_uploads/ www/public/uploads')

def replace_string_in_file(file_path,old_line,new_line):
    config_file = open(file_path,'w+')
    content = config_file.read()
    new_content = content.replace(old_line,new_line)
    config_file.write(new_content)
    config_file.close()

def set_github_cdn():
    with cd('/home/amuzzin/www'):
        run('cp data/production.rb config/environments/')

def update_whenever():
    with cd('/home/amuzzin/www'):
        run('bundle exec whenever -w')

def deploy(source="amuzzin"):
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    set_rvm()
    export_repo()
    run_bundle()
    set_github_cdn()
    start_servers()
    restart_forever()

def quick_deploy(source="amuzzin"):
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    set_rvm()
    export_repo()
    run_migrations()
    set_github_cdn()
    start_passenger()
    restart_forever()

def deploy_rails(source="amuzzin"):    
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    set_rvm()
    export_repo()
    run_bundle()
    clean_up_assets()
    precompile_assets()
    load_assets_to_github_cdn()
    run_migrations()
    set_github_cdn()
    start_servers()
    restart_forever()

def deploy_nodejs(source="amuzzin"):
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    restart_forever()

# Only Used tasks are below
###########################
# 1. Production => full_deploy()
# 2. Stage => alpha_deploy()

def full_deploy(source="amuzzin"):
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    set_rvm()
    export_repo()
    run_bundle()
    clean_up_assets()
    precompile_assets()
    load_assets_to_github_cdn()
    run_migrations()
    set_github_cdn()
    update_whenever()
    start_servers()
    restart_forever()

def alpha_deploy(source="amuzzin"):
    with cd('repos/amuzzinapp'):
        with settings(warn_only=True):
            if run('git pull git@bitbucket.org:%s/amuzzinapp.git master' % source).failed:
                recreate_repo(source)
    set_rvm()
    export_repo()
    run_bundle()
    stop_dev_servers()
    clean_up_assets()
    precompile_assets()
    run_migrations()
    update_whenever()
    start_other_services()
    start_dev_servers()
    restart_forever()

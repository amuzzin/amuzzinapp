require 'test_helper'
require 'rails/performance_test_help'

class BrowsingTest < ActionDispatch::PerformanceTest
  # Refer to the documentation for all available options
  # self.profile_options = { :runs => 5, :metrics => [:wall_time, :memory]
  #                          :output => 'tmp/performance', :formats => [:flat] }

  def setup
    get '/users/sign_in' , :users => { :email => "dummy@gmail.com" , :password => "123456" }
  end

  def test_homepage
    get '/'
  end

  def test_login
    get '/users/sign_in' , :users => { :email => "dummy@gmail.com" , :password => "123456" }
    assert_response :success
  end

  def test_thinking_sphinx
    post '/utility/search_results' , :query => 'hillary'
    assert_response :success
  end
end

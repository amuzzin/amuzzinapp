require 'test_helper'

class SongMasterTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test " get real image for songs with art " do 
    with_art = song_masters(:with_image_song)
    assert_not_equal with_art.album_art , "default.png"
  end
end

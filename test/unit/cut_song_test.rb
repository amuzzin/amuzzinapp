require 'test_helper'

class CutSongTest < ActiveSupport::TestCase
  test " whether comments are fetched" do
    with_comment = cut_songs(:with_comment)
    assert_equal with_comment.comments,with_comment.get_comment
  end
end

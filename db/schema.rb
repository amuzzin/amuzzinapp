# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131215080355750) do

  create_table "access_tokens", :force => true do |t|
    t.string   "provider"
    t.string   "token"
    t.string   "secret"
    t.integer  "user_id"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "is_read",        :default => false
  end

  add_index "activities", ["is_read"], :name => "index_activities_on_is_read"
  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "chat_messages", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "comments", :force => true do |t|
    t.integer  "owner_id",         :null => false
    t.integer  "commentable_id",   :null => false
    t.string   "commentable_type", :null => false
    t.text     "body",             :null => false
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "cut_songs", :force => true do |t|
    t.integer  "start_duration"
    t.integer  "end_duration"
    t.integer  "uploaded_song_id"
    t.string   "path"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.text     "finger_print"
    t.integer  "duration"
    t.integer  "user_id"
    t.text     "comments"
    t.string   "url"
    t.integer  "smiles"
    t.string   "shared"
    t.text     "shortened_url"
  end

  add_index "cut_songs", ["url"], :name => "index_cut_songs_on_url"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "favourites", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "uploaded_song_id"
  end

  create_table "friendships", :force => true do |t|
    t.integer "friendable_id"
    t.integer "friend_id"
    t.integer "blocker_id"
    t.boolean "pending",       :default => true
  end

  add_index "friendships", ["friendable_id", "friend_id"], :name => "index_friendships_on_friendable_id_and_friend_id", :unique => true

  create_table "group_logs", :force => true do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.datetime "event_type"
    t.integer  "event_data_id"
    t.string   "event_data_type"
    t.datetime "log_time"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "group_members", :force => true do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.integer  "started_by_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "name"
  end

  create_table "oauth_access_grants", :force => true do |t|
    t.integer  "resource_owner_id", :null => false
    t.integer  "application_id",    :null => false
    t.string   "token",             :null => false
    t.integer  "expires_in",        :null => false
    t.text     "redirect_uri",      :null => false
    t.datetime "created_at",        :null => false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], :name => "index_oauth_access_grants_on_token", :unique => true

  create_table "oauth_access_tokens", :force => true do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             :null => false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        :null => false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], :name => "index_oauth_access_tokens_on_refresh_token", :unique => true
  add_index "oauth_access_tokens", ["resource_owner_id"], :name => "index_oauth_access_tokens_on_resource_owner_id"
  add_index "oauth_access_tokens", ["token"], :name => "index_oauth_access_tokens_on_token", :unique => true

  create_table "oauth_applications", :force => true do |t|
    t.string   "name",                         :null => false
    t.string   "uid",                          :null => false
    t.string   "secret",                       :null => false
    t.text     "redirect_uri",                 :null => false
    t.string   "scopes",       :default => "", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_applications", ["uid"], :name => "index_oauth_applications_on_uid", :unique => true

  create_table "playlist_songs", :force => true do |t|
    t.integer  "playlist_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "uploaded_song_id"
  end

  create_table "playlists", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "user_id"
    t.boolean  "public",     :default => false
    t.boolean  "delta",      :default => true,  :null => false
  end

  create_table "profile_song_histories", :force => true do |t|
    t.integer  "user_id"
    t.integer  "uploaded_song_id"
    t.text     "status"
    t.datetime "set_at"
    t.datetime "updated_at"
  end

  create_table "profile_songs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "uploaded_song_id"
    t.text     "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "rates", :force => true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         :null => false
    t.string   "dimension"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "rates", ["rateable_id", "rateable_type"], :name => "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], :name => "index_rates_on_rater_id"

  create_table "rating_caches", :force => true do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            :null => false
    t.integer  "qty",            :null => false
    t.string   "dimension"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], :name => "index_rating_caches_on_cacheable_id_and_cacheable_type"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "shared_cuts", :force => true do |t|
    t.integer  "cut_song_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "song_masters", :force => true do |t|
    t.string   "title"
    t.string   "artist"
    t.string   "album"
    t.integer  "track"
    t.integer  "year"
    t.string   "genre"
    t.text     "comment1"
    t.text     "comment2"
    t.text     "lyrics"
    t.string   "album_art"
    t.string   "file_name"
    t.string   "file_hash"
    t.text     "finger_print"
    t.integer  "duration"
    t.string   "language"
    t.integer  "bitrate"
    t.string   "type"
    t.integer  "size"
    t.string   "path"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
    t.integer  "rating"
    t.string   "video_url"
    t.string   "video_img"
    t.string   "youtube_url"
    t.string   "is_copyrighted"
  end

  create_table "uploaded_songs", :force => true do |t|
    t.text     "path"
    t.text     "album_art"
    t.integer  "duration"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.text     "finger_print"
    t.integer  "song_master_id"
    t.boolean  "youtube_mp3"
    t.integer  "user_id"
    t.string   "title"
    t.string   "artist"
    t.string   "album"
    t.integer  "track"
    t.integer  "year"
    t.string   "genre"
    t.string   "comment1"
    t.string   "comment2"
    t.text     "lyrics"
    t.string   "file_hash"
    t.string   "language"
    t.integer  "bitrate"
    t.string   "type"
    t.integer  "size"
    t.integer  "rating"
    t.string   "video_url"
    t.string   "video_img"
    t.string   "youtube_url"
    t.boolean  "delta",               :default => true,  :null => false
    t.string   "shortened_url"
    t.boolean  "soundcloud_mp3",      :default => false
    t.string   "soundcloud_url"
    t.string   "soundcloud_user_url"
    t.string   "soundcloud_art"
    t.string   "soundcloud_artist"
    t.string   "soundcloud_id"
    t.string   "image"
  end

  create_table "user_histories", :force => true do |t|
    t.integer  "uploaded_song_id"
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "count",            :default => 0
  end

  create_table "users", :force => true do |t|
    t.string   "email",                           :default => "",                    :null => false
    t.string   "encrypted_password",              :default => "",                    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                   :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.decimal  "lat"
    t.decimal  "lng"
    t.string   "city"
    t.string   "state"
    t.string   "country_code"
    t.string   "provider"
    t.string   "address"
    t.string   "name"
    t.datetime "last_seen",                       :default => '2016-10-14 23:41:53'
    t.boolean  "delta",                           :default => true,                  :null => false
    t.boolean  "enable_upload",                   :default => true
    t.integer  "song_count",                      :default => 0
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "image"
    t.string   "authentication_token"
    t.datetime "authentication_token_created_at"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end

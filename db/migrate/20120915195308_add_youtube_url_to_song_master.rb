class AddYoutubeUrlToSongMaster < ActiveRecord::Migration
  def change
    add_column :song_masters, :youtube_url, :string
  end
end

class AddLinksToPlaylist < ActiveRecord::Migration
  def change
    add_column :playlists, :song_master_id, :integer
    add_column :playlists, :user_id, :integer
  end
end

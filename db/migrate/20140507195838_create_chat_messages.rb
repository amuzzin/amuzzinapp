class CreateChatMessages < ActiveRecord::Migration
  def change
    create_table :chat_messages do |t|
      t.integer :sender_id
      t.integer :recipient_id
      t.string :recipient_type

      t.timestamps
    end
  end
end

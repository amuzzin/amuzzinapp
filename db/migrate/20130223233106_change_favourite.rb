class ChangeFavourite < ActiveRecord::Migration
  def up
    remove_column :favourites , :song_master_id
    add_column :favourites , :uploaded_song_id, :integer 
  end

  def down
    add_column :favourites, :song_master_id, :integer
    remove_column :favourites , :uploaded_song_id
  end
end

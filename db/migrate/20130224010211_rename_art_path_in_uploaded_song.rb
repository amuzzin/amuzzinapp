class RenameArtPathInUploadedSong < ActiveRecord::Migration
  def up
    rename_column :uploaded_songs,:art_path,:album_art
  end

  def down
    rename_column :uploaded_songs,:album_art,:art_path
  end
end

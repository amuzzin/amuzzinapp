class CreateSongMasters < ActiveRecord::Migration
  def change
    create_table :song_masters do |t|
      t.string :title
      t.string :artist
      t.string :album
      t.integer :track
      t.integer :year
      t.string :genre
      t.text :comment1
      t.text :comment2
      t.text :lyrics
      t.string :album_art
      t.string :file_name
      t.string :file_hash
      t.text :finger_print
      t.integer :duration
      t.string :language
      t.integer :bitrate
      t.string :type
      t.integer :size
      t.string :path
      t.timestamps
    end
  end
end

class AddingUrlCutSong < ActiveRecord::Migration
  def up
    add_column :cut_songs , :url , :string
    add_column :cut_songs , :smiles , :integer
    add_column :song_masters , :rating , :integer
    add_index(:cut_songs, :url)
  end

  def down
    drop_column :cut_songs , :url 
    drop_column :cut_songs , :smiles
    drop_column :song_masters , :rating 
  end
  
end

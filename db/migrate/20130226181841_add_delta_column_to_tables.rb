class AddDeltaColumnToTables < ActiveRecord::Migration
  def up
    add_column :uploaded_songs, :delta, :boolean, :default => true,:null => false
    add_column :playlists, :delta, :boolean, :default => true,:null => false
    add_column :users, :delta, :boolean, :default => true,:null => false
  end

  def down
    remove_column :uploaded_songs, :delta
    remove_column :playlists, :delta
    remove_column :users, :delta
  end
end

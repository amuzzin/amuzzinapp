class CreateUserHistories < ActiveRecord::Migration
  def change
    create_table :user_histories do |t|
      t.integer  :uploaded_song_id
      t.integer :user_id

      t.timestamps
    end
  end
end

class CreateSharedCuts < ActiveRecord::Migration
  def change
    create_table :shared_cuts do |t|
      t.integer :cut_song_id
      t.string :provider
      t.string :uid
      t.timestamps
    end
  end
end

class AddSoundcloudRelatedFields < ActiveRecord::Migration
  def up
  	add_column :uploaded_songs , :soundcloud_mp3 , :boolean , :default => false 
  	add_column :uploaded_songs , :soundcloud_url , :string 
  	add_column :uploaded_songs , :soundcloud_user_url , :string 
  	add_column :uploaded_songs , :soundcloud_art , :string 
  	add_column :uploaded_songs , :soundcloud_artist , :string 
    add_column :uploaded_songs , :soundcloud_id , :string 
	
  end

  def down
  	delete_column :uploaded_songs , :soundcloud_mp3  
  	delete_column :uploaded_songs , :soundcloud_url 
  	delete_column :uploaded_songs , :soundcloud_user_url 
  	delete_column :uploaded_songs , :soundcloud_art 
  	delete_column :uploaded_songs , :soundcloud_artist
    delete_column :uploaded_songs , :soundcloud_id 
  end
end

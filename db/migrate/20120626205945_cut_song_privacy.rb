class CutSongPrivacy < ActiveRecord::Migration
  def up
    add_column :cut_songs , :shared , :string
    
  end

  def down
    drop_column :cut_songs , :shared
  end
end

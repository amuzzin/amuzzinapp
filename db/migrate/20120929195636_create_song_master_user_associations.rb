class CreateSongMasterUserAssociations < ActiveRecord::Migration
  def self.up
    create_table :song_master_user_associations do |t|
      t.integer :user_id
      t.integer :song_master_id
      t.timestamps
    end
  end
  
  def self.down
    drop_table :song_master_user_associations
  end
end

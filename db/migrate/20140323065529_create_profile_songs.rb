class CreateProfileSongs < ActiveRecord::Migration
  def change
    create_table :profile_songs do |t|
      t.integer :user_id
      t.integer :uploaded_song_id
      t.text :status
      t.timestamps
    end
  end
end

class ChangePlaylistSong < ActiveRecord::Migration
  def up
    remove_column :playlist_songs , :song_master_id
    add_column :playlist_songs , :uploaded_song_id, :integer 
  end

  def down
    add_column :playlist_songs , :song_master_id, :integer
    remove_column :playlist_songs , :uploaded_song_id
  end
end

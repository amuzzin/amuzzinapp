class AddNameToUploadedSongs < ActiveRecord::Migration
  def change
    add_column :uploaded_songs, :name, :string
  end
end

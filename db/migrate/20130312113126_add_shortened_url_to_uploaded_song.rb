class AddShortenedUrlToUploadedSong < ActiveRecord::Migration
  def change
    add_column :uploaded_songs, :shortened_url, :string
  end
end

class UserGeoInfo < ActiveRecord::Migration
  def up
    add_column :users , :lat , :decimal
    add_column :users , :lng , :decimal
    add_column :users , :city , :string
    add_column :users , :state , :string
    add_column :users , :country_code , :string
    add_column :users , :provider , :string
    add_column :users , :address , :string
  end

  def down
    drop_column :users , :lat 
    drop_column :users , :lng
    drop_column :users , :city 
    drop_column :users , :state 
    drop_column :users , :country_code 
    drop_column :users , :provider 
    drop_column :users , :address
  end
end

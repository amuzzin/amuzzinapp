class AddUploadRestrictionForUser < ActiveRecord::Migration
  def up
    add_column :users , :enable_upload , :boolean , :default => true
    add_column :users , :song_count , :integer , :default => 0
  end

  def down
    remove_column :users , :song_limit
    remove_column :users , :enable_upload
  end
end

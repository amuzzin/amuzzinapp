class Addyoutubemp3ToUploadedSong < ActiveRecord::Migration
  def up
    add_column :uploaded_songs, :youtube_mp3, :boolean
  end

  def down
    remove_column :uploaded_songs, :youtube_mp3
  end
end

class AddLinkToCutSongUser < ActiveRecord::Migration
  def change
    add_column :cut_songs , :user_id , :integer
    add_column :song_masters , :user_id , :integer
  end
end

class RemoveSongMasterIdFromPlaylist < ActiveRecord::Migration
  def up
    remove_column :playlists , :song_master_id 
  end

  def down
    add_column :playlists, :song_master_id, :integer
  end
end

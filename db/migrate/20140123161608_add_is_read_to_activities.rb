class AddIsReadToActivities < ActiveRecord::Migration
  def change
    change_table :activities do |t|
      t.boolean :is_read, :default => false
    end
    add_index :activities, :is_read
  end
end

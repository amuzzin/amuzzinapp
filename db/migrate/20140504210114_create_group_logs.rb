class CreateGroupLogs < ActiveRecord::Migration
  def change
    create_table :group_logs do |t|
      t.integer :group_id
      t.integer :user_id
      t.datetime :event_type
      t.integer :event_data_id
      t.string :event_data_type
      t.datetime :log_time
      t.timestamps
    end
  end
end

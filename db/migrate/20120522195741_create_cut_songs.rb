class CreateCutSongs < ActiveRecord::Migration
  def change
    create_table :cut_songs do |t|
      t.integer :start_duration
      t.integer :end_duration
      t.integer :uploaded_song_id
      t.string :path
      t.timestamps
    end
  end
end

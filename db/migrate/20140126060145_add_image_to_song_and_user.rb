class AddImageToSongAndUser < ActiveRecord::Migration
  def change
    add_column :uploaded_songs , :image , :string , :default => nil 
    add_column :users , :image , :string , :default => nil 
  end
end

class CreateUploadedSongs < ActiveRecord::Migration
  def change
    create_table :uploaded_songs do |t|
      t.text :path
      t.text :art_path
      t.integer :duration
      t.timestamps
    end
  end
end

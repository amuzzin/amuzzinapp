class AddIsCopyrightedToSongMaster < ActiveRecord::Migration
  def up
    add_column :song_masters, :is_copyrighted, :string
  end
  
  def down
    remove_column :song_masters, :is_copyrighted
  end
end

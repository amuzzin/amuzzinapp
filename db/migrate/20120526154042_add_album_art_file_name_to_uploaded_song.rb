class AddAlbumArtFileNameToUploadedSong < ActiveRecord::Migration
  def change
    add_column :uploaded_songs , :album_art , :string
    add_column :uploaded_songs ,:file_name , :string
    add_column :uploaded_songs , :file_hash ,:string
  end
end

class RenameSongMasterPlaylistAssociationToPlaylistSongs < ActiveRecord::Migration
  def up
    rename_table :song_master_playlist_associations, :playlist_songs
  end

  def down
    rename_table :playlist_songs,:song_master_playlist_associations
  end
end

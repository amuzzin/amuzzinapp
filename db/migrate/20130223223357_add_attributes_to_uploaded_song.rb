class AddAttributesToUploadedSong < ActiveRecord::Migration
  def change
    add_column :uploaded_songs, :title, :string
    add_column :uploaded_songs, :artist, :string
    add_column :uploaded_songs, :album, :string
    add_column :uploaded_songs, :track, :integer
    add_column :uploaded_songs, :year, :integer
    add_column :uploaded_songs, :genre, :string
    add_column :uploaded_songs, :comment1, :string
    add_column :uploaded_songs, :comment2, :string
    add_column :uploaded_songs, :lyrics, :text
    add_column :uploaded_songs, :file_hash, :string
    add_column :uploaded_songs, :language, :string
    add_column :uploaded_songs, :bitrate, :integer
    add_column :uploaded_songs, :type, :string
    add_column :uploaded_songs, :size, :integer
    add_column :uploaded_songs, :rating, :integer
    add_column :uploaded_songs, :video_url, :string
    add_column :uploaded_songs, :video_img, :string
    add_column :uploaded_songs, :youtube_url, :string
  end
end

class CreateProfileSongHistories < ActiveRecord::Migration
  def change
    create_table :profile_song_histories do |t|
      t.integer :user_id
      t.integer :uploaded_song_id
      t.text :status
      t.datetime :set_at
      t.datetime :updated_at
    end
  end
end

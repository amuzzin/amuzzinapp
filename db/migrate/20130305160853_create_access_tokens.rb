class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.string :provider
      t.string :token
      t.string :secret
      t.integer :user_id
      t.string :uid
      t.timestamps
    end
  end
end

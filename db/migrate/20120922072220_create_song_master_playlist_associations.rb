class CreateSongMasterPlaylistAssociations < ActiveRecord::Migration
  def self.up
    create_table :song_master_playlist_associations do |t|
      t.integer :song_master_id
      t.integer :playlist_id

      t.timestamps
    end
  end
  
  def self.down
    drop_table :song_master_playlist_associations
  end
end

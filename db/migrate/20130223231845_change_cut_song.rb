class ChangeCutSong < ActiveRecord::Migration
  def up
    remove_column :cut_songs , :song_master_id 
  end

  def down
    add_column :cut_songs, :song_master_id, :integer
  end

end

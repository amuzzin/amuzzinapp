class AddVideoToSongMaster < ActiveRecord::Migration
  def change
    add_column :song_masters, :video_url, :string
    add_column :song_masters, :video_img, :string
  end
end

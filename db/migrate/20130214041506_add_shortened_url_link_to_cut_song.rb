class AddShortenedUrlLinkToCutSong < ActiveRecord::Migration
  def change
    add_column :cut_songs , :shortened_url , :text
  end
end

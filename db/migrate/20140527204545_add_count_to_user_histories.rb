class AddCountToUserHistories < ActiveRecord::Migration
  def change
    add_column :user_histories, :count, :integer, :default => 0
  end
end

class AddingUserIdToUploadedSongs < ActiveRecord::Migration
  def up
    add_column :uploaded_songs , :user_id , :integer
    
  end

  def down
    drop_column :uploaded_songs , :user_id
    
  end
end

class RenameSongMasterUserAssociationToFavourite < ActiveRecord::Migration
  def up
    rename_table :song_master_user_associations, :favourites
  end

  def down
    rename_table :favourites,:song_master_user_associations
  end
end

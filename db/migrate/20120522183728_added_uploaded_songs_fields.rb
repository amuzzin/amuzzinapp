class AddedUploadedSongsFields < ActiveRecord::Migration
  def up
    add_column :uploaded_songs , :title , :string #self.title = tag[0][:text]
    add_column :uploaded_songs , :artist , :string #self.artist = tag[1][:text]
    add_column :uploaded_songs , :album , :string #self.album = tag[2][:text]
    add_column :uploaded_songs , :track , :string #self.track = tag[3][:text]
    add_column :uploaded_songs , :year , :integer #self.year = tag[4][:text]
    add_column :uploaded_songs , :comment1 , :text #self.comment1 = tag[5][:text]
    add_column :uploaded_songs , :comment2 , :text #self.comment2 = tag[6][:text]
    add_column :uploaded_songs , :genre , :text #self.genre = tag[7][:text]
    add_column :uploaded_songs , :size , :integer 
  end

  def down
    remove_column :uploaded_songs , :title 
    remove_column :uploaded_songs , :artist 
    remove_column :uploaded_songs , :album 
    remove_column :uploaded_songs , :track
    remove_column :uploaded_songs ,:year 
    remove_column :uploaded_songs ,:comment1
    remove_column :uploaded_songs ,:comment2 
    remove_column :uploaded_songs ,:genre
    remove_column :uploaded_songs , :size 
  end
end

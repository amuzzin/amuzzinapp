class AddFingerPrintToSongs < ActiveRecord::Migration
  def self.up
    add_column :uploaded_songs , :finger_print , :text
  
    add_column :cut_songs , :finger_print , :text
    add_column :cut_songs , :duration , :integer
  end
  
  def self.down
    drop_column :uploaded_songs , :finger_print 
    
    drop_column :cut_songs , :finger_print 
    drop_column :cut_songs , :duration
  end
end

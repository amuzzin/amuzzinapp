amuzzin
======= 

Prologue
---------
     Amuzzin is a social music sharing platform for music lovers.
     We in amuzzin are striving hard everyday to bring
     the REAL music you love, always close to you, with your CLOSER ones.
 
Profile
-------
- Every user who signs into amuzzin either thru' amuzzin signup or thru' other services will have their own profile
- Current Users songs and Public playlists will be displayed their profile. 
- If you are searching for a friend then you can use Search bar at bottom of the player 

Player
------
- Player appears at the center top of the page and will always appear docked.
- You can either click on the buttons or press [space] for playing the song , [left] and [right] arrow to change prev and next song

Current Queue
-------------
- Current queues are temporary means to add all songs
- Once you like the compilation you can save it as a playlist from provided option
- You can also add a song to Playlist from My Songs 

Creations
--------- 
- Each song you upload are considered to be your creations
- Songs you upload directly or from youtube are considered to be yours
- You should own the rights to listen , share and do anything with that media. 
- Amuzzin owns complete rights to delete the song in case of Copyright infringement claimed from author 
- You can add lyrics , Album , Title if our automatic mp3 extraction of those metadate are incomplete


Friends Stream
-------------- 

- Friends stream allows you to view the songs, all your online friends are listening
- We explicitly made no suggest friend or probably your friend kind of feature
- This would increase the quality of music you listen, thinking of them
- If you really miss your friend you can search him from Search bar and send a Friend Stream request
- You can stream from your friends queue or add it for later listening

Feed
----

- News Feed are real way to stay updated with our friends, but facebook does it better
- amuzzin completely deals with Music and no bullshit 
- You can see all realtime interaction your friends does with the Music Here 

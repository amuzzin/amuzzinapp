/*** Generated by streamline 0.8.1 (callbacks) - DO NOT EDIT ***/ var __rt=require('streamline/lib/callbacks/runtime').runtime(__filename),__func=__rt.__func,__cb=__rt.__cb,__trap=__rt.__trap,__tryCatch=__rt.__tryCatch; (function main(_) { var kue, redis, jsyaml, config, r_client, Heap, heap;











































  function set_curr_song(data, _) { var prev_song, prev_song_count, s_count; var __frame = { name: "set_curr_song", line: 45 }; return __func(_, this, arguments, set_curr_song, 1, __frame, function __$set_curr_song() {

      console.log(("set_curr_song" + data));


      prev_song = data["prev_song"]; return (function __$set_curr_song(__then) {
        if ((prev_song != null)) {

          console.log(("reduce:" + prev_song));
          return r_client.hincrby("song_count", prev_song, -1, __cb(_, __frame, 9, 20, function ___(__0, __1) { prev_song_count = __1;
            return put_in_heap(prev_song, prev_song_count, "", __cb(_, __frame, 10, 2, __then, true)); }, true)); } else { __then(); } ; })(function __$set_curr_song() {


        return r_client.hincrby("song_count", data["song"], 1, __cb(_, __frame, 13, 11, function ___(__0, __2) { s_count = __2;
          console.log("setting current song count");
          return put_in_heap(data["song"], s_count, "", __cb(_, __frame, 15, 1, _, true)); }, true)); }); }); };


  function add_to_queue(data, _) { var q_count, songs, i, song; var __frame = { name: "add_to_queue", line: 63 }; return __func(_, this, arguments, add_to_queue, 1, __frame, function __$add_to_queue() {

      console.log(("add_to_queue" + data));


      songs = data["song"].split(",");
      i = 0; var __3 = false; return (function ___(__break) { var __more; var __loop = __cb(_, __frame, 0, 0, function __$add_to_queue() { __more = false; if (__3) { i++; } else { __3 = true; } ; var __2 = (i < songs.length); if (__2) {

            song = songs[i];
            return r_client.hincrby("queue_count", song, 1, __cb(_, __frame, 9, 12, function ___(__0, __1) { q_count = __1;
              console.log(heap);
              return put_in_heap(song, "", q_count, __cb(_, __frame, 11, 2, function __$add_to_queue() { while (__more) { __loop(); }; __more = true; }, true)); }, true)); } else { __break(); } ; }); do { __loop(); } while (__more); __more = true; })(_); }); };




  function clear_queue(data, user_queue, _) { var value, queue, i, s_count, q_count; var __frame = { name: "clear_queue", line: 79 }; return __func(_, this, arguments, clear_queue, 2, __frame, function __$clear_queue() {

      console.log(("clear_queue" + data));

      value = user_queue; return (function __$clear_queue(__then) {
        if (((value != null) && (value != ""))) {
          queue = value.toString().split(",");
          i = 0; var __6 = false; return (function ___(__break) { var __more; var __loop = __cb(_, __frame, 0, 0, function __$clear_queue() { __more = false; if (__6) { i++; } else { __6 = true; } ; var __5 = (i < queue.length); if (__5) {



                return r_client.hget("song_count", queue[i], __cb(_, __frame, 11, 18, function ___(__0, __1) { prev_s_count = __1;
                  console.log(prev_s_count); return (function __$clear_queue(__then) {
                    if (((prev_s_count != null) && (prev_s_count != 0))) {
                      return r_client.hincrby("song_count", queue[i], -1, __cb(_, __frame, 14, 14, function ___(__0, __2) { s_count = __2; __then(); }, true)); } else {

                      s_count = 0; __then(); } ; })(function __$clear_queue() {

                    return r_client.hget("queue_count", queue[i], __cb(_, __frame, 18, 18, function ___(__0, __3) { prev_q_count = __3; return (function __$clear_queue(__then) {
                        if (((prev_q_count != null) && (prev_q_count != 0))) {
                          return r_client.hincrby("queue_count", queue[i], -1, __cb(_, __frame, 20, 14, function ___(__0, __4) { q_count = __4; __then(); }, true)); } else {

                          q_count = 0; __then(); } ; })(function __$clear_queue() {
                        return put_in_heap(queue[i], s_count, q_count, __cb(_, __frame, 23, 3, function __$clear_queue() { while (__more) { __loop(); }; __more = true; }, true)); }); }, true)); }); }, true)); } else { __break(); } ; }); do { __loop(); } while (__more); __more = true; })(__then); } else { __then(); } ; })(_); }); };




  function remove_from_queue(data, queue_len, _) { var s_count, q_count; var __frame = { name: "remove_from_queue", line: 107 }; return __func(_, this, arguments, remove_from_queue, 2, __frame, function __$remove_from_queue() {

      console.log(("remove_from_song" + data));

      return r_client.hget("queue_count", data["song"], __cb(_, __frame, 4, 16, function ___(__0, __1) { prev_q_count = __1; return (function __$remove_from_queue(__then) {
          if (((prev_q_count != null) && (prev_q_count != 0))) {
            return r_client.hincrby("queue_count", data["song"], -1, __cb(_, __frame, 6, 12, function ___(__0, __2) { q_count = __2; __then(); }, true)); } else {

            q_count = 0; __then(); } ; })(function __$remove_from_queue() { return (function __$remove_from_queue(__then) {
            if ((queue_len == 0)) {


              return r_client.hget("song_count", data["song"], __cb(_, __frame, 12, 17, function ___(__0, __3) { prev_s_count = __3; return (function __$remove_from_queue(__then) {
                  if (((prev_s_count != null) && (prev_s_count != 0))) {
                    return r_client.hincrby("song_count", data["song"], -1, __cb(_, __frame, 14, 13, function ___(__0, __4) { s_count = __4; __then(); }, true)); } else {

                    s_count = 0; __then(); } ; })(function __$remove_from_queue() {
                  return put_in_heap(data["song"], s_count, q_count, __cb(_, __frame, 17, 2, __then, true)); }); }, true)); } else {



              return put_in_heap(data["song"], "", q_count, __cb(_, __frame, 21, 2, __then, true)); } ; })(_); }); }, true)); }); };




  function decide(song_object) {

    var min = heap.peek();
    if (((min.song_count < song_object.song_count) || (((min.song_count == song_object.song_count) && (min.queue_count < song_object.queue_count))))) {
      heap.replace(song_object); }; };


  function put_in_heap(data, song_count, queue_count, _) { var __frame = { name: "put_in_heap", line: 140 }; return __func(_, this, arguments, put_in_heap, 3, __frame, function __$put_in_heap() { return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$put_in_heap() { return (function __$put_in_heap(__then) {



              if ((song_count == "")) {
                return r_client.hget("song_count", data, __cb(_, __frame, 5, 15, function ___(__0, __1) { song_count = __1; __then(); }, true)); } else { __then(); } ; })(function __$put_in_heap() { return (function __$put_in_heap(__then) {
                if ((queue_count == "")) {
                  return r_client.hget("queue_count", data, __cb(_, __frame, 7, 16, function ___(__0, __2) { queue_count = __2; __then(); }, true)); } else { __then(); } ; })(function __$put_in_heap() {
                if ((song_count == null)) {
                  song_count = 0; } ;
                if ((queue_count == null)) {
                  queue_count = 0; } ;
                song_object = {

                  song: data,
                  song_count: song_count,
                  queue_count: queue_count };

                if (heap.contains(song_object)) {
                  heap.updateItem(song_object); } else {

                  (((heap.size() < 50)) ? heap.push(song_object) : decide(song_object)); } ;
                console.log("heap:");
                console.log(heap.nodes);
                return r_client.set("heap", JSON.stringify(heap.nodes), __cb(_, __frame, 24, 1, __then, true)); }); }); }); })(function ___(err, __result) { __tryCatch(_, function __$put_in_heap() { if (err) {



              console.log(err); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, _); }); }); };




  function log(type, msg) {


    var color = "[0m", reset = "[0m";

    switch (type) {
    case "info": color = "[36m";
      break;
    case "warn":
      color = "[33m";
      break;
    case "error":
      color = "[31m";
      break;
    case "msg":
      color = "[34m";
      break;
      default:
      color = "[0m";
    };


    console.log((((((color + "   ") + type) + "  - ") + reset) + msg)); };


  function set_heap(_) { var __frame = { name: "set_heap", line: 198 }; return __func(_, this, arguments, set_heap, 0, __frame, function __$set_heap() {


      heap = new Heap(function(a, b) {
        if ((a.song_count != b.song_count)) {
          return (a.song_count - b.song_count); } else {

          return (a.queue_count - b.queue_count) }; });

      return r_client.get("heap", __cb(_, __frame, 9, 17, function ___(__0, __1) { redis_heap = __1;
        if ((redis_heap != null)) {

          heap.nodes = JSON.parse(redis_heap); } ; _(); }, true)); }); }; var __frame = { name: "main", line: 1 }; return __func(_, this, arguments, main, 0, __frame, function __$main() { kue = require("kue"); redis = require("redis"); jsyaml = require("js-yaml"); config = require("../config/database.yml"); if ((process.argv[2] == "development")) { config = config["development"]; } else { config = config["production"]; } ; r_client = redis.createClient(config["redis_port"], "localhost"); r_client.debug_mode = true; kue.redis.createClient = function() { var client = redis.createClient(config["redis_port"], "localhost"); return client; }; jobs = kue.createQueue(); log("info", "connected to redis server"); Heap = require("heap"); return set_heap(__cb(_, __frame, 21, 2, function __$main() { jobs.process("count_job", function __1(job, done, _) { var __frame = { name: "__1", line: 24 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { console.log(("processing job: " + job.data.job_type)); return (function __$__1(__break) { switch (job.data.job_type) { case "set_curr_song": return set_curr_song(JSON.parse(job.data.info), __cb(_, __frame, 6, 2, __break, true)); case "add_to_queue": return add_to_queue(JSON.parse(job.data.info), __cb(_, __frame, 9, 2, __break, true)); case "remove_from_queue": return remove_from_queue(JSON.parse(job.data.info), job.data.queue, __cb(_, __frame, 12, 2, __break, true)); case "clear_queue": return clear_queue(JSON.parse(job.data.info), job.data.queue, __cb(_, __frame, 15, 2, __break, true)); default: return __break(); }; })(function __$__1() { done(); _(); }); }); }); _(); }, true)); });}).call(this, __trap);
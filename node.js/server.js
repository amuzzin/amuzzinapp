/*** Generated by streamline 0.8.1 (callbacks) - DO NOT EDIT ***/ var __rt=require('streamline/lib/callbacks/runtime').runtime(__filename),__func=__rt.__func,__cb=__rt.__cb,__tryCatch=__rt.__tryCatch; require("newrelic");





var express = require("express"), http = require("http"), server = http.createServer(app), jsyaml = require("js-yaml"), config = require("../config/database.yml"), child_process = require("child_process"), sscan = require("strscan").StringScanner;






var app = express();
if ((process.argv[2] == "development")) {
  config = config["development"];}
 else {
  config = config["production"];};


const redis = require("redis");
const r_client = redis.createClient(config["redis_port"], "localhost");
log("info", "connected to redis server");

var kue = require("kue");
kue.redis.createClient = function() {
  var client = redis.createClient(config["redis_port"], "localhost");
  return client;};

jobs = kue.createQueue();
kue.app.listen(3001);
kue.app.set("title", "Amuzzin-Count queue");

const io = require("socket.io")({
    "authorization": function(data, callback) {
      if (data.query) {

        data.sessionID = data.query.vtoken;

        r_client.hget("session", data.sessionID, function(err, session) {

          if ((err || !session)) {
            return callback("Unauthorized user", false); }


           else {

            data.session = JSON.parse(session);
            return callback(null, true); } ; }); }





       else {
        return callback("Unauthorized user", false); } ; } 
});



const PORT = config["socket_port"];
const HOST = config["socket_host"];

jobs.on("job complete", function(id) {
  kue.Job.get(id, function(err, job) {
    if (err) { return };
    job.remove(function(err) {
      if (err) { throw err };
      console.log("removed completed job #%d", job.id); }); });});







if (!module.parent) {

  server.listen(config["socket_port"], config["socket_host"]);
  const socket = io.listen(server);




  socket.on("connection", function(client) {
    const subscribe = redis.createClient(config["redis_port"], "localhost");
    user = "";
    username = "";
    profile_pic = "";
    channel = "";

    client.on("subscribe", function __1(data, _) { var __frame = { name: "__1", line: 91 }; return __func(_, this, arguments, __1, 1, __frame, function __$__1() {

        r_client.hget("session", data["vtoken"], function(err, session) {

          if ((err || !session)) {
            client.emit("error", "You are not authorized to subscribe"); }



           else {
            console.log(session);
            json_data = JSON.parse(session);
            console.log(json_data);
            user = json_data["user_id"];
            username = json_data["user_name"];
            profile_pic = json_data["user_icon"];
            channel = ("realtime_" + user);
            subscribe.subscribe(channel);
            if ((data["channel"].indexOf("realtime") != -1)) {
              setUserStatus(channel, username, "online", profile_pic); };
            subscribe.on("message", function(channel, message) {
              console.log(message);
              client.send(message); }); } ; }); _(); }); });











    client.on("message", function(msg) {
      log("debug", msg); });


    client.on("disconnect", function() {
      log("warn", "disconnecting from redis");
      subscribe.quit();
      console.log(user);
      if ((user != "")) {
        setUserStatus(channel, username, "offline", profile_pic); }; });


    client.on("invite_to_group", function __2(data, _) { var __frame = { name: "__2", line: 137 }; return __func(_, this, arguments, __2, 1, __frame, function __$__2() {
        invite_user = data["invite"];
        console.log("inviting user");
        console.log(invite_user);
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 141 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                json_data = JSON.parse(session);
                console.log(json_data);
                data["user_id"] = json_data["user_id"]; return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$__1() {


                      return invite_to_group(data["user_id"], invite_user, __cb(_, __frame, 13, 6, __then, true)); }); })(function ___(e, __result) { __tryCatch(_, function __$__1() { if (e) {



                        console.log(e); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, __then); }); } ; })(_); }); }); _(); }); });






    client.on("send_chat_to_group", function __3(data, _) { var __frame = { name: "__3", line: 165 }; return __func(_, this, arguments, __3, 1, __frame, function __$__3() {
        message = data["message"];
        console.log("sending message: ");
        console.log(message);
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 169 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                json_data = JSON.parse(session);
                console.log(json_data); return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$__1() {


                      return r_client.hget("user_group", json_data["user_id"], __cb(_, __frame, 12, 17, function ___(__0, __1) { group_id = __1;
                        add_to_log(((("<i>" + json_data["user_name"]) + " says: </i>") + message), group_id, "chat", json_data["user_id"]); __then(); }, true)); }); })(function ___(e, __result) { __tryCatch(_, function __$__1() { if (e) {



                        console.log(e); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, __then); }); } ; })(_); }); }); _(); }); });






    client.on("accept_group_invite", function __4(data, _) { var __frame = { name: "__4", line: 193 }; return __func(_, this, arguments, __4, 1, __frame, function __$__4() {
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 194 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                json_data = JSON.parse(session);
                console.log(json_data);
                data["user_id"] = json_data["user_id"];
                data["user_name"] = json_data["user_name"]; return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$__1() {


                      return join_group(data["user_id"], data["user_name"], __cb(_, __frame, 14, 6, __then, true)); }); })(function ___(e, __result) { __tryCatch(_, function __$__1() { if (e) {



                        console.log(e); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, __then); }); } ; })(_); }); }); _(); }); });






    client.on("leave_group", function __5(data, _) { var __frame = { name: "__5", line: 219 }; return __func(_, this, arguments, __5, 1, __frame, function __$__5() {
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 220 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                json_data = JSON.parse(session);
                console.log(json_data);
                data["user_id"] = json_data["user_id"];
                data["user_name"] = json_data["user_name"]; return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$__1() {


                      return leave_group(data["user_id"], data["user_name"], __cb(_, __frame, 14, 6, __then, true)); }); })(function ___(e, __result) { __tryCatch(_, function __$__1() { if (e) {



                        console.log(e); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, __then); }); } ; })(_); }); }); _(); }); });






    client.on("ignore_group_invite", function __6(data, _) { var __frame = { name: "__6", line: 245 }; return __func(_, this, arguments, __6, 1, __frame, function __$__6() {
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 246 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                json_data = JSON.parse(session);
                console.log(json_data);
                data["user_id"] = json_data["user_id"];
                data["user_name"] = json_data["user_name"]; return (function ___(__then) { (function ___(_) { __tryCatch(_, function __$__1() {


                      return ignore_group_request(data["user_id"], data["user_name"], __cb(_, __frame, 14, 6, __then, true)); }); })(function ___(e, __result) { __tryCatch(_, function __$__1() { if (e) {



                        console.log(e); __then(); } else { _(null, __result); } ; }); }); })(function ___() { __tryCatch(_, __then); }); } ; })(_); }); }); _(); }); });







    client.on("get_song_queue_count", function __7(data, _) { var __frame = { name: "__7", line: 272 }; return __func(_, this, arguments, __7, 1, __frame, function __$__7() {
        r_client.hget("session", data["token"], function __1(err, session, _) { var __frame = { name: "__1", line: 273 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() { return (function __$__1(__then) {

              if ((err || !session)) {

                return _(null, null); } else {



                return get_song_queue_count(data, __cb(_, __frame, 8, 13, function ___(__0, __1) { count = __1;

                  client.emit("message", JSON.stringify({
                    type: "song_queue_count",
                    song: data["song"],
                    song_count: count.song_count,
                    queue_count: count.queue_count })); __then(); }, true)); } ; })(_); }); }); _(); }); });







    client.on("set_curr_song", function __8(data, _) { var __frame = { name: "__8", line: 295 }; return __func(_, this, arguments, __8, 1, __frame, function __$__8() {

        console.log("In set current song");
        r_client.hget("session", data["token"], function __1(err, session, _) { var count, job; var __frame = { name: "__1", line: 298 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() {

            if ((err || !session)) {

              return _(null, null); }


             else {

              json_data = JSON.parse(session);
              console.log(json_data);
              data["user_id"] = json_data["user_id"]; 
              r_client.hset("current_song",data['user_id'], data['song']);
publishCurrSong(("realtime_"+json_data["user_id"]),data);

} ;


            return get_song_queue_count(data, __cb(_, __frame, 13, 16, function ___(__0, __1) { count = __1;

              client.emit("message", JSON.stringify({
                type: "song_queue_count",
                song: data["song"],
                song_count: (count.song_count + 1),
                queue_count: count.queue_count }));

              console.log(("id=" + data["user_id"]));
              if ((data["user_id"] != null)) {




                job = jobs.create("count_job", { info: JSON.stringify(data), job_type: "set_curr_song" }).attempts(5).save();
                console.log("created job"); } ; _(); }, true)); }); }); _(); }); });




    client.on("add_to_queue", function __9(data, _) { var __frame = { name: "__9", line: 331 }; return __func(_, this, arguments, __9, 1, __frame, function __$__9() {
        console.log("In add to queue");
        r_client.hget("session", data["token"], function __1(err, session, _) { var value; var __frame = { name: "__1", line: 333 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() {
             console.log(data);
console.log(session);
            if ((err || !session)) {

              return _(null, null); }


             else {
              json_data = JSON.parse(session);
              console.log(json_data);
              data["user_id"] = json_data["user_id"]; } ;



            console.log(("id=" + data["user_id"])); return (function __$__1(__then) {
              if ((data["user_id"] != null)) {

                return r_client.hget("current_queue", data["user_id"], __cb(_, __frame, 16, 13, function ___(__0, __2) { value = __2; return (function __$__1(_) {
                    var __1 = (((value == null) || (value == ""))); return (function __$__1(__then) { if (__1) { return r_client.hset("current_queue", data["user_id"], data["song"], __cb(_, __frame, 17, 38, _, true)); } else { __then(); } ; })(function __$__1() { return r_client.hset("current_queue", data["user_id"], ((value.toString() + ",") + data["song"]), __cb(_, __frame, 17, 103, _, true)); }); })(__cb(_, __frame, -332, 18, function __$__1() {
                    jobs.create("count_job", {
                      info: JSON.stringify(data),
                      job_type: "add_to_queue"
                    }).attempts(5).save();
                    console.log("created job"); __then(); }, true)); }, true)); } else { __then(); } ; })(_); }); }); _(); }); });




    client.on("remove_from_queue", function __10(data, _) { var __frame = { name: "__10", line: 360 }; return __func(_, this, arguments, __10, 1, __frame, function __$__10() {
        console.log("In remove from queue");
        r_client.hget("session", data["token"], function __1(err, session, _) { var value, queue; var __frame = { name: "__1", line: 362 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() {

            if ((err || !session)) {

              return _(null, null); }


             else {
              json_data = JSON.parse(session);
              console.log(json_data);
              data["user_id"] = json_data["user_id"]; } ;



            console.log(("id=" + data["user_id"])); return (function __$__1(__then) {
              if ((data["user_id"] != null)) {

                return r_client.hget("current_queue", data["user_id"], __cb(_, __frame, 17, 12, function ___(__0, __1) { value = __1; return (function __$__1(__then) {
                    if (((value != null) && (value != ""))) {

                      queue = value.toString().split(",");
                      queue.splice(queue.indexOf(data["song"]), 1); return (function __$__1(__then) {
                        if ((queue.length == 0)) {


                          return r_client.hdel("current_queue", data["user_id"], __cb(_, __frame, 25, 6, function __$__1() {


                            return r_client.hdel("current_song", data["user_id"], __cb(_, __frame, 28, 6, __then, true)); }, true)); } else {



                          return r_client.hset("current_queue", data["user_id"], queue.join(","), __cb(_, __frame, 32, 6, __then, true)); } ; })(__then); } else { __then(); } ; })(function __$__1() {


                    jobs.create("count_job", {
                      info: JSON.stringify(data),
                      job_type: "remove_from_queue",
                      queue: queue.length
                    }).attempts(5).save();
                    console.log("created job"); __then(); }); }, true)); } else { __then(); } ; })(_); }); }); _(); }); });




    client.on("clear_queue", function __11(data, _) { var __frame = { name: "__11", line: 407 }; return __func(_, this, arguments, __11, 1, __frame, function __$__11() {
        console.log("In clear queue");
        r_client.hget("session", data["token"], function __1(err, session, _) { var value; var __frame = { name: "__1", line: 409 }; return __func(_, this, arguments, __1, 2, __frame, function __$__1() {

            if ((err || !session)) {
              return _(null, null); }


             else {
              json_data = JSON.parse(session);
              console.log(json_data);
              data["user_email"] = json_data["user_email"];
              data["user_id"] = json_data["user_id"]; } ;



            console.log(("id=" + data["user_id"])); return (function __$__1(__then) {
              if ((data["user_id"] != null)) {

                return r_client.hget("current_queue", data["user_id"], __cb(_, __frame, 17, 12, function ___(__0, __1) { value = __1;

                  return r_client.hdel("current_queue", data["user_id"], __cb(_, __frame, 19, 4, function __$__1() {

                    return r_client.hdel("current_song", data["user_id"], __cb(_, __frame, 21, 4, function __$__1() {
                      jobs.create("count_job", {
                        info: JSON.stringify(data),
                        job_type: "clear_queue",
                        queue: value
                      }).attempts(5).save();
                      console.log("created job"); __then(); }, true)); }, true)); }, true)); } else { __then(); } ; })(_); }); }); _(); }); }); });};







function log(type, msg) {


  var color = "[0m", reset = "[0m";

  switch (type) {
  case "info": color = "[36m";
    break;
  case "warn":
    color = "[33m";
    break;
  case "error":
    color = "[31m";
    break;
  case "msg":
    color = "[34m";
    break;
    default:
    color = "[0m";
  };


  console.log((((((color + "   ") + type) + "  - ") + reset) + msg));};


function get_song_queue_count(data, _) { var s_count, q_count; var __frame = { name: "get_song_queue_count", line: 469 }; return __func(_, this, arguments, get_song_queue_count, 1, __frame, function __$get_song_queue_count() {


    return r_client.hget("song_count", data["song"], __cb(_, __frame, 3, 11, function ___(__0, __1) { s_count = __1;
      return r_client.hget("queue_count", data["song"], __cb(_, __frame, 4, 11, function ___(__0, __2) { q_count = __2;
        console.log(s_count);
        console.log(q_count);
        if ((s_count == null)) {
          s_count = 0; } ;
        if ((q_count == null)) {
          q_count = 0; } ;
        count = {
          song_count: s_count,
          queue_count: q_count };

        return _(null, count); }, true)); }, true)); });};



function setUserStatus(realtime_id, username, user_status, profile_pic) {

  redis_cli = redis.createClient(config["redis_port"], "localhost");

  user_id = realtime_id.split("realtime_")[1];

  redis_cli.pubsub("channels", function(error, stdout, stderr) {
    redis_cli.hget("friends", user_id, function(f_error, f_stdout, f_stderr) {
      friends = JSON.parse(f_stdout);
      if (((friends == null) || (friends == []))) {
        return };

      online_users = stdout;
      if (((online_users == null) || (online_users == []))) {
        return };
      for (var n in friends) {
        if (((online_users.indexOf(friends[n]) != -1) && (friends[n] != realtime_id))) {
          message = (((((((("{\"type\":\"" + user_status) + "\",\"name\" : \"") + username) + "\" , \"id\":") + user_id) + " , \"icon\": \"") + profile_pic) + "\"}");
          redis_cli.publish(friends[n], message); } ; }; }); });};


function publishCurrSong(realtime_id, data) {

  redis_cli = redis.createClient(config["redis_port"], "localhost");

  user_id = realtime_id.split("realtime_")[1];

  redis_cli.pubsub("channels", function(error, stdout, stderr) {
    redis_cli.hget("friends", user_id, function(f_error, f_stdout, f_stderr) {
      friends = JSON.parse(f_stdout);
      if (((friends == null) || (friends == []))) {
        return };

      online_users = stdout;
      if (((online_users == null) || (online_users == []))) {
        return };
      for (var n in friends) {
        if (((online_users.indexOf(friends[n]) != -1) && (friends[n] != realtime_id))) {
           data["user_id"] = parseInt(user_id)
           data["type"] = "song_change"
  	   message = data
          redis_cli.publish(friends[n], JSON.stringify(message)); } ; }; }); });};







function join_group(user_id, user_name, _) { var __frame = { name: "join_group", line: 513 }; return __func(_, this, arguments, join_group, 2, __frame, function __$join_group() {

    return get_invite_group(user_id, __cb(_, __frame, 2, 13, function ___(__0, __1) { group_id = __1; return (function __$join_group(__then) {
        if ((group_id != null)) {

          return clear_group_invite(user_id, __cb(_, __frame, 5, 3, function __$join_group() {
            return r_client.hset("user_group", user_id, group_id, __cb(_, __frame, 6, 3, function __$join_group() {
              return r_client.hget("group_members", group_id, __cb(_, __frame, 7, 15, function ___(__0, __2) { members = __2;
                if ((members != null)) {
                  members += ("," + user_id); } else {

                  members = user_id; } ;
                return r_client.hset("group_members", group_id, members, __cb(_, __frame, 12, 5, function __$join_group() {
                  return add_to_log((user_name + " joined the group"), group_id, "join", user_id, __cb(_, __frame, 13, 5, __then, true)); }, true)); }, true)); }, true)); }, true)); } else { __then(); } ; })(_); }, true)); });};



function ignore_group_request(user_id, user_name, _) { var __frame = { name: "ignore_group_request", line: 530 }; return __func(_, this, arguments, ignore_group_request, 2, __frame, function __$ignore_group_request() {

    return get_invite_group(user_id, __cb(_, __frame, 2, 12, function ___(__0, __2) { group_id = __2; return (function __$ignore_group_request(__then) {
        if ((group_id != null)) {


          return get_invited_by(user_id, __cb(_, __frame, 6, 15, function ___(__0, __3) { invited_by = __3;
            return clear_group_invite(user_id, __cb(_, __frame, 7, 2, function __$ignore_group_request() {
              redis_cli = redis.createClient(config["redis_port"], "localhost");
              redis_cli.pubsub("channels", function __1(error, stdout, stderr, _) { var __frame = { name: "__1", line: 539 }; return __func(_, this, arguments, __1, 3, __frame, function __$__1() {
                  redis_cli.publish(("realtime_" + invited_by), JSON.stringify({
                    type: "group_listen_request_ignored",
                    user_id: user_id,
                    user_name: user_name })); _(); }); }); __then(); }, true)); }, true)); } else { __then(); } ; })(_); }, true)); });};





function leave_group(user_id, user_name, _) { var __frame = { name: "leave_group", line: 549 }; return __func(_, this, arguments, leave_group, 2, __frame, function __$leave_group() {

    console.log(((user_id + " ") + user_name));
    return r_client.hget("user_group", user_id, __cb(_, __frame, 3, 13, function ___(__0, __1) { group_id = __1;
      return r_client.hdel("user_group", user_id, __cb(_, __frame, 4, 4, function __$leave_group() {
        return r_client.hget("group_members", group_id, __cb(_, __frame, 5, 14, function ___(__0, __2) { members = __2;
          console.log(members); return (function __$leave_group(__then) {
            if ((members != null)) {

              members = members.split(",");
              members.splice(members.indexOf((user_id + "")), 1);
              return r_client.hset("group_members", group_id, members.join(","), __cb(_, __frame, 11, 6, function __$leave_group() {
                return add_to_log((user_name + " left the group"), group_id, "left", user_id, __cb(_, __frame, 12, 6, __then, true)); }, true)); } else { __then(); } ; })(_); }, true)); }, true)); }, true)); });};



function invite_to_group(user_id, invite_user, _) { var __frame = { name: "invite_to_group", line: 565 }; return __func(_, this, arguments, invite_to_group, 2, __frame, function __$invite_to_group() {

    console.log(((user_id + " ") + invite_user));
    return r_client.hget("user_group", user_id, __cb(_, __frame, 3, 13, function ___(__0, __2) { group_id = __2;
      return r_client.hget("user_group", invite_user, __cb(_, __frame, 4, 22, function ___(__0, __3) { invite_user_group = __3;

        console.log(((group_id + " ") + invite_user_group)); return (function __$invite_to_group(__then) {
          if ((group_id != null)) {

            return set_group_invite(user_id, invite_user, group_id, __cb(_, __frame, 9, 3, function __$invite_to_group() {
              redis_cli = redis.createClient(config["redis_port"], "localhost");
              redis_cli.pubsub("channels", function __1(error, stdout, stderr, _) { var __frame = { name: "__1", line: 576 }; return __func(_, this, arguments, __1, 3, __frame, function __$__1() {
                  redis_cli.publish(("realtime_" + invite_user), JSON.stringify({
                    type: "group_listen_request",
                    group_id: group_id,
                    user_id: user_id })); _(); }); }); __then(); }, true)); } else { __then(); } ; })(_); }, true)); }, true)); });};





function add_to_log(msg, group_id, activity, user, _) { var __frame = { name: "add_to_log", line: 586 }; return __func(_, this, arguments, add_to_log, 4, __frame, function __$add_to_log() {

    return r_client.hget("group_log", group_id, __cb(_, __frame, 2, 8, function ___(__0, __2) { log = __2;
      log = (log ? (log + msg) : msg);
      return r_client.hset("group_log", group_id, (log + "<br/>"), __cb(_, __frame, 4, 5, function __$add_to_log() {
        redis_cli = redis.createClient(config["redis_port"], "localhost");
        redis_cli.pubsub("channels", function __1(error, stdout, stderr, _) { var i; var __frame = { name: "__1", line: 592 }; return __func(_, this, arguments, __1, 3, __frame, function __$__1() {
            return r_client.hget("group_members", group_id, __cb(_, __frame, 1, 13, function ___(__0, __1) { members = __1;
              if ((members != null)) {

                members = members.split(",");
                for (i = 0; (i < members.length); i++) {

                  redis_cli.publish(("realtime_" + members[i]), JSON.stringify({
                    type: "group_log",
                    log_type: activity,
                    user_id: user,
                    log: (msg + "<br/>") })); }; } ; _(); }, true)); }); }); _(); }, true)); }, true)); });};







function set_group_invite(invited_by, invited, group, _) { var __frame = { name: "set_group_invite", line: 611 }; return __func(_, this, arguments, set_group_invite, 3, __frame, function __$set_group_invite() {

    return r_client.hset("invite_group", invited, group, __cb(_, __frame, 2, 1, function __$set_group_invite() {
      return r_client.hset("invited_by", invited, invited_by, __cb(_, __frame, 3, 1, _, true)); }, true)); });};


function get_invite_group(user, _) { var __frame = { name: "get_invite_group", line: 617 }; return __func(_, this, arguments, get_invite_group, 1, __frame, function __$get_invite_group() {

    return r_client.hget("invite_group", user, __cb(_, __frame, 2, 8, _, true)); });};


function get_invited_by(user, _) { var __frame = { name: "get_invited_by", line: 622 }; return __func(_, this, arguments, get_invited_by, 1, __frame, function __$get_invited_by() {

    return r_client.hget("invited_by", user, __cb(_, __frame, 2, 8, _, true)); });};


function clear_group_invite(user, _) { var __frame = { name: "clear_group_invite", line: 627 }; return __func(_, this, arguments, clear_group_invite, 1, __frame, function __$clear_group_invite() {

    return r_client.hdel("invite_group", user, __cb(_, __frame, 2, 1, function __$clear_group_invite() {
      return r_client.hdel("invited_by", user, __cb(_, __frame, 3, 1, _, true)); }, true)); });};

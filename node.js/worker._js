  var kue = require('kue'),
  redis = require('redis'),
  jsyaml = require('js-yaml'), 
  config = require('../config/database.yml');
  if (process.argv[2] == "development") {
 	config = config["development"];
  } else {
 	config = config["production"];
  }
  var r_client = redis.createClient(config["redis_port"],'localhost');
  r_client.debug_mode = true;
  kue.redis.createClient = function() {
    var client = redis.createClient(config["redis_port"], 'localhost');
    return client;
  };
  jobs = kue.createQueue();
  log('info', 'connected to redis server');
  var Heap = require('heap');
  // Minheap to get popular songs
  var heap;

  set_heap(_);
  
  jobs.process('count_job', function(job, done,_)
  {
  	console.log("processing job: "+job.data.job_type);
	switch(job.data.job_type)
	{
	case 'set_curr_song':
		set_curr_song(JSON.parse(job.data.info),_);
		break;
	case 'add_to_queue':
		add_to_queue(JSON.parse(job.data.info),_);
		break;
	case 'remove_from_queue':
		remove_from_queue(JSON.parse(job.data.info),job.data.queue,_);
		break;
	case 'clear_queue':
		clear_queue(JSON.parse(job.data.info),job.data.queue,_);
		break;	
	}
  	done();
  });
  
function set_curr_song(data,_)
{
	console.log('set_curr_song'+data);
	var prev_song,prev_song_count,s_count;
	// get previous song and reduce its song count
	prev_song = data['prev_song'];
	if (prev_song != null) 
	{
		console.log("reduce:"+prev_song);
		prev_song_count = r_client.hincrby("song_count",prev_song,-1,_);
		put_in_heap(prev_song, prev_song_count, '',_);
	}
	//increment current song's count
	s_count = r_client.hincrby("song_count",data['song'],1,_);
	console.log("setting current song count");
	put_in_heap(data['song'], s_count, '',_);
}

function add_to_queue(data,_)
{
	console.log('add_to_queue'+data);
	var q_count;
	//Increment queue count of song
	var songs = data['song'].split(',');
	for ( var i = 0; i < songs.length; i++) 
	{
		var song = songs[i];
		q_count = r_client.hincrby("queue_count",song,1,_);
		console.log(heap);
		put_in_heap(song, '', q_count,_);
	}
			
}

function clear_queue(data,user_queue,_)
{
	console.log('clear_queue'+data);	
	var value;
	value = user_queue;
	if (value != null && value != '') {
		var queue = value.toString().split(',');
		for ( var i = 0; i < queue.length; i++) 
		{
			var s_count,q_count;
			//decrement song count
			prev_s_count = r_client.hget("song_count",queue[i],_)
			console.log(prev_s_count);
			if(prev_s_count!=null && prev_s_count !=0)
				s_count = r_client.hincrby("song_count",queue[i], -1, _);
			else
				s_count = 0;  						
			//decrement queue count
			prev_q_count = r_client.hget("queue_count",queue[i],_)
			if(prev_q_count!=null && prev_q_count !=0)
				q_count = r_client.hincrby("queue_count",queue[i], -1,_);
			else
				q_count = 0; 
			put_in_heap(queue[i], s_count, q_count,_);
		}
	}
}

function remove_from_queue(data,queue_len,_)
{
	console.log('remove_from_song'+data);
	var s_count,q_count;
	prev_q_count = r_client.hget("queue_count",data['song'],_)
	if(prev_q_count!=null && prev_q_count !=0)
		q_count = r_client.hincrby("queue_count",data['song'], -1, _);
	else
		q_count = 0; 
	if (queue_len == 0) 
	{
		//Reduce the current song's count as there wont be a call made to set curr song
		prev_s_count = r_client.hget("song_count",data['song'],_)
		if(prev_s_count!=null && prev_s_count !=0)
			s_count = r_client.hincrby("song_count",data['song'], -1, _);
		else
			s_count = 0; 
		put_in_heap(data['song'], s_count, q_count,_);
	}
	else
	{
		put_in_heap(data['song'], '', q_count,_);
	}
			
}

function decide(song_object)
{
	var min = heap.peek();
	if(min.song_count<song_object.song_count || (min.song_count==song_object.song_count && min.queue_count<song_object.queue_count ))
	heap.replace(song_object);
}

function put_in_heap(data, song_count, queue_count,_) 
{
	try
	{
	if(song_count == '')
		song_count = r_client.hget("song_count",data, _);
	if(queue_count == '')
		queue_count = r_client.hget("queue_count",data, _);
	if(song_count==null)
		song_count = 0;
	if(queue_count==null)
		queue_count = 0;
	song_object = 
	{
		song : data,
		song_count : song_count,
		queue_count : queue_count
	};
	if(heap.contains(song_object))
		heap.updateItem(song_object);
	else
		(heap.size() < 50) ? heap.push(song_object) : decide(song_object);
	console.log("heap:");
	console.log(heap.nodes);
	r_client.set("heap", JSON.stringify(heap.nodes), _);
	}
	catch(err)
	{
		console.log(err);
	}

}

function log(type, msg) 
{

	var color = '\u001b[0m', reset = '\u001b[0m';

	switch(type) {
		case "info":
		color = '\u001b[36m';
		break;
		case "warn":
		color = '\u001b[33m';
		break;
		case "error":
		color = '\u001b[31m';
		break;
		case "msg":
		color = '\u001b[34m';
		break;
		default:
		color = '\u001b[0m'
	}

	console.log(color + '   ' + type + '  - ' + reset + msg);
}

function set_heap(_)
{
	// Check if priority queue is already created else create one
    heap = new Heap(function(a, b) {
		if (a.song_count != b.song_count)
			return a.song_count - b.song_count;
		else
			return a.queue_count - b.queue_count;
    });
    redis_heap = r_client.get('heap',_);
  	if(redis_heap!=null)
	{
		heap.nodes = JSON.parse(redis_heap);
	}
}

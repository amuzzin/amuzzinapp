/*
 Node.js server script
 Required node packages: express, redis, socket.io, priorityqueuejs
 */

 require('newrelic');
 var express = require('express'), 
 http = require('http'), 
 server = http.createServer(app), 
 jsyaml = require('js-yaml'), 
 pg = require('pg'), 
 config = require('../config/database.yml'),
 child_process = require('child_process'),
 cookie = require('cookie'), 
 sscan = require("strscan").StringScanner,
 html_strip = require('htmlstrip-native');

 var strip_options = {
        include_script : false,
        include_style : false,
        compact_whitespace : true
   	};
var clone = require('node-v8-clone').clone;

 var app = express();
 if (process.argv[2] == "development") {
 	config = config["development"];
 } else {
 	config = config["production"];
 }
 //pg
 var pg = require('pg');
 var pgString = "postgres://" + config["username"] + ":" + config["password"] + "@localhost/" + config["database"];
 //rpsc
 var rPubSub = require('rpsc');
 //redis
 const redis = require('redis');
 const r_client = redis.createClient(config["redis_port"],'localhost');
 const r_sub_client = redis.createClient(config["redis_port"],'localhost');
 const r_pub_client = redis.createClient(config["redis_port"],'localhost');
 rPubSub.init(r_sub_client);
 node_log('info', 'connected to redis server');
 //kue
 var kue = require('kue');
 kue.redis.createClient = function() {
    var client = redis.createClient(config["redis_port"], 'localhost');
    return client;
 };
 jobs = kue.createQueue();
 kue.app.listen(3001);
 kue.app.set('title', 'Amuzzin-Count queue');

 const io = require('socket.io');

 const PORT = config["socket_port"];
 const HOST = config["socket_host"];

 jobs.on('job complete', function(id){
	kue.Job.get(id, function(err, job){
		if (err) return;
		job.remove(function(err){
			if (err) throw err;
			console.log('removed completed job #%d', job.id);
		});
	});
 });




if (!module.parent) 
{
	server.listen(config["socket_port"], config["socket_host"]);
	const socket = io.listen(server);
 	socket.configure(function (){
 		socket.set('authorization', function (data, callback) {
	 		if (data.query) 
	 		{
	 			data.session_id = data.query.vtoken;
	     		// retrieve session from redis using the unique key stored in cookies
	     		r_client.hget("session", data.session_id, function (err, session) 
	     		{	    			
	     			if (err || !session) {
	     				return callback('Unauthorized user', false);
	     			} 
	     			else 
	     			{
	           		// store session data in nodejs server for later use
	           		data.session = JSON.parse(session);
	           		data.session['channel'] = 'realtime_' + data.session['user_id'];
	           		return callback(null, true);
	           		}
	          	});
	    		
	     	} 
	     	else 
	     	{
	     		return callback('Unauthorized user', false);
	     	}
     });
 });


	socket.on('connection', function(client) {
response.setHeader("Access-Control-Allow-Origin", '*');

		var subscribe = rPubSub.createClient();
		console.log(client.handshake.session);
		client.on('subscribe', function(data,_) {
			channel = client.handshake.session['channel'];
        	subscribe.subscribe(channel);
            if (data["channel"].indexOf("realtime") != -1)
            	setUserStatus(client.handshake.session,"online");
            subscribe.onMessage(function(channel, message){
        		console.log(message);
                client.send(message);
    		});		

		}); 


		client.on('message', function(msg) {
			node_log('debug', msg);
		});

		client.on('disconnect', function() {
			try
			{
			node_log('warn', 'disconnecting from redis');
			channel = client.handshake.session['channel'];
			subscribe.unsubscribe(channel);
			subscribe.end();
			//leave_group(client.handshake.session);	
			setUserStatus(client.handshake.session,"offline");
			}
			catch(e)
			{
				console.log(e);
			}
		});


		client.on('send_chat_to_group', function(data, _) {
			message = data["message"];
			message = html_strip.html_strip(message,strip_options);
			if(message.replace(/\s/g, '').length)
			{
				console.log("sending message: ");
				console.log(message);
				try
				{
					group_id = r_client.hget("user_group",client.handshake.session['user_id'],_);
					if(group_id!=null)
					{
						msg = { 'user' : client.handshake.session,
				  				'type' : 'chat',
				  				'message' : message,
				  				'group' : group_id
				  		  	  }
						add_to_log(msg,group_id,_);
					}
				}
				catch(e)
				{
					console.log(e);
				}
			}

		});
		

		client.on('get_song_queue_count', function(data, _) {
			count = get_song_queue_count(data,_);
				
			client.emit('message', JSON.stringify({
				"type" : "song_queue_count",
				"song" : data['song'],
				"song_count" : count.song_count,
				"queue_count" : count.queue_count
			}));
		}); 


		client.on('set_curr_song', function(data,_) {	

			console.log('In set current song');
			var count;
			if(data['group_listen'])
			{
				group = r_client.hget("user_group",client.handshake.session['user_id'],_);
				if(group!=null)
				{
					r_client.hset("group_song",group, data['song'], _);
					count = get_song_queue_count(data,_);
					msg = { 'user' : client.handshake.session,
		  					'type' : 'set_curr_song',
		  					'song' : data['song'],
		  					'song_name': data['song_name'],
		  					'group' : group
		  	 			  	  }
					add_to_log(msg,group,_);
				}
			}
			else
			{
				prev_song = r_client.hget("current_song",client.handshake.session['user_id'],_);
				data['prev_song'] = prev_song;
				r_client.hset("current_song",client.handshake.session['user_id'], data['song'], _);
				count = get_song_queue_count(data,_);
				var job = jobs.create('count_job', {
					info: JSON.stringify(data) ,
					job_type : 'set_curr_song',
				}).attempts(5).save();
				console.log('created job');
			}
			if(count != null)
			{	
				client.emit('message', JSON.stringify({
					"type" : "song_queue_count",
					"song" : data['song'],
					"song_count" : count.song_count+1,
					"queue_count" : count.queue_count
				}));
			}
		});

		client.on('add_to_queue', function(data,_) {
			console.log('In add to queue');
			var value;
			if(data['group_listen'])
			{
				group = r_client.hget("user_group",client.handshake.session['user_id'],_);
				if(group!=null)
				{
					value = r_client.hget("group_queue",group,_);
					(value == null || value == '') ? r_client.hset("group_queue",group, data['song'],_) : r_client.hset("group_queue",group, value.toString() + ',' + data['song'],_); 
					msg = { 'user' : client.handshake.session,
		  					'type' : 'add_to_queue',
		  					'song' : data['song'],
		  					'group' : group
		  	 			  	  }
					add_to_log(msg,group,_);
				}

			}
			else
			{
				value = r_client.hget("current_queue",client.handshake.session['user_id'],_);
				(value == null || value == '') ? r_client.hset("current_queue",client.handshake.session['user_id'], data['song'],_) : r_client.hset("current_queue",client.handshake.session['user_id'], value.toString() + ',' + data['song'],_); 
				jobs.create('count_job', {
					info: JSON.stringify(data) ,
					job_type : 'add_to_queue'
				}).attempts(5).save();
				console.log('created job');

			}
		});

		client.on('remove_from_queue', function(data,_) {
			console.log('In remove from queue');
			var value,queue;
			if(data['group_listen'])
			{
				group = r_client.hget("user_group",client.handshake.session['user_id'],_);
				if(group!=null)
				{
					value = r_client.hget("group_queue",group,_);
					queue = value.toString().split(',');
					queue.splice(queue.indexOf(data['song']), 1);	
					if (queue.length == 0) 
					{
						//delete queue if it is empty
						r_client.hdel("group_queue",group, _);
						// delete the current song also if the queue is empty as there won't be a call made to set curr song
						r_client.hdel("group_song",group, _);			
					}
					else
						r_client.hset("group_queue",group, queue.join(','), _);
					msg = { 'user' : client.handshake.session,
		  					'type' : 'remove_from_queue',
		  					'song' : data['song'],
		  					'song_name': data['song_name'],
		  					'group' : group
		  		  	  	  }
					add_to_log(msg,group,_);
				}

			}
			else
			{
				value = r_client.hget("current_queue",client.handshake.session['user_id'],_);
				if (value != null && value != '') 
				{
					queue = value.toString().split(',');
					queue.splice(queue.indexOf(data['song']), 1);	
					if (queue.length == 0) 
					{
						//delete queue if it is empty
						r_client.hdel("current_queue",client.handshake.session['user_id'], _);
						// delete the current song also if the queue is empty as there won't be a call made
						//to set curr song
						r_client.hdel("current_song",client.handshake.session['user_id'], _);
						
					}
					else
						r_client.hset("current_queue",client.handshake.session['user_id'], queue.join(','), _);
				}
				jobs.create('count_job', {
					info: JSON.stringify(data) ,
					job_type : 'remove_from_queue' ,
					queue : queue.length
				}).attempts(5).save();
				console.log('created job');
				}
		});

		client.on('clear_queue', function(data,_) {	
			console.log('In clear queue');
			var value;
			if(data['group_listen'])
			{
				group = r_client.hget("user_group",client.handshake.session['user_id'],_);
				if(group!=null)
				{
					value = r_client.hget("group_queue",group,_);
					//delete group queue
					r_client.hdel("group_queue",group, _);
					//delete group song
					r_client.hdel("group_song",group, _);
					msg = { 'user' : client.handshake.session,
							'type' : 'clear_queue',
							'group' : group
			 	  	  	  }
					add_to_log(msg,group,_);
				}
			}
			
			else
			{
				value = r_client.hget("current_queue",client.handshake.session['user_id'],_);
				//delete queue
				r_client.hdel("current_queue",client.handshake.session['user_id'], _);
				//delete current song
				r_client.hdel("current_song",client.handshake.session['user_id'], _);
				jobs.create('count_job', {
					info: JSON.stringify(data) ,
					job_type : 'clear_queue' ,
					queue : value
				}).attempts(5).save();
				console.log('created job');
			}
		});
	});


}
function node_log(type, msg) 
{

	var color = '\u001b[0m', reset = '\u001b[0m';

	switch(type) {
		case "info":
		color = '\u001b[36m';
		break;
		case "warn":
		color = '\u001b[33m';
		break;
		case "error":
		color = '\u001b[31m';
		break;
		case "msg":
		color = '\u001b[34m';
		break;
		default:
		color = '\u001b[0m'
	}

	console.log(color + '   ' + type + '  - ' + reset + msg);
}

function get_song_queue_count(data,_)
{
	var s_count,q_count;
	s_count = r_client.hget("song_count",data['song'],_);
	q_count = r_client.hget("queue_count",data['song'],_);
	console.log(s_count);
	console.log(q_count);
	if(s_count==null)
		s_count = 0;
	if(q_count==null)
		q_count = 0;
	count = {
		song_count: s_count,
		queue_count: q_count
	}
	return count;
	
}

function setUserStatus(data, user_status) 
{
	r_pub_client.pubsub('channels', function(error, stdout, stderr) {
		r_client.hget('friends',data['user_id'], function(f_error, f_stdout, f_stderr) {
			friends = JSON.parse(f_stdout);
			if (friends == null || friends == [])
				return;

			online_users = stdout;
			if (online_users == null || online_users == [])
				return;
			for (var n in friends) {
				if (online_users.indexOf(friends[n]) != -1 && friends[n] != data['channel']) {
					message = '{"type":"' + user_status + '","name" : "' + data['user_name'] + '" , "id":'+data['user_id']+' , "icon": "'+data['user_icon']+'"}';
					r_pub_client.publish(friends[n], message);
				}
			}
		});
	});
}



function add_to_log(msg,group_id,_)
{
	console.log("in add to log");
	msg['time'] = new Date().getTime();
	msg_clone = clone(msg, true);
	members = r_client.hget("group_members",group_id,_);
	if (members!=null)
	{
		members = members.split(",");
		user = msg['user']['user_id'];
		for(var i=0;i<members.length;i++)
		{
			if(is_group_listen_enabled(members[i],_) && is_in_group(members[i],group_id,_))
			{
				if(members[i]==user)
					msg['user']['current_user'] = true;
				else
					msg['user']['current_user'] = false;
				r_pub_client.publish("realtime_"+members[i], JSON.stringify({
					"type" : "group_log",
					"log" : msg 
				}));
			}
		}
	}
	if(msg_clone["type"] == "chat")
      r_client.lpush("group:"+group_id+":log.chat",JSON.stringify(msg_clone),_)
    else
  	  r_client.lpush("group:"+group_id+":log.other",JSON.stringify(msg_clone),_)
}

function is_group_listen_enabled(user_id,_)
{
	return (r_client.hget("group_enabled",user_id,_)!=null)
}

function is_in_group(user_id,group_id,_)
{
	return (r_client.hget("user_group",user_id,_)==group_id)
}


ActionMailer::Base.smtp_settings = {
  :address              => "smtp.gmail.com",
  :port                 => 587,
  :domain               => "amuzz.in",
  :user_name            => "amuzzin.app",
  :password             => "daredevil007",
  :authentication       => "plain",
  :enable_starttls_auto => true
}

module Devise::Models::Confirmable
 
	# Override Devise's own method. This one is called only on user creation, not on subsequent address modifications.
	def send_on_create_confirmation_instructions
		WelcomeMailer.welcome_email(self).deliver
	end
 
end
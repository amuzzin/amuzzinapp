if Rails.env.production?
  OmniAuth.config.full_host = "http://amuzz.in"
else
  OmniAuth.config.full_host = "http://amuzz.in:9999"
end
 
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, FACEBOOK_KEY, FACEBOOK_SECRET , :display => 'popup'
  
end

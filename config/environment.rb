# Load the rails application
require File.expand_path('../application', __FILE__)
require 'will_paginate'
require 'dalli'
require "redis"

# Initialize the rails application
MusicCutter::Application.initialize!
#for storing current playlist of friends

config   = Rails.configuration.database_configuration
$rails_redis_port = config[Rails.env]["rails_redis_port"]
$redis = Redis.new(:host => 'localhost', :port => $rails_redis_port)
CACHE = Dalli::Client.new(['127.0.0.1:11211'])

require './lib/song_recommender.rb'
Recommender = SongRecommender.new
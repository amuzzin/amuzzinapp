MusicCutter::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false
  BetterErrors::Middleware.allow_ip! ENV['TRUSTED_IP'] if ENV['TRUSTED_IP']

  config.consider_all_requests_local = false

  # Expands the lines which load the assets
  config.assets.debug = true
  config.action_mailer.default_url_options = { :host => 'localhost:9999' }
  
  FACEBOOK_KEY = 210651542320460
  FACEBOOK_SECRET = '7aa9964fef2abc0a748e88e8721f2dcf'

  TWITTER_KEY = '8l25JxHE2M91U8kfkcK8ug'
  TWITTER_SECRET = 'XpsJfbSWNhhsmBwyEycvONdjDsoEPegwEj1hUDch2s'
  TWITTER_OAUTH_TOKEN = '62305160-lOClP5kowLpoqb3YaxBR5zq2u1FWi7QyfLhMr4dBQ'
  TWITTER_OAUTH_TOKEN_SECRET = 'Y5wZ14N8cXvRvqPuzFT4G3pKziNRnT570JF9ARNOEwY'
  
  GOOGLE_KEY = '562777790188.apps.googleusercontent.com'
  GOOGLE_SECRET = 'gAOKu4Ev1Hb_MEfJOqK5lKCq'

  GITHUB_KEY = '32ae212cbfe351042777'
  GITHUB_SECRET = '3f69802f3f3a9a10edb78729faa9717fa9555f0e'

  SOCKET_IO_URL = '127.0.0.1:3008'

  IMG_CDN = ""
  MP3_CDN = ""

  SOUNDCLOUD_CLIENT_ID = "e876623b522c99579f380ce3b0a18c11"
  SOUNDCLOUD_CLIENT_SECRET = "26bc8f7bf1b967d9f5e6ad1e8216e215"
end

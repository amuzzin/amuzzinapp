MusicCutter::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Configure static asset server for tests with Cache-Control for performance
  config.serve_static_assets = true
  config.static_cache_control = "public, max-age=3600"

  # Log error messages when you accidentally call methods on nil
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment
  config.action_controller.allow_forgery_protection    = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Print deprecation notices to the stderr
  config.active_support.deprecation = :stderr  
  FACEBOOK_KEY = 210651542320460
  FACEBOOK_SECRET = '7aa9964fef2abc0a748e88e8721f2dcf'

  TWITTER_KEY = '8l25JxHE2M91U8kfkcK8ug'
  TWITTER_SECRET = 'XpsJfbSWNhhsmBwyEycvONdjDsoEPegwEj1hUDch2s'
  TWITTER_OAUTH_TOKEN = '62305160-lOClP5kowLpoqb3YaxBR5zq2u1FWi7QyfLhMr4dBQ'
  TWITTER_OAUTH_TOKEN_SECRET = 'Y5wZ14N8cXvRvqPuzFT4G3pKziNRnT570JF9ARNOEwY'

  GOOGLE_KEY = '562777790188.apps.googleusercontent.com'
  GOOGLE_SECRET = 'gAOKu4Ev1Hb_MEfJOqK5lKCq'

  GITHUB_KEY = '32ae212cbfe351042777'
  GITHUB_SECRET = '3f69802f3f3a9a10edb78729faa9717fa9555f0e'

  SOCKET_IO_URL = "notify.amuzz.in"

  IMG_CDN = "http://img.amuzz.in"
  MP3_CDN = "http://media.amuzz.in"

  SOUNDCLOUD_CLIENT_ID = "e876623b522c99579f380ce3b0a18c11"
  SOUNDCLOUD_CLIENT_SECRET = "26bc8f7bf1b967d9f5e6ad1e8216e215"
end

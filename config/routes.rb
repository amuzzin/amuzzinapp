MusicCutter::Application.routes.draw do
  use_doorkeeper

  match '/rate' => 'rater#create', :as => 'rate'
  match '/users/sign_out' => 'users#sign_out'

  get "activities/index"

  opinio_model

  devise_for :users, :controllers => { :registrations => "registrations" }

  resource :password

  resources :users

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'upload_song#index'
  match '/auth/:provider/callback' => 'authentications#create'
  match '/auth/failure' => 'authentications#failure'
  
  match '/ct/:id' => 'ct#handle_url'
  match '/up/:id' => 'ct#handle_upload_song_url'
  match '/uploaded_song/:id' => 'upload_song#edit_song'
  match '/uploaded_song/stream/:id' => 'upload_song#stream'
  
  #match '/playlist' => 'playlist#my_playlist'
  match '/favourites' => 'favourite#my_favourite'
  match '/songs' => 'user#my_song'
  match '/insight' => 'user#insight'
  match '/friends' => 'user#friends_stream'
#  match '/feed' => 'activities'
  match '/upload' => 'upload_song#uploader'
  match '/messages' => 'notification#notification'
  match '/group_listen' => 'group#group_listen'
  match '/all_friends' => 'user#all_friends'
  match '/discover' => 'song_discovery#index'

  match '/privacy' => 'terms#privacy'
  match '/terms' => 'terms#terms'
  match '/u/:id' => 'user#profile'
  match '/s/:id' => 'upload_song#song_page'

  match '/playlists/mass_add_songs/:queue' => 'playlist#mass_add_songs'
  
  resources :authentications
  resources :uploaded_song
  resources :song_master
  resources :activity
  resources :playlists do
    member do
      post :add_song
      post :add_multiple_song
      post :remove_song
    end
    collection do
      post :make_public
      post :make_private
    end  
  end

  resources :uploaded_songs do
   opinio
  end
  
 # namespace :admin do
 #   match 'mailer(/:action(/:id(.:format)))' => 'mailer#:action'
 # end
  # See how all your routes lay out with "rake routes"
  
  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
   match ':controller(/:action(/:id))(.:format)'

   #404
   match '/404', :to => 'errors#page_not_found'
   match '/422', :to => 'errors#server_error'
   match '/500', :to => 'errors#server_error'
end

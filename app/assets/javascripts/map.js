function showAddress(address)
{
  if (geocoder) 
  {
    geocoder.getLatLng(
      address,
      function(point) {
        if (!point) 
        {
          lat = google.loader.ClientLocation.latitude;
          lon = google.loader.ClientLocation.longitude;
          var marker = new GMarker(lat, lon);
          map.addOverlay(new GMarker(marker));
        } 
        else 
        {
          map.setCenter(point, 15);
          var marker = new GMarker(point, {draggable: true});
          map.addOverlay(marker);
          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
          });
        }
      }
    );
  }
}


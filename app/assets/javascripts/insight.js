
function drawChart() {
	drawArtistTimelineChart();
	drawArtistFavouriteChart();
	drawArtistListendChart();	
	drawArtistQueuedChart();	
}

function drawArtistTimelineChart()
{
	$.getJSON('/user/timeline_insight',function(data_array){
		var data = google.visualization.arrayToDataTable(data_array);

		 var options = {
		    title: 'Timeline events on songs Uploaded by you',
		    legend: { position: 'bottom' },
		    vAxis:{viewWindow: {min: 0}}
		   };

		var chart = new google.visualization.LineChart(document.getElementById('timeline_insight'));
		chart.draw(data, options);
	});
}

function drawArtistFavouriteChart()
{
	$.getJSON('/user/favourite_insight',function(data_array){
		var data = google.visualization.arrayToDataTable(data_array);
		 var options = {
		    title: 'Insight on songs Favourited by your fans',
		    legend: { position: 'bottom' }
		  };

		var chart = new google.visualization.PieChart(document.getElementById('favourite_insight'));
		chart.draw(data, options);
	});
}

function drawArtistListendChart()
{
	$.getJSON('/user/listend_insight',function(data_array){
	var data = google.visualization.arrayToDataTable(data_array);

	 var options = {
	    title: 'Insight on songs Currently Listened by your fans',
	    legend: { position: 'bottom' }
	  };

	var chart = new google.visualization.PieChart(document.getElementById('listend_insight'));
	chart.draw(data, options);
});
}

function drawArtistQueuedChart()
{
	$.getJSON('/user/queued_insight',function(data_array){
	var data = google.visualization.arrayToDataTable(data_array);
	 var options = {
	    title: 'Insight on songs Queued for playing by your fans',
	    legend: { position: 'bottom' }
	  };

	var chart = new google.visualization.PieChart(document.getElementById('queued_insight'));
	chart.draw(data, options);
});
}
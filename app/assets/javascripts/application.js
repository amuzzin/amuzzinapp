// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require essentials
//= require jquery-fileupload
//= require twitter/bootstrap
//= require cut_song
//= require my_song
//= require uploader
//= require playlist
//= require favourite
//= require comment
//= require queue
//= require search
//= require socket
//= require jquery.bpopup.min
//= require jquery.slimscroll.min
//= require jquery.popover
//= require amuzzin
//= require mousetrap.min
//= require player
//= require jquery.dotdotdot-1.5.1
//= require jquery.history
//= require socket.io
//= require jquery.cookie
//= require jquery.timeago
//= require intro.min
//= require jquery.inview.min
//= require jquery.iframe-transport
//= require friend_map
//= require audio.min
//= require jquery.qrcode.min
//= require ZeroClipboard.min
//= require jquery.raty
//= require letsrate
//= require upload
//= require current_song
//= require id3-minimized
//= require jquery.form.min
//= require scroll
//= require group_listen
//= require insight

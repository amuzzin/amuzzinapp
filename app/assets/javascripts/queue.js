$(document).ready(function() {
	$('#queue-playlist').hide();
	$('#search-queue').hide();

	$("#save-queue").click(function() {
		$('#save_as_playlist_name').val('');
		if (!$('#search-queue').is(':hidden')) 
		{
			$('#search-queue-btn').removeClass('transparent');
			$('#search-queue').slideToggle('medium', function() {});	
		}
		if ($('#queue-playlist').is(':hidden')) {
			$('#save-queue').addClass('transparent');
		} else {
			$('#save-queue').removeClass('transparent');
		}
		$('#queue-playlist').slideToggle('medium', function() {
		});
	});
	jQuery.expr[':'].contains = function(a, i, m) { 
  		return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0; 
	};
	$("#search-queue-input").on('input',function() {
		$('#playlist1 .queue-found').removeClass('queue-found');
		var searchString = $('#search-queue-input').val(),
        found = $('#playlist1 span:contains("' + searchString + '")');
        if(found.length>0)
        {
        	found = $(found[0]);
			found.addClass('queue-found');
			console.log(found.offset().top - $('#menu1').offset().top);
			$('#playlist1').slimScroll({scrollTo: (found.offset().top - $('#menu1').offset().top -10) });
		}
	});
	$("#search-queue-btn").click(function() {
		$('#search-queue-input').val('');
		if (!$('#queue-playlist').is(':hidden')) 
		{
			$('#save-queue').removeClass('transparent');
			$('#queue-playlist').slideToggle('medium', function() {});	
		}
		if ($('#search-queue').is(':hidden')) {
			$('#search-queue-btn').addClass('transparent');
		} else {
			$('#playlist1 .queue-found').removeClass('queue-found');
			$('#search-queue-btn').removeClass('transparent');
		}
		$('#search-queue').slideToggle('medium', function() {
			$('#search-queue-input').focus();
		});
	});
	$("#save-as-playlist-form").submit(function() {
		if ($('#save_as_playlist_name').val() == '') {
			alert('Enter a playlist name for the queue');
			return false;
		}
		$('#queue-playlist').slideUp('medium', function() {
			$('#save-queue').removeClass('disabled');
		});
	});
}); 
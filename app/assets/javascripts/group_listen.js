var group_div_state = true;
var group_chat_title_enabled = false;
var group_chat_title;

$(document).ready(function(){
  $('body').on('click','#show-group-members',function(e){
    e.preventDefault();
    $('#group-slider').animate({scrollLeft: -1 * $('#group-slider').width() }, 500);
  });
  $('body').on('click','#show-online-friends',function(e){
    e.preventDefault();
    $('#group-slider').animate({scrollLeft: $('#group-slider').width() }, 500);
  });
  $('body').on('focus','#send_chat_to_group_box',function(){
    chat_focus = true;
    $('#group_chat_count').html(0);
    $('#group_chat_count').hide();
    group_chat_title_enabled = false;
  });
  $('body').on('blur','#send_chat_to_group_box',function(){
    chat_focus = false;
  });
  $('body').on('click','.invite-offline-friend',function(e){
    e.preventDefault();
    post_action = $(this).attr('class','btn btn-xs btn-success disabled');
    post_action = $(this).html("<i class='icon icon-plus'></i> Invited</button>");
    invoke_controller_action('','POST','group/call_offline_friend/'+$(this).attr('data-id'),post_action)
  });
  $('body').on('click','#show-group-chat',function(e){
    $(this).addClass('disabled');
    $('#show-group-log').removeClass('disabled');
    $('#group-log-chat').show();
    $('#group-log-other').hide();
    var scrollTo_val = $('#group-chat').prop('scrollHeight') + 'px';
    $('#group-chat').slimScroll({scrollTo: scrollTo_val});
  });
  $('body').on('click','#show-group-log',function(e){
    $(this).addClass('disabled');
    $('#show-group-chat').removeClass('disabled');
    $('#group-log-other').show();
    $('#group-log-chat').hide();
    var scrollTo_val = $('#group-other').prop('scrollHeight') + 'px';
    $('#group-other').slimScroll({scrollTo: scrollTo_val});
  });
});

function start_new_group_listen()
{
  group_name = $('#group_name')[0].value;
  if(isBlank(group_name))
  {
    alert("Please enter a name for Group");
    return;
  }
  invoke_controller_action('','GET','/group/start_group_listen?name='+encodeURIComponent(group_name),'player_objects[0].enable_group_listen();');
}

function toggle_group_online()
{
  if(group_div_state)
    $('#right-panel').animate({ 'left': '-200px' });
  else
    $('#right-panel').animate({ 'left': '0px' });
  group_div_state = !group_div_state;
}

function show_group_listen_request(data)
{
  $('#bpopup-div').css('min-height','100px');
  $('#bpopup-div').css('min-width','350px');
  $('#bpopup-div').bPopup({
  opacity: 0.8,
  onOpen: function() {
  $('#bpopup-div').html('<div style="min-height:inherit;min-width:inherit;background:url(\'/loading.gif\') no-repeat center center;"></div>');  
  },
  onClose: function() {
     $('#bpopup-div').html('');
     group_call_audio_js.pause();
     resume_running_player();
  },
  onOpen: function() {
    stop_running_player();
    group_call_audio_js.play();
  },
 loadUrl: "/group/group_request?id="+data["group_id"]+"&user_id="+data["user"]["user_id"]
 });

}

function invite_to_group(el)
{
  if ($(el).attr('data-calling') == 'false')
  {
    disable_group_call($(el));
    invoke_controller_action('','POST','/group/invite_to_group?invite_user='+$(el).attr('data-user'),'');
    //socket.emit('invite_to_group', { invite: $(el).attr('data-user') });
  }
}

function leave_group()
{
  invoke_controller_action('','POST','/group/leave_group','put_state("/group_listen", "Amuzzin-Group Listen", "/group_listen");player_objects[0].disable_group_listen();');
  //socket.emit('leave_group');

}

function send_chat_to_group()
{
  message = $('#send_chat_to_group_box')[0].value;
  if(message.replace(/\s/g, '').length)
    socket.emit('send_chat_to_group', { message: message});
  $('#send_chat_to_group_box')[0].value='';
}

function accept_group_request(id)
{
  //clear the timeout for ignore request
  try
  {
    if(typeof ignore_request != "undefined" && ignore_request !=null)
    {
      clearTimeout(ignore_request);
      ignore_request = null;
    }
    //socket.emit('accept_group_invite');
    callback = 'show_flash("You have joined group listen. Your queue will now be replaced by group\'s queue<br/> Once you leave the group, your queue will be restored",false);'
    callback += 'put_state("/group_listen", "Amuzzin-Group Listen", "/group_listen");player_objects[0].enable_group_listen();'
    if(typeof id!= "undefined")
      invoke_controller_action('','POST','/group/join_group/'+id,callback);
    else
      invoke_controller_action('','POST','/group/join_group',callback);
  }
  catch(e)
  {
    console.log("Timeout not cleared");
  }
  if($('#bpopup-div').is(":visible"))
  {
    $('#bpopup-div').bPopup().close();  
  }
  
}

function switch_group(id)
{
  //clear the timeout for ignore request
  try
  {
    if(typeof ignore_request != "undefined" && ignore_request !=null)
    {
      clearTimeout(ignore_request);
      ignore_request = null;
    }
    //socket.emit('accept_group_invite');
    callback = 'show_flash("You have joined group listen. Your queue will now be replaced by group\'s queue<br/> Once you leave the group, your queue will be restored",false);'
    callback += 'put_state("/group_listen", "Amuzzin-Group Listen", "/group_listen");player_objects[0].enable_group_listen();'
    if(typeof id!= "undefined")
      invoke_controller_action('','POST','/group/switch_group/'+id,callback);
    else
      return;
  }
  catch(e)
  {
    console.log("Timeout not cleared");
  }
  if($('#bpopup-div').is(":visible"))
  {
    $('#bpopup-div').bPopup().close();  
  }
  
}

function ignore_group_request()
{
  clearTimeout(ignore_request);
  //socket.emit('ignore_group_invite');
  invoke_controller_action('','POST','/group/ignore_group_request','');
  $('#bpopup-div').bPopup().close();
}

function enable_group_call(button)
{
  button.addClass('btn-danger');
  button.removeClass('btn-success');
  button.removeClass('blink');
  button.html('<i class="icon icon-plus"></i>');
  button.attr('data-calling','false');
}

function disable_group_call(button)
{
  button.addClass('btn-success');
  button.addClass('blink');
  button.removeClass('btn-danger');
  button.html('<i class="icon icon-phone"></i>');
  button.attr('data-calling','true');
}

function change_group_members(data,action)
{
  if(action=='add')
    $('#group_member_table').append('<tr id="group_member_'+data['user']['user_id']+'"><td><img src="'+data['user']['user_icon']+'" class="small-image queue-image"/></td><td>'+data['user']['user_name']+'</td></tr>')
  else if(action=='remove')
    $('#group_member_'+data['user']['user_id']).remove();
}

function set_curr_song_for_group(song_id)
{
  playlist = player_objects[0].playlist;
  for(i=0;i<playlist.length;i++)
  {
    el = $('#'+playlist[i]);
    if(el.attr('data-id')==song_id)
    {
      player_objects[0].socket_enabled = false;
      status = player_objects[0].status;
      player_objects[0].playChosen(player_objects[0].playlist[i]);
      //Play only if the player was already playing
      if(status == 0)
        player_objects[0].play_pause_song();
      player_objects[0].socket_enabled = true;
    }
  }
}

function remove_song_from_group(song_id)
{
  playlist = player_objects[0].playlist;
  for(i=0;i<playlist.length;i++)
  {
    el = $('#'+playlist[i]);
    if(el.attr('data-id')==song_id)
    {
      player_objects[0].socket_enabled = false;
      player_objects[0].remove_song(null,playlist[i]);
      player_objects[0].socket_enabled = true;
    }
  }
}

function add_songs_to_group(log)
{
  invoke_controller_action('','GET','/queue/add_songs_to_queue?id='+log['song']+'&user='+log['user']['user_name'],'');
}

function enable_group()
{
  //socket.emit('enable_group_listen');
  invoke_controller_action('','POST','/group/enable_group','put_state("/group_listen", "Amuzzin-Group Listen", "/group_listen");player_objects[0].enable_group_listen();');
}

function disable_group()
{
  //socket.emit('disable_group_listen');
  invoke_controller_action('','POST','/group/disable_group','put_state("/group_listen", "Amuzzin-Group Listen", "/group_listen");player_objects[0].disable_group_listen()');
}


function notify_missed_call(user_id,group_id)
{
  invoke_controller_action('','POST',"/user/notify_missed_call?group_id="+group_id+"&user_id="+user_id,'');
}

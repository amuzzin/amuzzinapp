$(document).ready(function() {
  $("body").on('change','.media-file',function(event){

      var input = $(event.currentTarget);
      var file = input[0].files[0];
      if(file.name.search(/.mp3$/) == -1)
      {
        show_flash("Sorry! currently we support only mp3 files.");
        return false;
      }
      var bar = $('#upload-bar');
      var fstatus = $('#file-status');
      var percent = $('#upload-percent');
      var progress = $('.upload-progress');

      fstatus.html(' Uploading ...');

      $('#fileupload').ajaxSubmit({
           beforeSend: function() {
              fstatus.show();
              progress.show();
              percent.show();
              percentVal = '0%';
              bar.width(percentVal)
              percent.html(percentVal);
          },
          uploadProgress: function(event, position, total, percentComplete) {
              percentVal = percentComplete + '%';
              bar.width(percentVal)
              percent.html(percentVal);
          },
          success: function() {
              percentVal = '100%';
              bar.width(percentVal)
              percent.html(percentVal);
          },
        complete: function(xhr) {
          data = JSON.parse(xhr.responseText);
          $("#upload_edit_form_for_song").attr("action","/uploaded_song/"+data["files"][0]["id"]);
          $("#uploaded_song_save_button").removeAttr("disabled");
          $("#uploaded_song_save_button").attr("value","Update Song Details");
          progress.hide();
          percent.hide();
          fstatus.hide();
          bar.width("0%");
          show_flash(file.name+' uploaded successfully. <br/> Please update the details if required.',true);
        }
      });

     loadFromFile(file);
  });
});


function loadFromFile(file) {
var url = file.urn ||file.name;
loadUrl(url, null, FileAPIReader(file));
} 

function loadUrl(url, callback, reader) {
ID3.loadTags(url, function() {
var tags = ID3.getAllTags(url);


  // Populate the edit field with Values

  $("#uploaded_song_artist").attr('value',tags.artist || "");
  $("#uploaded_song_title").attr('value',tags.title || "");
  $("#uploaded_song_album").attr('value',tags.album || "");
  $("#uploaded_song_year").attr('value',tags.year || "");
  $("#uploaded_song_genre").attr('value',tags.genre || "");
  $("#uploaded_song_track").attr('value',tags.track || "");
  $("#uploaded_song_lyrics").attr('value',(tags.lyrics||{}).lyrics || "");

  // Populate Album Art from Data Stream 

  if( "picture" in tags ) {
    var tmp_image = tags.picture;
    var base64String = "";
    for (var i = 0; i < tmp_image.data.length; i++) {
      base64String += String.fromCharCode(tmp_image.data[i]);
    }
    $("#new_album_art").attr('src',"data:" + tmp_image.format + ";base64," + window.btoa(base64String));
  } 
  else 
  {
    $("#new_album_art").attr('src',"/default.jpg");
  }

 // Show the Populated form
 $('#edit_new_song').show();
 $('#copyright_info').hide();
 
if( callback ) { callback(); };
  },
  {tags: ["artist", "title", "album", "year", "comment", "track", "genre", "lyrics", "picture"],
   dataReader: reader});
}
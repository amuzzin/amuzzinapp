$(document).ready(function() {

	$("body").on('mouseenter', '.popular-td', function() {
		$($(this).children('div')[0]).children('.hide').show();
	});
	$("body").on('mouseleave', '.popular-td', function() {
		$($(this).children('div')[0]).children('.hide').hide();
	});
	
	$('body').on('click','.play-page',function() {
		mass_add_songs_to_queue(1, $('#'+$(this).parent().attr('id')+' .song-el').children('span'), 'play');
	});
	$('body').on('click','.add-page',function() {
		mass_add_songs_to_queue(1, $('#'+$(this).parent().attr('id')+' .song-el').children('span'), 'add');
	}); 
	$("body").on('click', '.play-it', function() {
		playit(1, $(this).parents('td').siblings('.song-el').children('span')[0]);
	});
	$("body").on('click', ".add-it", function() {
		addit(1, $(this).parents('td').siblings('.song-el').children('span')[0]);
	});
}); 
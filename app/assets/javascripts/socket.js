function addMessage(data,friends,user_id) {
	console.log(data);

	switch(data["type"]) {
		case "online":
			friends[data["id"]] = 1;
			$('#online_count').html(getOnline(friends,user_id));
			$('#online_count').show();
			$('#none-online').remove();
			if($('#online_friend_'+data["id"]).length <=0)
			{
				//if he is a group member hide invite button else show it
				if($('#group_member_'+data["id"]).length >0)
				{
					$('#group_online_table').append('<tr id="online_friend_'+data["id"]+'">'+
	 					'<td><img src="'+data["icon"]+'" class="small-image queue-image"/></td>'+
	      	  			'<td style="width:100px">'+data["name"]+'</td>'+
	      	  			'<td><button style="display:none" class="btn btn-danger btn-xs invite_user_to_group" id="invite_user_'+data["id"]+'" data-user="'+data["id"]+'"  data-calling="false" onclick="invite_to_group(this)">'+
	      	  			'<i class="icon icon-plus"></i></button></td></tr>');	
				}
				else
				{
					$('#group_online_table').append('<tr id="online_friend_'+data["id"]+'">'+
	 					'<td><img src="'+data["icon"]+'" class="small-image queue-image"/></td>'+
	      	  			'<td style="width:100px">'+data["name"]+'</td>'+
	      	  			'<td><button class="btn btn-danger btn-xs invite_user_to_group" id="invite_user_'+data["id"]+'" data-user="'+data["id"]+'"  data-calling="false" onclick="invite_to_group(this)">'+
	      	  			'<i class="icon icon-plus"></i></button></td></tr>');
				}
			
			}
			break;
		case "offline":
			friends[data["id"]] = 0;
			$('#online_count').html(getOnline(friends,user_id));
			if(parseInt($('#online_count').html()) < 1)
			{
				$('#online_count').hide();
			}
			$('#online_friend_'+data["id"]).remove();
			if($('#group_online_table tr').length == 0)
			{
				$('#group_online_table').html('<tr id="none-online"><td colspan="2">No friend is online</td></tr>');
			}
			break;
		case "comment":
			notify_audio_js.play();
			if(Notification)
			{
			n = new Notification(data["name"] + " commented on your song " + data["song"], {
				body : data["body"],
				icon: data["icon"],
				tag: "comment"+data["id"]+data["body"]
			});
			n.onclick = function(x) { window.focus(); this.cancel(); invoke_controller_action.call(window,'','GET','/notification/notification','');};
			n.onshow = function() { setTimeout(n.close, 5000) }
			n.show();
			}
			$('#notify_count').html(parseInt($('#notify_count').html())+1);
			$('#notify_count').show();
			break;
		case "favourite":
			notify_audio_js.play();
			if(Notification)
			{
			n = new Notification("New favourite by " + data["name"], {
				body : data["name"] + " Favourited on your song " + data["song"],
				icon: data["icon"],
				tag: "favourite"+data["id"]+data["song"]
			});
			n.onclick = function(x) { window.focus(); this.cancel(); invoke_controller_action.call(window,'','GET','/notification/notification','');};
			n.onshow = function() { setTimeout(n.close, 5000) }
			n.show();
			}
			$('#notify_count').html(parseInt($('#notify_count').html())+1);
			$('#notify_count').show();
			break;
		case "missed_call":
			notify_audio_js.play();
			if(Notification)
			{
			n = new Notification("Missed group listen request from " + data["from"], {
				body : data["from"] + " sent you a group listen request",
				icon: data["icon"],
				tag: "missed_call"+data["id"]
			});
			n.onclick = function(x) { window.focus(); this.cancel(); invoke_controller_action.call(window,'','GET','/notification/notification','');};
			n.onshow = function() { setTimeout(n.close, 5000) }
			n.show();
			}
			$('#notify_count').html(parseInt($('#notify_count').html())+1);
			$('#notify_count').show();
			break;
		case "add_friend":
			notify_audio_js.play();
			if(Notification)
			{
			n = new Notification("Friend request from " + data["from"], {
				body : data["from"] + " sent you a friend request",
				icon: data["icon"],
				tag: "add_friend_"+data["id"]
			});
			n.onclick = function(x) { window.focus(); this.cancel(); invoke_controller_action.call(window,'','GET','/notification/notification','');};
			n.onshow = function() { setTimeout(n.close, 5000) }
			n.show();
			}
			$('#notify_count').html(parseInt($('#notify_count').html())+1);
			$('#notify_count').show();
			break;
		case "accept_friend":
			notify_audio_js.play();
			if(Notification)
			{
			n = new Notification("Friend request accepted by " + data["from"], {
				body : data["from"] + " has accepted your friend request",
				icon: data["icon"],
				tag: "accept_friend_"+data["id"]
			});
			n.onclick = function(x) { window.focus(); this.cancel(); invoke_controller_action.call(window,'','GET','/notification/notification','');};
			n.onshow = function() { setTimeout(n.close, 5000) }
			n.show();
			}
			$('#notify_count').html(parseInt($('#notify_count').html())+1);
			$('#notify_count').show();
		case "feed":
		  	notify_audio_js.play();
			console.log("feed id:" + data["feed_id"]);
			additional_feeds.push(data["feed_id"]);
			$('#new_feed').html('<center><a class="link" onclick="load_diff_feed()"><span class="red"> You have ' + additional_feeds.length + ' new Feed(s) </span></a></center>');
			$('#new_feed').show();
			$('#unread_feed_count').html(additional_feeds.length);
			$('#unread_feed_count').show();
			break;

		case "song_queue_count":
			if (data['song'] == '')
				$('#info-div').html("No info available");
			else
			{
				if(player_objects[0].current_audio.getAttribute('data-id') == data['song'])
					$('#info-div').html("<p><span class='red'><strong>" + data['song_count'] + "</strong></span> people are currently listening to this song worldwide</p><p><span class='red'><strong>" + data['queue_count'] + "</strong></span> people have this song in their queue worldwide</p>");
			}
			break;
		case "nowplaying":
			sync_song_with_friend(data["audio"]);
			break;

		case "group_listen_request":
			show_group_listen_request(data);
			break;

		case "group_listen_request_ignored":
			show_flash(data['user']['user_name'] + " is not available for group listen");
  			var button = $('#invite_user_'+data['user']['user_id']);
  			enable_group_call(button); 
			break;

		case "group_log":
			log = data['log'];
			html = '';
			switch(log['type'])
			{
				case 'join':
					var button = $('#invite_user_'+log['user']['user_id']);
					button.hide();
					html = '<div style="margin-top:5px;">'+log['user']['user_name']+' <strong>joined</strong> the group</div>';
					change_group_members(log,'add');
					break;
				case 'leave':
					var button = $('#invite_user_'+log['user']['user_id']);
					button.show();
					enable_group_call(button);
					html = '<div style="margin-top:5px;">'+log['user']['user_name']+' <strong>left</strong> the group</div>';
					change_group_members(log,'remove');
					break;
				case 'chat':
					if(!log['user']['current_user'])
					{	
						if (!(window.location.href.indexOf("/group_listen") != -1 && window_focus && chat_focus))
						{
							notify_audio_js.play();
							$('#group_chat_count').html(parseInt($('#group_chat_count').html())+1);
							if(parseInt($('#group_chat_count').html()) < 1)
							{
								$('#group_chat_count').hide();
							}
							else
							{
								$('#group_chat_count').show();	
							}
							group_chat_title_enabled = true;
							group_chat_title = log['user']['user_name']+" says ... - Group Listen - amuzzin";
						}
					}

					html = '<div style="margin-top:5px"><image class="small-image queue-image" src="'+log['user']['user_icon']+'"/>&nbsp;&nbsp;&nbsp;<strong>'+log['user']['user_name']+'</strong>: ';
					html += log['message']+'<br><div class="pull-right offset10pxr"><p class="time-ago"><i class="icon icon-time"></i>&nbsp;<abbr class="timeago" title="'+new Date(log['time']).toISOString()+'">';
					html += new Date(log['time']).toLocaleString()+'</abbr></p></div></div><br clear="both">'; 
					break;
				case 'switch_to':
				case 'enable':
					el = $('#group_member_'+log['user']['user_id']);
					el.removeClass('transparent');
					el.attr('title','');
					break;
				case 'switch_from':
				case 'disable':
					el = $('#group_member_'+log['user']['user_id']);
					el.addClass('transparent');
					el.attr('title','This user is not in this group right now');
					break;
				case 'set_curr_song':
					if(!log['user']['current_user'])
					{
						set_curr_song_for_group(log['song']);
					}
					html = '<div style="margin-top:5px;">'+log['user']['user_name']+' <strong>changed</strong> the song to: <span class="red">'+log['song_name']+'</span></div>';					
					break;
				case 'clear_queue':
					if(!log['user']['current_user'])
					{
						player_objects[0].reset_player();
					}
					html = '<div style="margin-top:5px;">'+log['user']['user_name']+' <strong>cleared</strong> the group queue</div>';					
					break;
				case 'remove_from_queue':
					if(!log['user']['current_user'])
					{
						remove_song_from_group(log['song']);
					}
					html = '<div style="margin-top:5px;">'+log['user']['user_name']+' <strong>removed</strong> the song : <span class="red">'+log['song_name']+'</span> from the group queue</div>';					
					break;
				case 'add_to_queue':
					if(!log['user']['current_user'])
					{
						add_songs_to_group(log);
					}
					break;


			}
			if($('#group-log').attr('data-id')==log['group'])
			{
				if(log['type']=='chat')
				{
					$('#group-chat').append(html);
					$("abbr.timeago").timeago();
					var scrollTo_val = $('#group-chat').prop('scrollHeight') + 'px';
					$('#group-chat').slimScroll({scrollTo: scrollTo_val});
				}
				else
				{
					$('#group-other').append(html);
					var scrollTo_val = $('#group-other').prop('scrollHeight') + 'px';
					$('#group-other').slimScroll({scrollTo: scrollTo_val});
				}
			
			}

			break;
	}

}

function getOnline (friends,user_id) {
	online = 0;
	Object.keys(friends).forEach(function (key) { 
    	online = friends[key] + online;
	});
	if(friends[user_id] == 1)
	online = online - 1;
	return online;
}

function load_diff_feed() {

	invoke_controller_action('', 'POST', '/activities/load_diff?feed_ids=' + JSON.stringify(additional_feeds), '', "Amuzzin-Feeds", "/activities/");
	additional_feeds = [];

}

function setStatus(msg) {
	console.log('Connection Status : ' + msg);
}
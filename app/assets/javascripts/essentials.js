//= require jquery.ui.all
//= require jquery_ujs
//= require jquery.remotipart

function show_flash(message,permanent) {
	$('#flash').html('<div class="alert alert-info"><button class="close" data-dismiss="alert">×</button>' + message + '</div>');
	$('#flash').hide();
	$('#flash').stop().fadeOut();

		$('#flash').fadeIn('normal', function() {
			if(!permanent)
			{
				$(this).delay(3500).fadeOut();
			}
		});
}

function enable_onbeforeunload()
{
	window.onbeforeunload=confirmOnPageExit;
}

function disable_onbeforeunload()
{
	window.onbeforeunload=null;
}
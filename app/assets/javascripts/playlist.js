$(document).ready(function() {
	$('body').on('submit', ".playlist-form", function() {
		if (isBlank($('#playlist_name').val())) 
		{
			alert('Enter a playlist name');
			return false;
		}
	});

	$('body').on('click', '.play-playlist', function() {
		var el = this;
		if ($(el).attr('data-loaded') == 'false') {
			var div = $(el).parents('tr').next().children('td').children('div')[0];
			$(div).load('/playlists/playlist_songs/' + $(div).attr('id').replace('playlist_songs_', ''), function() {
				mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'play');
			});
			$(el).attr('data-loaded', 'true');
		} else
			mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'play');
	});

	$('body').on('click', '.add-playlist', function() {
		var el = this;
		if ($(el).attr('data-loaded') == 'false') {
			var div = $(el).parents('tr').next().children('td').children('div')[0];
			$(div).load('/playlists/playlist_songs/' + $(div).attr('id').replace('playlist_songs_', ''), function() {
				mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'add');
			});
			$(el).attr('data-loaded', 'true');
		} else

			mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'add');
	});

	$('body').on('click', '.play-friend-playlist', function() {
		var el = this;
		if ($(el).attr('data-loaded') == 'false') {
			var div = $(el).parents('tr').next().children('td').children('div')[0];
			$(div).load('/user/friend_songs/' + $(div).attr('id').replace('friend_songs_', ''), function() {
				mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'play');
			});
			$(el).attr('data-loaded', 'true');
		} else
			mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'play');
	});

	$('body').on('click', '.add-friend-playlist', function() {
		var el = this;
		if ($(el).attr('data-loaded') == 'false') {
			var div = $(el).parents('tr').next().children('td').children('div')[0];
			$(div).load('/user/friend_songs/' + $(div).attr('id').replace('friend_songs_', ''), function() {
				mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'add');
			});
			$(el).attr('data-loaded', 'true');
		} else

			mass_add_songs_to_queue(1, $($(el).parents('tr').next().find('.play-all')), 'add');
	});

	$('body').on('click', '.playlist-ajax-button', function() {
		var el = $(this).siblings('.playlist-menu')[0];
		$(el).html('<center><img src="/loading.gif"></center>');
		$(el).load('/playlists/available_playlists/' + $(this).attr('id').replace('playlist_', ''), function() {
			$($(el).children('div')[0]).slimScroll({
				height : '150px',
				alwaysVisible : true,
				railVisible : true
			});
		});

	});

});

function check_empty_selection()
{
	if($('#my_playlists .playlist-status:checked').length == 0)
	{
		alert('You need to select atleast one Playlist to perform this action. Choose the appropriate playlist(s) you want to change by selecting left checkbox.');
	}
}
function make_public() {
	check_empty_selection();
	var public_playlists = [];
	$('#my_playlists .playlist-status').each(function() {
		if ($(this).is(':checked')) {
			public_playlists.push($(this).attr('data-id'));
		}
	});
	if (public_playlists.length > 0)
		invoke_controller_action('', 'POST', "/playlists/make_public?id=" + public_playlists.join(','), '');
}

function make_private() {
	check_empty_selection();
	var private_playlists = [];
	$('#my_playlists .playlist-status').each(function() {
		if ($(this).is(':checked')) {
			private_playlists.push($(this).attr('data-id'));
		}
	});
	if (private_playlists.length > 0)
		invoke_controller_action('', 'POST', "/playlists/make_private?id=" + private_playlists.join(','), '');
}

function disable_favourite(el) {
	$(el).addClass('disabled');

}

function remove_playlist(id, el) {
	invoke_controller_action('', 'DELETE', "/playlists/" + id, '');
}

function showSongs(el, type) {
	var div = $(el).parents('tr').next().children('td').children('div')[0];
	if ($(div).height() == 0) {
		$(el).html('<i class="icon-chevron-down icon"></i>');
		$(div).html('<center><img src="/loading.gif"></center>');
		$(div).stop().animate({
			"height" : "200px"
		}, "fast", function() {
			if (type == "playlist") {
				$($(el).siblings('.play-playlist')[0]).attr('data-loaded','true');
				$($(el).siblings('.add-playlist')[0]).attr('data-loaded','true');
				$(div).load('/playlists/playlist_songs/' + $(div).attr('id').replace('playlist_songs_', ''), function() {
					$($(div).children('.playlist-songs-inner')[0]).slimScroll({
						height : '200px',
						alwaysVisible : true,
						railVisible : true
					});
				});
			} else if (type == "friend") {
				$($(el).siblings('.play-friend-playlist')[0]).attr('data-loaded','true');
				$($(el).siblings('.add-friend-playlist')[0]).attr('data-loaded','true');
				$(div).load('/user/friend_songs/' + $(div).attr('id').replace('friend_songs_', ''), function() {
					$($(div).children('.friend-songs-inner')[0]).slimScroll({
						height : '200px',
						alwaysVisible : true,
						railVisible : true
					});
				});
			}
		});
	} else {
		$(el).html('<i class="icon-chevron-up icon"></i>');
		$(div).stop().animate({
			"height" : "0px"
		}, "fast");
	}

}


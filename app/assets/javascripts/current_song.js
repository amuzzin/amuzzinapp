var soundcloud_loaded = false;
var youtube_loaded = false;
var related_loaded = false;

function get_sorry(current_song_id,type)
{
	html = " <br/> Sorry, we couldn't find any "+type+" for this song :( <br/><br/>";
	html += '<button id="editpopup_'+current_song_id+'" class="editpopup btn btn-default btn-xs" data-bpopup="{" contentcontainer":".content"}"="" title=" [#] Edit Metadata of this song. If you feel the songs Metadata is either incorrect or missing , you can edit the fields like Title, Album, Genre, Year and Lyrics of the song and make it perfect. You can also add YouTube Url of this song if you occassionaly wish to watch the video while hearing the song."><i class="icon icon-pencil"></i> try editing the song details for better results.</button>';
	return html;
}

function load_video(video,title)
{
	$('#video-title').html(decodeURI(title));
	$('#modal-content').html('<iframe width="530" height="300" src="'+video+'?autoplay=1" frameborder="0" allowfullscreen ></iframe>');
	$('#videoModal').modal('show');
}

function load_soundcloud(eid,title)
{
    $('#video-title').html(decodeURI(title));
    $('#modal-content').html('<iframe width="100%" height="166" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A//api.soundcloud.com/tracks/'+eid+'&amp;color=cc3333&amp;auto_play=true&amp;show_artwork=true"></iframe>');
    $('#videoModal').modal('show');
}

function bind_events_for_video_modal()
{

	$('#videoModal').bind('shown', function() {
                stop_running_player();
    });
	$('#videoModal').bind('hide', function() {
        $('#videoModal #modal-content').html('');
		//Pause song when video plays and vice versa
		resume_running_player();
	});
}

    function hide_and_show(div_id,song_query,owner,current_song_id)
	{
		 $("#current_song").hide();
                $("#twitter_nowplaying").hide();
                $('#soundcloud_mix').hide();
                 $('#youtube_related').hide();
                 $('#twitter_trend').hide();
                 $('#related').hide();

                current_div = '#'+div_id;
                current_div_scroll = $(current_div); 
                $(current_div).show();


                if(div_id == 'soundcloud_mix' && soundcloud_loaded == false) 
                {
                        trunc_text = 15;
                        soundcloud_div_content = '<a href="#" onclick="hide_and_show(\'current_song\')"> <i class="icon icon-caret-left"></i> <strong>Back to Current Song Info</strong> </a>';
                        soundcloud_div_content += '<br/><br/><span  class="amuzzin-font"><span class="red">Mix</span> Songs </span><br/><br/>';

                        soundcloud_div_content += "<table class='table striped'>";
                        SC.get('/tracks', { q: song_query , limit: 20}, function(tracks) {

                                if(tracks == null || tracks == undefined || tracks.length == 0)
                                {
                                        soundcloud_div_content += get_sorry(current_song_id,'Mixes');
                                        if($('#soundcloud_mix').html(soundcloud_div_content))
                                                {
                                                        soundcloud_loaded = true;                
                                                }
                                                return;
                                }
                                tracks.forEach(function (e) {
                                    try{
                                    e.title = e.title.replace("'","");
                               if(e.artwork_url != null){
     soundcloud_div_content += '<tr><td title="'+e.title+'"><div><a href="#" onclick="load_soundcloud(\''+e.id+'\',\''+encodeURI(e.title)+'\')"><img src="'+e.artwork_url+'" height=100 widht=100/></a></div></td>';

     soundcloud_div_content += '<td><a href="#" onclick="load_soundcloud(\''+e.id+'\',\''+encodeURI(e.title)+'\')"><i class="icon icon-play" /> &nbsp;'+e.title.substr(0,trunc_text)+'...</a><br/>';

     soundcloud_div_content +=  " By: <a href=\""+e.user.permalink_url+"\" target='_blank'>"+e.user.username.substr(0,trunc_text)+'...</a>';
     soundcloud_div_content += "<br/> Plays: <font color='green'>"+e.playback_count+"</font>";
          soundcloud_div_content += "<br/> Favourites: <font color='green'>"+e.favoritings_count+"</font>";
     soundcloud_div_content += "<br/> Genre: <font color='green'>"+e.genre.substr(0,trunc_text)+"</font></td></tr>";
                         }
                     } 
                     catch(err)
                     {}
                         // soundcloud_div_content += '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/'+e.id+'&amp;color=cc3333&amp;auto_play=false&amp;show_artwork=true"></iframe>';
                        });
                        soundcloud_div_content += "</table>"
                        $('#soundcloud_mix').html(soundcloud_div_content);
                         soundcloud_loaded = true;
                    });

                }

                if(div_id == 'youtube_related' && youtube_loaded == false)
                {


                        $.getJSON('https://gdata.youtube.com/feeds/api/videos?q='+song_query+'&max-results=16&order-by=published&alt=json',function(data){
                                html = "<a href='#'' onclick=\"hide_and_show('current_song')\"> <i class=\"icon icon-caret-left\"></i> <strong><strong>Back to Current Song Info</strong></strong> </a><br><br><span  class=\"amuzzin-font\"><span class=\"red\">Related</span> Videos</span><br><br>";
                                if(data["feed"]["openSearch$totalResults"]["$t"] == 0)
                                {
                                        html += get_sorry(current_song_id,'Videos');
                                        if($('#youtube_related').html(html))
                                                {
                                                        youtube_loaded = true;        
                                                           return;                
                                                }


                                }
                                videos = data["feed"]["entry"];
                                trunc_text = 15; 
                                html += "<table class='table striped'>"
                                for(var i in videos)
                                {
                                        videos[i]["title"]["$t"] = videos[i]["title"]["$t"].replace("'","");

                                        html_tmp = '';
                                        try 
                                        {
                                        html_tmp += '<tr title=\"'+videos[i]["title"]["$t"]+'\">'
                                        original_url = videos[i]["link"][0]["href"]
                                        youtube_url = videos[i]["link"][0]["href"].replace('https','http').replace('watch?v=','embed/').replace('&feature=youtube_gdata','');

                                        html_tmp += '<td><div><a href="#" onclick="load_video(\''+youtube_url+'\',\''+encodeURI(videos[i]["title"]["$t"])+'\')"><img src=\"'+videos[i]["media$group"]["media$thumbnail"][0]["url"]+'\" height=100 width=100/></a>';

                                        

                                        html_tmp += '</div></td>';
                        

                                        html_tmp += '<td><a href="#" onclick="load_video(\''+youtube_url+'\',\''+encodeURI(videos[i]["title"]["$t"])+'\')"><i class="icon icon-play" /> &nbsp;'+videos[i]["title"]["$t"].substr(0,trunc_text)+'...</a>';

                                        if(owner == 'true')
                                        {
                                        html_tmp += '<div class="btn-group pull-right">';

                                        html_tmp += '<button id =\"link_youtube_'+current_song_id+'\" title="Link this Video to your currently playing song. This will appear in your song information." class="btn btn-xs btn-danger" onclick=\'invoke_controller_action("","POST","/user/link_youtube_url?id='+current_song_id+'&youtube_url='+youtube_url+'&youtube_img_url='+videos[i]["media$group"]["media$thumbnail"][0]["url"]+'&original_url='+original_url+'","refresh_current_song_page('+current_song_id+')")\' style="margin-top:-5px"><i class="icon icon-link"></i> </button>';
                                        html_tmp += '</div>';
                                        }

                                        html_tmp += "<br/> By: "+videos[i]["author"][0]["name"]["$t"].substr(0,trunc_text)+'...';
                                        html_tmp += "<br/> Rating: ";
                                        if(videos[i]["gd$rating"])
                                        {
                                        html_tmp += "<span class='red'>"+videos[i]["gd$rating"]["average"].toString().substr(0,3)+"</span>/"+videos[i]["gd$rating"]["max"];
                                        }
                                        else
                                        {
                                                html_tmp += "N/A";
                                        }
                                        html_tmp += "<br/> Views: <font color='green'>"+videos[i]["yt$statistics"]["viewCount"]+'</font>';
                                        html_tmp += "</td></tr>"
                                        html += html_tmp;
                                }
                                catch(err)
                                {}

                                }
                                $('#youtube_related').html(html);
                                youtube_loaded = true;

                        });
                }
                
                if(div_id == 'related' && !related_loaded)
                {
                        $("#related-songs-container").load("/utility/related_songs?id="+current_song_id );
                        related_loaded = true;
                }
                               

		}
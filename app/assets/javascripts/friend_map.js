
function show_friends_in_map()
{
	$('#map').toggle();
	bounds_array = [];
	reverse_loc_to_user = {};

	for(var key in friend_geojson)
	{
		bounds_array.push([parseFloat(friend_geojson[key]["lat"]),parseFloat(friend_geojson[key]["lng"])]);
		if(reverse_loc_to_user[friend_geojson[key]["lat"]+friend_geojson[key]["lng"]] == undefined)
			reverse_loc_to_user[friend_geojson[key]["lat"]+friend_geojson[key]["lng"]] = [key];
		else 
			reverse_loc_to_user[friend_geojson[key]["lat"]+friend_geojson[key]["lng"]].push(key);

	}
	if(friend_map == undefined)
	{
		friend_map = L.map('map').setView([51.505, -0.09],1);
		tile = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		    styleId: 997,
		    maxZoom: 13
		}).addTo(friend_map);
		friend_map.fitBounds(bounds_array);

	}	

	for(var key in friend_geojson)
	{
		marker= L.marker([parseFloat(friend_geojson[key]["lat"]),parseFloat(friend_geojson[key]["lng"])]).addTo(friend_map);
		users_in_same_loc = reverse_loc_to_user[friend_geojson[key]["lat"]+friend_geojson[key]["lng"]];
		message = '';
		for(var index in users_in_same_loc)
		{
			user = users_in_same_loc[index];
			user_icon = friend_geojson[user]["img"];
			message += '<table class="table table-condensed table-bordered"><tr><td rowspan=2>';
			message += "<img class='small-image' src='"+user_icon+"' width='35px' height='35px' class='pull-right'/></td>";
			message += "<td><b>"+friend_geojson[user]["name"];
			/*
			message += '<a href="javascript:connect_with_friend('+key+')">';
			message += '<i class="icon icon-headphones red pull-right"></i></a>'; 
			*/
			message += "</b></td></tr><tr><td><a href='"+friend_geojson[user]["current_song_url"];
			message += "' class='ajax-link' data-title='Amuzzin-"+friend_geojson[user]["current_song"]+"'> ♫ -";
			message += friend_geojson[user]["current_song"]+"</a></td></tr></table>";
		}

		popup = marker.bindPopup(message).openPopup();
	}
}

function locate_friend(id)
{	
	if(friend_map == undefined)
	show_friends_in_map();
	$('#map').show();
		
	user_icon = friend_geojson[id]["img"],

	friend_map.setView([parseFloat(friend_geojson[id]["lat"]),parseFloat(friend_geojson[id]["lng"])],13);
	//friend_map.redraw();
	window.scrollTo(0,0);

	marker= L.marker([parseFloat(friend_geojson[id]["lat"]),parseFloat(friend_geojson[id]["lng"])]).addTo(friend_map);
	user_icon = friend_geojson[id]["img"];
	message = '';
	message += '<table class="table table-condensed table-bordered"><tr><td rowspan=2>'
	message += "<img class='small-image' src='"+user_icon+"' width='35px' height='35px' class='pull-right'/></td>"
	message += "<td><b>"+friend_geojson[id]["name"];
	/*message += '<a href="javascript:connect_with_friend('+id+')">';
	message += '<i class="icon icon-headphones red pull-right"></i></a>';*/
	message += "</b></td></tr><tr><td><a href='"+friend_geojson[id]["current_song_url"];
	message += "' class='ajax-link' data-title='Amuzzin-"+friend_geojson[id]["current_song"]+"'> ♫ -"
	message += friend_geojson[id]["current_song"]+"</a></td></tr></table>";

	marker.bindPopup(message).openPopup();
	
}

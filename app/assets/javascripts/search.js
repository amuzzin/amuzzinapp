$(document).ready(function() {
	$('#search-text').autocomplete({
		delay : 500,
		minLength : 3,
		source : '/utility/autocomplete',
		messages : {
			noResults : '',
			results : function() {
			}
		},
		focus : function(event, ui) {
			$('#search-text').val(ui.item.name);
			return false;
		},
		select : function(event, ui) {
			$('#search-text').val('');
			put_state(ui.item.url, 'Amuzzin-'+ui.item.name, ui.item.url);
		}
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		return $("<li></li>").data("ui-autocomplete-item", item).append("<a><div style='height:35px;'><img class='small-image auto-image' src='" + item.picture + "' /><span style='position:absolute;left:50px;height:20px;overflow:hidden;float:right;margin-top:7px;'>" + item.name +"</span><span style='position:absolute;right:10px;height:15px;float:right;margin-top:7px;'> (" + item.type + ")</span></div><div ></a>").appendTo(ul);
	};
	$("#search-form").submit(function() {
		if ($('#search-text').val() == '') {
			return false;
		} else {
			put_state('/utility/search_results?query=' + $('#search-text').val(), 'Amuzzin-Search Results', '/utility/search_results?query=' + $('#search-text').val());
		}
	});
}); 
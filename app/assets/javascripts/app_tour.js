 function startIntro(){
        var intro = introJs();
          intro.setOptions({
            steps: [
              {
                element: document.querySelector('#main-nav-bar-all'),
                intro: "This main bar at the top follows wherever you go. It is fixed so that you can access your music controls while moving between actions.",
              },
              {
                element: document.querySelector('#user_name'),
                intro: "This denotes the current Logged in User.If its not you please logout from facebook and loggin into your Facebook account",
                position: 'bottom'
              },
              {
                element: document.querySelector('#middle-container'),
                intro: "This is the main Music Player. All your required music actions like play / pause / stop / seek / volume (+) or (-) will appear here for your conveneince",
              },
                            {
                element: document.querySelector('#sidebar-menu'),
                intro: "These are the list of Actions you can perform",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#songs'),
                intro: "These are the list of Songs you have uploaded. You can come back any time and listen your songs.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#upload'),
                intro: "To start with , you need to upload songs from your local. Please make sure you own the rights of the song to distribute.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#cuts'),
                intro: "Cuts are Important feature and backbone of amuzzin. Your favourite clipping you cut from main song will appear here.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#shared'),
                intro: "Shared cuts are different from my cuts. If you share a clipping of a song it would appear in My Cuts. If some of your friends shares a song with you it would appear here.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#favourite'),
                intro: "Hate playlists ? Love a song on top of everything else in the world ? Just favourite it and it would appear here :)",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#playlist'),
                intro: "How good it would be if you can organize your songs based on your taste ? Just go to any of your song and click on playlist icon. Choose the playlist to tag and it comes here.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#friend'),
                intro: "Did we mention we integrate your facebook users automatically ? How cool is that ? Click on this tab and we will pull out what your friends are listening to. Don't thank me , thats why I'm here for :)",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#playlist1'),
                intro: "Sometimes we may go messy rite ? Thats why we have queues. Add any song from anywhre you see on the page and it will automatically get queued here.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#search-bar'),
                intro: "We don't need any explanation why we need a search. But this search does things beyond you think. Enter any random lyrics of a song or album name or even your friends name. We will find it for you and will return you the results. Please click on appropriate tab of search results to view all the results.",
                position: 'bottom'
              },
                            {
                element: document.querySelector('#my-profile-link'),
                intro: "You can view how your profile looks in Public searches by clicking here.",
                position: 'bottom'
              }
            ]
          });

          intro.start();
      }
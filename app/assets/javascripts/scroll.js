var lastScrollLeft = -5;
var initialized_scroll = false;
var side_menu_scroll;
var current_div_scroll;
var current_content_scroll;
var nav_bar_scroll;
var hor_scroll;
var main_player_scroll;

function scroll_logic()
{
	if(initialized_scroll)
	{
		//height of left panel
		var queue_height = side_menu_scroll.height();
		//height of right panel
		var current_song_height = current_div_scroll.height();
		//height of content div
		var content_height = current_content_scroll.height();
		var window_height = window.innerHeight + window.pageYOffset-36;
		//space occupied by contents at the top
		var content_top = 100;
		var queue_top = 50;
		var current_song_top = 56;

		//horizontal scroll
		var documentScrollLeft = $(document).scrollLeft();
		if (lastScrollLeft != documentScrollLeft) {
			lastScrollLeft = documentScrollLeft;
			hor_scroll.css({
				'margin-left':(-1 * documentScrollLeft)+'px',
			});
			main_player_scroll.css({
				'margin-left':(-1 *(documentScrollLeft+4))+'px'
			});
		}
		//vertical scroll
		if (content_height < window.innerHeight-100) {
			current_content_scroll.css('bottom', '');
			current_content_scroll.css({
				'position' : 'fixed',
				'margin-top' : '0px',
				'margin-left': (-1 * documentScrollLeft)+'px'
			});
			
		} else if (window_height-content_top >= content_height && content_height < Math.max(queue_height,current_song_height)) {
			current_content_scroll.css('top', '');
			current_content_scroll.css({
				'position' : 'fixed',
				'bottom' : '0px',
				'margin-left': (-1 * documentScrollLeft)+'px'
			});
		} else {
			current_content_scroll.css('top', '');
			current_content_scroll.css({
				'position' : 'static',
				'bottom' : '0px',
				'margin-left': '0px'
			});
		}

		if (queue_height < window.innerHeight-queue_top) {
			side_menu_scroll.css('bottom', '');
			side_menu_scroll.css({
				'position' : 'fixed',
				'top' : '36px',
				'margin-left': (-1 * documentScrollLeft)+'px'
			});
		} else if (window_height-10 >= queue_height && queue_height < Math.max(content_height,current_song_height)) {
			side_menu_scroll.css('top', '');
			side_menu_scroll.css({
				'position' : 'fixed',
				'bottom' : '0px',
				'margin-left': (-1 * documentScrollLeft)+'px'
			});
		} else {
			side_menu_scroll.css('top', '');
			side_menu_scroll.css({
				'position' : 'static',
				'bottom' : '0px',
				'margin-left': '0px'
			});
		}

		if (current_song_height < window.innerHeight-current_song_top) {
			current_div_scroll.css('bottom', '');
			current_div_scroll.css({
				'position' : 'fixed',
				'top' : '56px',
				'margin-left': 10 + (-1 * documentScrollLeft)+'px'
			});
			$('#current_song').css({'margin-left': (-1 * documentScrollLeft)+'px'});
		} else if (window_height-20 >= current_song_height && current_song_height < Math.max(content_height,queue_height)) {
			current_div_scroll.css('top', '');
			current_div_scroll.css({
				'position' : 'fixed',
				'bottom' : '0px',
				'margin-left': 10 + (-1 * documentScrollLeft)+'px'
			});
			$('#current_song').css({'margin-left': (-1 * documentScrollLeft)+'px'});
		} else {
			current_div_scroll.css('top', '');
			current_div_scroll.css({
				'position' : 'static',
				'bottom' : '0px',
				'margin-left': '10px'
			});
			$('#current_song').css({'margin-left': '0px'});
		}
	}
	
}

function initialize_scroll_variables()
{
	side_menu_scroll = $('#side-menu');
	current_div_scroll = $("#current_song");
	nav_bar_scroll = $('.navbar-inner');
	hor_scroll = $('.hor-scroll');
	main_player_scroll = $('#player1');
	initialized_scroll = true;
}
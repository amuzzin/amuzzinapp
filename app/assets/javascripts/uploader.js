//For Uploader
$(document).ready(function() {

	$('body').on('hidden', '#youtubeModal', function() {
		$('#signin_dropdown').click();
	});

	$('body').on('click', '#showlogin', function() {
		$('#youtubeModal').modal('hide');
		$('#signin_dropdown').dropdown();
	});
});

function load_existing_files() {
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url : $('#fileupload').fileupload('option', 'url'),
		dataType : 'json',
		context : $('#fileupload')[0]
	}).done(function(result) {
		$(this).fileupload('option', 'done').call(this, null, {
			result : result
		});
		$('#loading').remove();
	});
}

//Flag to indicate if the page is loaded newly
var isLoadedNew = false;
//the last active tab
var tab;
//preloaded images
var myimages = new Array();
//Queue of ajax urls
var ajax_queue = [];
//Current active container
var current_container = '#content';
//page title
var page_title = "Amuzzin - Socializing Music Experience";
//search bar toggle status
var search_toggle = false;
//active current song div
var current_div = "#current_song";
//Should refresh activity content or not
var refresh_activity = true;
var unity = '';
var window_focus = true;
var title_timer = null;

function initialize() {
	//If user uses facebook link then sign him in using fb
	if($('#signed_in_flag').length<=0 && document.referrer.indexOf('facebook.com')!=-1)
	{
		window.location.href="/auth/facebook";
	}
	title_timer = window.setInterval(function() {
		if(group_chat_title_enabled)
		{
			document.title = (document.title == group_chat_title) ? History.getState().title : group_chat_title
		}
		else if(player_objects[0].status ==1)
		{
			audio = player_objects[0].current_audio_js;
			time = humanize(audio.currentTime)
			document.title = "▶ " + time + ' - ' + audio.source.getAttribute('data-name') + " - amuzzin";
		}
		else
			document.title = History.getState().title;

	},1000);

	// prevent caching of ajax calls
	$.ajaxSetup({
		cache : false
	});
	(function(window, undefined) {

		//For history.js
		// Prepare
		var History = window.History;
		// Note: We are using a capital H instead of a lower h
		if (!History.enabled) {
			// History.js is disabled for this browser.
			// This is because we can optionally choose to support HTML4 browsers or not.
			return false;
		}

		// Bind to StateChange Event
		History.Adapter.bind(window, 'statechange', function() {// Note: We are using statechange instead of popstate
			var State = History.getState();
			// Note: We are using History.getState() instead of event.state
			var url = State.data.state;
			check_and_load(url);
		});

	})(window);
	// hide it initially
	$('#loading-div').hide();
	//Action for ajax links
	$('body').on('click', '.ajax-link', function(e) {
		e.preventDefault();
		if (History.getState().data.state != $(this).attr('href'))
			put_state($(this).attr('href'), $(this).attr('data-title'), $(this).attr('href'));
		else {
			check_and_load($(this).attr('href'));
		}
	});
	$('body').on('click', '.ajax-comment-link', function(e) {
		e.preventDefault();
		$("#current-song-container").load($(this).attr('href'));
		return false;
	});
	$('body').on('click', '.nav-tabs a', function(e) {
		tab = this.id;
	});

	$(document).ajaxStop(function() {
		//$('#loading-div').hide();
		if (isLoadedNew) {
			//Hide loader and show page after everything has loaded
			$('#page-load').hide();
			$('#main-content').css('visibility', 'visible');
			isLoadedNew = false;
			initialize_scroll_variables();
			scroll_logic();
			$('.special-ellipsis').dotdotdot({
				wrap : 'letter'
			});
			$('#main-playlist-menu-button').click(function() {
			var el=$(this).siblings('.playlist-menu')[0];
			$(el).html('<center><img src="/loading.gif"></center>');
			if($('#signed_in_flag').length > 0)
			{
				if(player_objects[0].position>-1)
				{
					$(el).load('/playlists/available_playlists/'+$(player_objects[0].current_audio).attr('data-id'),function(){
						$($(el).children('div')[0]).slimScroll({
							height : '150px',
							alwaysVisible : true,
							railVisible : true
						});
					});
				}	
				else
					$(el).html('<div style="margin-left:5px">Play a song to add it to playlist</div>');
			}
			else
			{
				$(el).html('<div style="margin-left:5px">Please sign in at the top right corner to add the current song to your playlists</div>');
			}
			
			});
			$('#take_tour_button').on('click', function(e) {
				  e.preventDefault();
				  document.body.scrollTop = document.documentElement.scrollTop = 0;
					introJs().start();
			});
			$('#flash').fadeIn('normal', function() {
				$(this).delay(3500).fadeOut();
			});

			load_tour();
			// Load all external services after everything has loaded
			//facebook
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=210651542320460";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			//twitter
			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
			//maps
			$.getScript("http://cdn.leafletjs.com/leaflet-0.6.4/leaflet.js", function(){
			   console.log("maps loaded.");
			 });
			//Google analytics
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-47140309-1', 'amuzz.in');
			  ga('send', 'pageview');

			 //soundcloud
			$.getScript("//connect.soundcloud.com/sdk.js", function(){
				 SC.initialize({
			    client_id: "e876623b522c99579f380ce3b0a18c11",
			  });

			   console.log("soundcloud loaded.");
			 });

			if(chrome && chrome.app && !chrome.app.isInstalled)
				$('#chrome_app_installation').show();
		

		}

	});

	load_player(1);
	$(window).focus(function() {
	    window_focus = true;
	});

	$(window).blur(function() {
	    window_focus = false;
	    $('#send_chat_to_group_box').blur();
	});
	$("#volume1").hide();
	$("#volume-container").mouseout(function() {
		$("#volume1").hide();
	});
	$("#volume-container").mouseover(function() {
		$("#volume1").show();
	});

	$('body').on('click', '.popup-close', function(event) {
		$('#bpopup-div').bPopup().close();
	});
	$("#page-loader").animate({width:"25%"},200);

	//restore queue
	if ($('#signed_in_flag').length > 0)// Check if user has signed in
	{
		if ($('#group_listen_enabled').length > 0)
		{
			player_objects[0].enable_group_listen();
			load_playlist(1, 0);
			$("#page-loader").animate({width:"50%"},200);
			load_page();
		}	
		else
		{
			//Restore queue from cache
			invoke_controller_action('', 'GET', '/queue/get_current_queue', 'load_playlist(1, 0);$("#page-loader").animate({width:"50%"},200);load_page();');
		}
	} else {
		//Restore queue from cookie
		if (read_queue_from_cookie() == "") {
			load_playlist(1, 0);
			enable_fav_button();
			load_page();
		} else {
			invoke_controller_action('', 'GET', '/queue/get_queue_from_cookie?queue=' + read_queue_from_cookie() + '&song=' + read_song_from_cookie(), 'load_playlist(1, 0);$("#page-loader").animate({width:"50%"},200);enable_fav_button();load_page();');
		}
	}

	$(window).scroll(function() {
		scroll_logic();
	})

	$(window).resize(function() {
		lastScrollLeft = -5;
  		scroll_logic();
	});

	bind_events_for_video_modal();
	//stop dropdown menu close on clicking inside it
    // $('body').on('click','.dropdown-menu',function(e) {
    //     e.stopPropagation();
    // });
    //close confirm dropdown on click
    $('body').on('click','.confirm-close',function(){
    	$(this).parents('.confirm-dropdown').removeClass('open');
    })

			$.fn.serializeObject = function() {
				var values = {}
				var id = this.attr('id');
				console.log(id);
				$("#" + id + " input," + "#" + id + " select," + "#" + id + " textarea").each(function() {
					values[this.name] = $(this).val();
				});

				return values;
			}
			//comment form
			$('body').on('submit','#new_comment',function(e) {
				form = this;
    			setTimeout(function() {
    				$(form).find('#comment_body').val('');
  				},100);
			});
			//playlist form
			$('body').on('submit','.playlist-form',function(e) {
				form = this;
    			setTimeout(function() {
    				$(form).find('.playlist-name').val('');
  				},100);
			});

	
}

function load_page() {
	//Get url and see if there is a '?' to check if a different page is requested else load home as default
	var path = window.location.href.split(window.location.host)[1];
	path = path.replace('#_=_','');
	if (path == "/") {
		//To load home initially when the page loads
			put_state("/home", "Amuzzin-Home", "/home");
	} else {
		previous_url = History.getState().data.state;
		if (previous_url == path) {
			check_and_load(path);
		} else
			put_state(path, 'Amuzzin-Socializing Music Experience', path);
	}
	$("#page-loader").animate({width:"100%"},200);
	//Set isLoadedNew
	isLoadedNew = true;
}

function check_and_load(url) {
	//If uploader or feed is requested, show the div if it has already been loaded. Else load it
	console.log(url);
	if(typeof url != 'undefined')
	{
	$(current_container).stop().fadeTo('fast','0.3',function(){});
	if (url == '/upload') {
		current_container="#upload-song-content";
		callback="current_content_scroll = $($(current_container).children()[0]);$('#content').hide();$('#activity-feed-content').hide();$('#upload-song-content').css('opacity', '0.3');$('#upload-song-content').css('visibility', 'visible');$(current_container).stop().fadeTo('fast','1',function(){});";
		if ($('#upload-song-content').is(':empty')) {
			load_url(url,callback);
		}
		else
		{
		eval(callback);
		}
		
	} else if (url == '/activities') {
		current_container="#activity-feed-content";
		callback="current_content_scroll = $($(current_container).children()[0]);$('#content').hide();$('#upload-song-content').css('visibility', 'hidden');$('#activity-feed-content').css('opacity', '0.3');$('#activity-feed-content').show();$(current_container).stop().fadeTo('fast','1',function(){});";
		if (refresh_activity) {
			load_url(url,callback);
			refresh_activity = false;
		}
		else
		{
			eval(callback);
		}
		
		
	} else {
		current_container="#content";
		callback="current_content_scroll = $($(current_container).children()[0]);$('#upload-song-content').css('visibility', 'hidden');$('#activity-feed-content').hide();$('#content').css('opacity', '0.3');$('#content').show();$(current_container).stop().fadeTo('fast','1',function(){});";
		load_url(url,callback);
	}
	}
}

function load_url(url,callback) {
	if (url != undefined) {
		invoke_controller_action("", 'GET', url, callback);
	}
}


function invoke_controller_action(pre_action, method, url, post_action) {
	if (ajax_queue.indexOf(url) == -1) {
		ajax_queue.push(url);
		eval(pre_action);
		$.ajax({
			type : method,
			url : url,
			dataType : 'script',
			error : function(x, t, m) {
				ajax_queue.splice(ajax_queue.indexOf(url), 1);
				show_flash('Some error occured. Please try again',true);

			},
			success : function(resp) {
				ajax_queue.splice(ajax_queue.indexOf(url), 1);
				eval(post_action);
			}
		});
	}
}

//for storing current state in window history
function put_state(_state, _title, _url) {
	if(_url == get_current_url())
	{
		load_url(_url,"");
	}
	else
	{
		page_title = _title;
		History.pushState({
			state : _state
		}, _title,_url);
	}
}

function load_content(content_id, param) {
	if (content_id == "current") {

		if (player_objects[0].position != -1) {
			$("#current-song-container").load("/utility/current_song?id=" + document.getElementById(player_objects[0].playlist[player_objects[0].position]).getAttribute('data-id'));

		} else {
			$("#current-song-container").load("/utility/current_song?id=0");

		}
	} else if (content_id == "fb_share") {
		$("#content").load("/upload_song/fb_share?cid=" + param);
	} else if (content_id == "publish_cut") {
		$("#content").load("/ct/" + param);
	}
}

function load_tab_url(url, active) {
	if (url != undefined) {
		script = "$('" + active + "')" + ".tab('show')";
		invoke_controller_action('', 'GET', url, script);
	}
}

function get_current_url(){
	//var url = decodeURIComponent(window.location.href.toString().split(window.location.host)[1]);
	var url = History.getState().data.state;
	return url;
}

function refresh_current_page() {
	load_url(get_current_url(),"");
}

//preload images

function preloadimages() {
	for ( i = 0; i < preloadimages.arguments.length; i++) {
		myimages[i] = new Image();
		myimages[i].src = preloadimages.arguments[i];
	}
}

function update_cookie(action, value, object_type) {
	//update cookie only when signed out
	if ($('#signed_in_flag').length <= 0) {
		if (object_type == "queue") {
			if (action == "clear")
				$.cookie("QUEUE", "", {
					expires : 9999
				});
			else if (action == "add") {
				var queue = [];
				if ($.cookie("QUEUE")) {
					queue = unescape($.cookie("QUEUE")).split(',');
				}
				if (queue.indexOf(value) == -1) {
					queue.push(value);
					$.cookie("QUEUE", escape(queue.join(',')), {
						expires : 9999
					});
				}
			} else if (action == "mass_add") {
				var queue = [];
				if ($.cookie("QUEUE")) {
					queue = unescape($.cookie("QUEUE")).split(',');
				}
				queue = arrayUnique(queue.concat(value));
				$.cookie("QUEUE", escape(queue.join(',')), {
					expires : 9999
				});
			} else if (action == "remove") {
				var queue = [];
				if ($.cookie("QUEUE")) {
					queue = unescape($.cookie("QUEUE")).split(',');
				}
				index = queue.indexOf(value);
				if (index != -1) {
					queue.splice(queue.indexOf(value), 1);
					$.cookie("QUEUE", escape(queue.join(',')), {
						expires : 9999
					});
				}
			}
		} else if (object_type == "song") {
			if (action == "clear")
				$.cookie("SONG", "", {
					expires : 9999
				});
			else if (action == "update") {
				$.cookie("SONG", value, {
					expires : 9999
				});
			}
		}
	}
}

function read_queue_from_cookie() {
	if ($.cookie('QUEUE') === undefined)
		return "";
	else {
		cookie = unescape($.cookie('QUEUE'));
		return cookie;
	}
}

function arrayUnique(array) {
	var a = array.concat();
	for (var i = 0; i < a.length; ++i) {
		for (var j = i + 1; j < a.length; ++j) {
			if (a[i] === a[j])
				a.splice(j--, 1);
		}
	}

	return a;
};

function read_song_from_cookie() {
	if ($.cookie('SONG') === undefined)
		return "";
	else {
		cookie = unescape($.cookie('SONG'));
		return cookie;
	}
}

function refresh_current_song_page(id)
{
	$('#current-song-container').load('/utility/current_song?id='+id);
}

function toggle_searchbar(action)
{
	if(action == 'hide')
	{
		search_toggle = true;
		$('#search-bar').hide();
		$('#content').css('top','45px');
		$('#upload-song-content').css('top','45px');
		$('#activity-feed-content').css('top','45px');
		$('#search_icon').show();
	}
	else if(action == 'show')
	{
		search_toggle = false;
		$('#search_icon').hide();
		$('#content').css('top','83px');
		$('#upload-song-content').css('top','83px');
		$('#activity-feed-content').css('top','83px');
		$('#search-bar').show();
		$('#search-bar').focus();
	}
}

function play_animoto_video()
{
	$('#video-title').html(decodeURI('amuzz.in - Amuzzin-Socializing Music Experience'));
	$('#modal-content').html('<iframe id="vp1km0Pn" title="Video Player" width="540" height="303" frameborder="0" src="https://s3.amazonaws.com/embed.animoto.com/play.html?w=swf/production/vp1&e=1389640514&f=km0PnyQ8f3go1AhyxHXucg&d=0&m=b&r=360p&volume=100&start_res=360p&i=m&asset_domain=s3-p.animoto.com&animoto_domain=animoto.com&options=autostart" allowfullscreen></iframe>');
	$('#videoModal').modal('show');
}

function disable_button(el_id)
{
  element = $("#"+el_id);
  element.addClass("disabled");
  element.html('<i class="icon icon-facebook" style="font-size:16px"></i><span class="divider-vertical"></span>Loading please wait ...');
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function load_once_div(div_id,url)
{
	$('#'+div_id).toggle();
	if($('#'+div_id).html() == "")
	{
		$('#'+div_id).html('<center><img src="/loading.gif"></center>');
		$('#'+div_id).load(url);
	}
}

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

//Enter path of images to be preloaded inside parenthesis. Extend list as desired.
//preloadimages("/images/player-sb48613c24d.png", "/images/curved-right.png","/images/play.png", "/images/pause.png", "/images/play-mini.png", "/images/pause-mini.png", "/images/stop.png", "/images/next.png", "/images/previous.png", "/images/volume.png", "/images/mute.png", "/images/favourite.png", "/images/add_to_playlist.png", "/pause.png", "/play.png", "/utube.png", "/images/facebook.jpg", "/logo.png");
//preloadimages("/loader.gif", "http://cdn.amuzz.in/assets/handle.png");
$(document).ready(initialize);


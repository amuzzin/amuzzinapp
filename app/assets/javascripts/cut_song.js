var audioElement;
$(document).ready(function() {
$('body').on('click','.cutpopup', function(e) {
e.preventDefault();
var id = $(this).attr('id').replace('cutpopup_','');
$('#emptydiv').bPopup({
opacity: 0.8,
loadUrl: "/share_service/popup_cutsong?id="+id,
onClose: function() {
    play=0,audioElement.pause();
},
onOpen: function() {
$('#emptydiv').html('<div style="min-height:inherit;min-width:inherit;background:url(\'/loading.gif\') no-repeat center center;"></div>');		
}
});
});

$('body').on('click','.bpopup', function(e) {
e.preventDefault();
var height = $(this).attr('data-height');
var width = $(this).attr('data-width');
$('#bpopup-div').css('height',height+'px');
$('#bpopup-div').css('min-width',width+'px');
$('#bpopup-div').bPopup({
opacity: 0.8,
onClose: function() {
   $('#bpopup-div').html('');
},
loadUrl: $(this).attr('data-url'),
onOpen: function() {
$('#bpopup-div').html('<div style="height:inherit;min-width:inherit;background:url(\'/loading.gif\') no-repeat center center;"></div>');	
}	
});
});
});



function change_tags(title,image,id)
{
	$('.og-tags').remove();
	$('head').append('<meta class="og-tags" property="og:image" content="'+image+'"/><meta class="og-tags" property="og:url" content="http://amuzz.in/s/'+id+'"/><meta class="og-tags" property="og:title" content="'+title+'"/><meta class="og-tags" property="og:description" content="Amuzzin - Socializing music experience"/>');
}
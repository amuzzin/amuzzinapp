//array holding player objects
var player_objects = [];
var id = 1;
//This maps player id with actual position of player in player_objects array
var map = {};
function player() {

	this.playlist = [];
	//Current song position
	this.position = -1;
	//Status: 0:song paused, 1:song playing
	this.status = 0;
	//variable used to keep track of volume button(muted or not)
	this.muted = false;
	//variable used to store the existing volume when volume is muted so that same volume can  be retrieved when unmuted
	this.volumestore = 0.6;
	this.first = 0;
	//option: 0:add playlist button, 1:play selected button
	this.option = 1;
	//player id
	this.pid;
	//id of setinterval
	this.sid;
	//variable used by updatetime function to see if song is playing or buffering
	this.isBuffering;
	//It temporarily stores the sav as playlist id for adding songs in the queue to it
	this.save_as_playlist_id;
	//Variables used when audio error occurs
	this.resetvolume;
	this.resettime;
	//current audio element being played
	this.current_audio;
	//current audio js instance
	this.current_audio_js;
	//Loop: true-on, false-off default is false
	this.loop_value = false;
	//Shuffle: true-on, false-off default is false
	this.shuffle_value = false;
	//Shuffled playlist
	this.original_playlist = [];
	//flag to show if a original playlist exists or not
	this.original_playlist_exists = false;
	//It identifies what players need to be resumed
	this.resume_flag = 0;
	//Current time of current song
	this.current_time = 0;
	//favourited
	this.favourited = false;
	//Group listen
	this.group_listen = false;
	//backup during group listen
	this.group_listen_backup = [];
	//enable socket messages
	this.socket_enabled = true;

	// keep reference to be used at several places
	var me = this;

	this.play_song = function(el) {
		try
		{
			el.play();	
		}
		catch(e)
		{
			me.current_audio.addEventListener('loadstarted',function(){
				el.play();
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
		}

	};

	this.pause_song = function(el) {
		var el1 = '#playtitle' + me.pid;
		try
		{
			el.pause();	
		}
		catch(e)
		{
			me.current_audio.addEventListener('loadstarted',function(){
				el.pause();
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
		}
		$(el1).text(me.current_audio.getAttribute('data-name'));
	};

	this.stop_all_other = function() {
		//to stop all other running players
		for (var y = 0; y < player_objects.length; y++) {
			if ((player_objects[y].status == 1) && (y != map[me.pid] - 1)) {
				player_objects[y].play_button();
				//var el = document.getElementById(player_objects[y].playlist[player_objects[y].position]);
				player_objects[y].pause_song(player_objects[y].current_audio_js);
			}
		}
	};

	this.play_selected = function(song_id) {
		var audio = document.getElementById(song_id);
		// //Remove the listener if any so that only one event listener for 'error' exists
		// audio.removeEventListener('error', me.resetaudio, false);
		// audio.addEventListener('error', me.resetaudio, false);
		me.option = 1;
		me.stop_all_other();


		var search = me.search_playlist(song_id);
		if (me.pid == 1) {
			var before = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
			$(before).attr('class','link link-text');
		}
		if (search == -1) {
			if (me.pid == 1) {
				if ($('#signed_in_flag').length > 0 && me.socket_enabled)
				{
					//invoke_controller_action('', 'POST', "/queue/add_to_current_queue?id=" + document.getElementById(song_id).getAttribute('data-id'), '');
					socket.emit('add_to_queue', {token: $.cookie("_validation_token_key"), song: document.getElementById(song_id).getAttribute('data-id'), group_listen: me.group_listen });
				}
				else if(!($('#signed_in_flag').length >0))
				{
				update_cookie("add",document.getElementById(song_id).getAttribute('data-id'),"queue");
				}
			}
			me.playlist.push(song_id);
			if (me.original_playlist_exists)
				me.original_playlist.push(song_id);
			me.position = me.playlist.length - 2;
			if(me.playlist.length == 1)
				me.load_playlist(0);
			else
				me.load_partial_playlist(me.playlist.length-1,me.playlist.length-1);
			//start playing selected
			if (me.playlist.length > 0) {
				me.pause_button();
				// add 'ended' listener to all songs
				
			} else
				me.play_button();
			me.nextSong();
		} else {
			me.pause_button();
			me.playChosen(me.playlist[search]);
		}

	};

	this.next = function() {
		me.nextSong();
	};

	this.end_play = function() {
		me.play_button();
		me.reset(me.current_audio_js);
	};

	this.reset = function(audio) {
		//reset everything
		var el = '#progress' + me.pid;
		var el1 = '#time' + me.pid;
		$(el).width(0);
		try {
			audio.skipTo(0);
		} catch(e) {
			audio.source.addEventListener('loadstarted',function(){
				this.skipTo(0);;
				this.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
		}
		$(el1).html('00:00');
	};

	this.add_to_playlist = function(song_id) {
		var audio = document.getElementById(song_id);
		// //Remove the listener if any so that only one event listener for 'error' exists
		// audio.removeEventListener('error', me.resetaudio, false);
		// audio.addEventListener('error', me.resetaudio, false);
		var flag = 0
		if (me.status == 1)
			me.option = 1;
		else
			me.option = 0;
		if (me.playlist.length == 0)
			flag = 1;
		//add songs to playlist
		if (me.search_playlist(song_id) == -1) {
			if (me.pid == 1) {
				//Add to current playlist
				if ($('#signed_in_flag').length > 0 && me.socket_enabled)
				{
					//invoke_controller_action('', 'POST', "/queue/add_to_current_queue?id=" + document.getElementById(song_id).getAttribute('data-id'), '')
					socket.emit('add_to_queue', {token: $.cookie("_validation_token_key"), song: document.getElementById(song_id).getAttribute('data-id'), group_listen: me.group_listen });
				}
				else if(!($('#signed_in_flag').length > 0))
				{
				update_cookie("add",document.getElementById(song_id).getAttribute('data-id'),"queue");
				}
			}
			me.playlist.push(song_id);
			if (me.original_playlist_exists)
				me.original_playlist.push(song_id);
			if(me.playlist.length == 1)
				me.load_playlist(0);
			else
				me.load_partial_playlist(me.playlist.length-1,me.playlist.length-1);
		}

		if (flag == 1)
			me.nextSong();
		
	};
	this.nextSong = function() {
		var el = "#next" + me.pid;
		var el1 = "#playtitle" + me.pid;
		var prev_pos = me.position;
		if (me.playlist.length > 0) {
			if (me.position > -1) {
				me.stop();
				me.stop_buffering();
				if (me.pid == 1) {
					var before = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
					$(before).attr('class', 'link link-text');
				}
			}
			if (me.position + 1 == me.playlist.length)
				me.position = 0;
			else
				me.position++;
			me.start_buffering();
			if (me.pid == 1) {
				var after = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
				$(after).attr('class','link link-text song-indicator');
			}
			me.setCurrentAudio();
			try
			{
				me.setVolumeForTrack();	
			}
			catch(e)
			{
				me.current_audio.addEventListener('loadstarted',function(){
				me.setVolumeForTrack();
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
			}
			if (me.status == 1) {
				$(el1).text('Buffering...');
			} else {
				$(el1).text(me.current_audio.getAttribute('data-name'));

			}

			if (me.pid == 1 && prev_pos!=me.position) {
				if ($('#signed_in_flag').length > 0 && me.socket_enabled)
				{
				//invoke_controller_action('', 'POST', "/utility/set_curr_song_and_fav_elig?id=" + me.current_audio.getAttribute('data-id'), '');
				socket.emit('set_curr_song', {token: $.cookie("_validation_token_key"), song: me.current_audio.getAttribute('data-id'), name: me.current_audio.getAttribute('data-name'), group_listen: me.group_listen, duration: me.current_audio_js.duration, pic:  me.current_audio.getAttribute('data-art'), media_url:  me.current_audio.getAttribute('data-src'), album:  me.current_audio.getAttribute('data-album') });
				}
				else if(!($('#signed_in_flag').length > 0))
				{
				update_cookie("update",me.current_audio.getAttribute('data-id'),"song");
				}
				$("#current-song-container").load("/utility/current_song?id=" + me.current_audio.getAttribute('data-id'));
				//$("#related-songs-container").load("/utility/related_songs?id=" + me.current_audio.getAttribute('data-id'));
				
			}

			if (me.position > -1 && me.status == 1 && me.option == 1) {
				me.play_song(me.current_audio_js);
			}
		} else {
			$(el1).html("Nothing to play");

		}
	};
	//To play randomly selected song from playlist
	this.playChosen = function(id) {
		var el1 = "#playtitle" + me.pid;
		me.option = 1;
		me.stop_all_other();
		song_index = me.search(me.playlist,id);
		// to start a new loop, stop currently playing song
		var prev_pos=me.position;
		if (me.position > -1) {
			me.stop();
			me.stop_buffering();
			if (me.pid == 1) {
				var before = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
				$(before).attr('class','link link-text');

			}
		}
		me.pause_button();

		me.position = song_index;
		me.start_buffering();
		if (me.pid == 1) {
			var after = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
			$(after).attr('class','link link-text song-indicator');
		}
		me.setCurrentAudio();
		try
		{
			me.setVolumeForTrack();	
		}
		catch(e)
		{
			me.current_audio.addEventListener('loadstarted',function(){
			me.setVolumeForTrack();
			me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  	},false);
		}
		if (me.pid == 1 && prev_pos != me.position) {
			if(prev_pos != -1)
			{
			if ($('#signed_in_flag').length > 0 && me.socket_enabled)
			{
			//invoke_controller_action('', 'POST', "/utility/set_curr_song_and_fav_elig?id=" + me.current_audio.getAttribute('data-id'), '');

				socket.emit('set_curr_song', {token: $.cookie("_validation_token_key"), song: me.current_audio.getAttribute('data-id'), name: me.current_audio.getAttribute('data-name'), group_listen: me.group_listen, duration: me.current_audio_js.duration, pic:  me.current_audio.getAttribute('data-art'), media_url:  me.current_audio.getAttribute('data-src'), album:  me.current_audio.getAttribute('data-album') });
			}
			else if(!($('#signed_in_flag').length > 0))
			{
			update_cookie("update",me.current_audio.getAttribute('data-id'),"song");
			}
			}		
			$("#current-song-container").load("/utility/current_song?id=" + me.current_audio.getAttribute('data-id'));
			//$("#related-songs-container").load("/utility/related_songs?id=" + me.current_audio.getAttribute('data-id'));
		}
		$(el1).text('Buffering...');
		me.play_song(me.current_audio_js);
	};

	this.setCurrentAudio = function() {
		me.current_audio = document.getElementById(me.playlist[me.position]);
	};
	this.setVolumeForTrack = function() {
		//set the volume of the song
		if (me.first == 0) {
			if (me.muted == false) {
				me.current_audio_js.setVolume(0.6);
				me.first++;
			} else {
				me.current_audio_js.setVolume(0);
			}
		} else {
			var vol = '#volume' + me.pid;
			if(me.pid==1)
			me.current_audio_js.setVolume ($(vol).slider('value') / 65);
			else
			me.current_audio_js.setVolume ($(vol).width() / 65);
		}
	};
	this.prevSong = function() {
		var el = "#previous" + me.pid;
		var el1 = "#playtitle" + me.pid;
		var prev_pos = me.position;
		if (me.playlist.length > 0) {
			if (me.position > -1) {
				me.stop();
				me.stop_buffering();
				if (me.pid == 1) {
					var before = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
					$(before).attr('class','link link-text');
				}
			}
			if (me.position - 1 < 0)
				me.position = me.playlist.length - 1;
			else
				me.position--;
			me.start_buffering();
			if (me.pid == 1) {
				var after = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
				$(after).attr('class','link link-text song-indicator');
			}
			me.setCurrentAudio();
			try
			{
				me.setVolumeForTrack();	
			}
			catch(e)
			{
				me.current_audio.addEventListener('loadstarted',function(){
				me.setVolumeForTrack();
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
			}
			if (me.status == 1) {
				$(el1).text('Buffering...');
			} else {
				$(el1).text(me.current_audio.getAttribute('data-name'));
			}

			if (me.pid == 1 && prev_pos!=me.position) {
				if ($('#signed_in_flag').length > 0 && me.socket_enabled)
				{
				//invoke_controller_action('', 'POST', "/utility/set_curr_song_and_fav_elig?id=" + me.current_audio.getAttribute('data-id'), '');
				socket.emit('set_curr_song', {token: $.cookie("_validation_token_key"), song: me.current_audio.getAttribute('data-id'), name: me.current_audio.getAttribute('data-name'), group_listen: me.group_listen, duration: me.current_audio_js.duration, pic:  me.current_audio.getAttribute('data-art'), media_url:  me.current_audio.getAttribute('data-src'), album:  me.current_audio.getAttribute('data-album') });

				}
				else if (!($('#signed_in_flag').length > 0))
				{
				update_cookie("update",me.current_audio.getAttribute('data-id'),"song");
				}
				$("#current-song-container").load("/utility/current_song?id=" + me.current_audio.getAttribute('data-id'));
				//$("#related-songs-container").load("/utility/related_songs?id=" + me.current_audio.getAttribute('data-id'));				
			}
			if (me.position > -1 && me.status == 1) {
				me.play_song(me.current_audio_js);
			}
		} else {
			$(el1).text("Nothing to play");

		}

	};
	
	this.start_buffering = function(){
		var audio = document.getElementById(me.playlist[me.position]);
		//$(audio).attr('src',$(audio).attr('data-src'));
		//create audiojs element
		  if($.cookie('quality') != undefined)
		  {
		  	data_src = $(audio).attr('data-src')+'_'+$.cookie('quality');
		  }
		  else
		  {
		  	data_src = $(audio).attr('data-src');
		  }
		  	data_src = $(audio).attr('data-src')+'_original';
    	me.current_audio_js = audiojs.create(audio);
    	me.current_audio_js.load(data_src);
    	document.getElementById(me.playlist[me.position]).addEventListener('trackended', me.decide_next_song, false);
		//audio.addEventListener('error', me.resetaudio, false);
		
  		
		
	};
	
	this.stop_buffering = function(){
		var audio = document.getElementById(me.playlist[me.position]);
		var new_audio = document.createElement('audio');
		//audio.removeEventListener('error', me.resetaudio, false);
		var attrs = $(audio).prop("attributes");
		$.each(attrs, function() {
			if(this.name!='src')
    		$(new_audio).attr(this.name, this.value);
		});
		document.getElementById('player' + me.pid).appendChild(new_audio);
		me.pause_song(me.current_audio_js);
		try
		{
		me.current_audio_js.load("");
		}
		catch(e)
		{
			me.current_audio.addEventListener('loadstarted',function(){
				me.current_audio_js.load("");
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
		}
		var audiojs_player = $(audio).parents('.audiojs'); 
		document.getElementById(me.playlist[me.position]).removeEventListener('trackended', me.decide_next_song, false);
		$(audio).remove();
		$(audiojs_player).remove();
		
	};
	//Increase the volume in one point
	this.incvolume = function() {
		var el = "#volume" + me.pid;

		var wid = parseInt($(el).width() * 100 / 30);
		if (wid < 100) {
			wid = wid + 10;
			if (wid > 100)
				wid = 100;
			$(el).width(parseInt(wid / (100 / 30)));
			if (me.position > -1) {
			try
			{
				me.current_audio_js.setVolume (wid / 100);	
			}
			catch(e)
			{
				me.current_audio.addEventListener('loadstarted',function(){
				me.current_audio_js.setVolume (wid / 100);
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
			}
				
			}
		}

	};
	//Decrease the volume in one point
	this.decvolume = function() {
		var el = "#volume" + me.pid;

		var wid = parseInt($(el).width() * 100 / 30);
		if (wid > 0) {
			wid = wid - 10;
			if (wid < 0)
				wid = 0;
			$(el).width(parseInt(wid / (100 / 30)));
			if (me.position > -1) {
			try
			{
				me.current_audio_js.setVolume (wid / 100);	
			}
			catch(e)
			{
				me.current_audio.addEventListener('loadstarted',function(){
				me.current_audio_js.setVolume (wid / 100);
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
			}
				
			}
		}
	};
	//Toggle play & pause
	this.play_button = function() {
		me.status = 0;
		var el = "#play" + me.pid;
		if (me.pid == 1)
			$(el).html('<i class="icon icon-play"></i>');
		else
			$(el).html('<span class="pl-mini player-play-mini" height="39" width="39"></span>');
	};

	this.pause_button = function() {
		me.status = 1;
		var el = "#play" + me.pid;
		if (me.pid == 1)
			$(el).html('<i class="icon icon-pause"></i>');
		else {
			$(el).html('<span class="pl-mini player-pause-mini" height="39" width="39"></span>');
		}
	};

	this.play_pause_song = function() {
		if (me.playlist.length > 0) {
			if (me.status == 1) {
				me.play_button();
				me.pause_song(me.current_audio_js);
			} else {
				me.pause_button();
				me.option = 1;
				me.play_song(me.current_audio_js);
				me.stop_all_other();
			}
		}
	};

	this.stop = function() {
		me.pause_song(me.current_audio_js);
		me.reset(me.current_audio_js);
	};

	this.stop_button = function() {
		if (me.position > -1) {
			var el = "#stop" + me.pid;
			me.stop();
			if (me.status == 1)
				me.play_button();
		}
	};
	this.search_playlist = function(id,flag) {
		var song;
		if(typeof flag != "undefined" && flag)
			song = id;
		else
			song = document.getElementById(id).getAttribute('data-id');
		for (var j = 0; j < me.playlist.length; j++) {
			if (document.getElementById(me.playlist[j]).getAttribute('data-id') == song) {
				return j;
			}
		}
		return -1;
	};

	this.search = function(playlist, id) {
		for (var j = 0; j < playlist.length; j++) {
			if (playlist[j] == id) {
				return j;
			}
		}
		return -1;
	};

	this.updateVolume = function(event) {
		var bar_width,x;
		if (me.pid == 1)
			bar_width = 65;
		else
			bar_width = 30;
		if(me.pid ==1)
		{
			x = $('#volume1').slider("value");
		}
		else
		{
		var el = "#volume" + me.pid;
		event = event || window.event;
		var prog = document.getElementById("volume" + me.pid);
		var offset = me.getPos(prog);
		x = event.pageX - offset;
		$(el).width(x);
		}
		if (me.position > -1) {
			try
			{
				me.current_audio_js.setVolume (x / bar_width);	
			}
			catch(e)
			{
				me.current_audio.addEventListener('loadstarted',function(){
				me.current_audio_js.setVolume (x / bar_width);
				me.current_audio.removeEventListener('loadstarted', arguments.callee, false);
	  		},false);
			}
			
		} else {
			//don't overwrite volume store when already muted
			if(x!=0)
			me.volume_store = x / bar_width;
		}
		if (me.pid == 1) {
			if (x == 0) {
				$("#volume-button" + me.pid).html('<button class="btn btn-default btn-player" onclick="mute(' + me.pid + ')" ><i class="icon icon-volume-off" style="font-size:20px"></i></button>');
				me.muted = true;
			} else {
				$("#volume-button" + me.pid).html('<button class="btn btn-default btn-player" onclick="mute(' + me.pid + ')" ><i class="icon icon-volume-off" style="font-size:20px"></i></button>');
				me.muted = false;
			}
		}
	};
	this.mute_volume = function() {

		var el = '#volume-button' + me.pid;
		var vol = '#volume' + me.pid;
		if (me.muted == false) {
			me.volumestore = ($(vol).slider('value'))/65;
			$(vol).slider('value',0);
			//$(el).html('<span class="pl-icon2 player-mute" height="30" width="30" onclick="mute(' + me.pid + ')"/>');
		} 
		else 
		{
			$(vol).slider('value',me.volumestore*65);
			//$(el).html('<span class="pl-icon2 player-volume" height="30" width="30" onclick="mute(' + me.pid + ')"/>');
		}

	};
	this.getPos = function(obj) {
		var output = 0;
		while (obj) {
			output += obj.offsetLeft;
			obj = obj.offsetParent;
		}
		return output;
	};

	this.updatetime = function(audio) {
		var el = "#time" + me.pid;
		time = humanize(audio.currentTime);
		$(el).text(time);
		if(me.pid==1)
		$("#duration1").text(humanize(audio.duration));

	};

	this.decide_next_song = function() {
		if(me.position != me.playlist.length-1)
		{
			me.next();
		}
		else
		{
			if(me.loop_value)
			{
				me.next();
			}
			else
			{
				me.end_play();
			}
		}
	}


	this.updatebar = function(event) {
		var bar_width;
		if (me.pid == 1)
			bar_width = 458;
		else
			bar_width = 235;
		var el = '#progress' + me.pid;
		var el1 = me.current_audio_js;
		if (el1.currentTime != 0) {
			if (me.position > -1) {
				event = event || window.event;
				var buff = document.getElementById("progressbar" + me.pid);
				var offset = me.getPos(buff);
				var x = event.pageX - offset;
				$(el).width(x);
				el1.skipTo(x/ bar_width);
				me.updatetime(el1);
			}
		}
	};
	this.load_playlist = function(flag) {
		var el = "#playlist" + me.pid;
		var value = '<div class="list-container" id="list-container' + me.pid + '">';
		if (me.playlist.length == 0) {
			value += 'Queue is empty</div>';
			$("#playtitle" + me.pid).text("Nothing to play");

			$("#current-song-container").load("/utility/current_song?id=0");
			//$("#related-songs-container").html('There are no suggestions right now');
			reset_fav_button();
		} else {
			value += '<table id="menu' + me.pid + '"><tbody>';
			for (var x = 0; x < me.playlist.length; x++) {
				e = document.getElementById(me.playlist[x]);
				value += '<tr id="queue_song_'+me.playlist[x]+'" data-id="'+me.playlist[x]+'" class="link link-text inner-border" ';
				value += 'style="border-bottom:1px solid #DDD;border-left:4px solid #fff" onclick="playChosen(' + me.pid + ',\'' + me.playlist[x] + '\');">';
				value += '<td><img class="small-image queue-image" src="' + e.getAttribute('data-art') + '"/></td>';
				value += '<td><div style="height:20px;width:111px;margin-left:3px" class="special-ellipsis"><span class="tool-tip queue-search" title="' + e.getAttribute('data-name') + '">';
				value += e.getAttribute('data-name') + '</span></div></td><td><button class="close"  onclick="remove_song(event,' + me.pid + ',\'' + me.playlist[x] + '\');">&times;</button></td></tr>';
			}
			value += '</tbody></table></div>';
		}
		$(el).html(value);
		var current = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
		$(current).attr('class','link link-text song-indicator');
		$('.special-ellipsis').dotdotdot({
			wrap : 'letter'
		});
		if (flag == 1) {
			$('#playlist' + me.pid).slideToggle('medium');
		}


	};

	this.load_partial_playlist = function(start,end) {
		var el = "#menu" + me.pid+' tbody';
		value = '';
		for (var x = start; x <=end; x++) {
			e = document.getElementById(me.playlist[x]);
				value += '<tr id="queue_song_'+me.playlist[x]+'" data-id="'+me.playlist[x]+'" class="link link-text inner-border" ';
				value += 'style="border-bottom:1px solid #DDD;border-left:4px solid #fff" onclick="playChosen(' + me.pid + ',\'' + me.playlist[x] + '\');">';
				value += '<td><img class="small-image queue-image" src="' + e.getAttribute('data-art') + '"/></td>';
				value += '<td><div style="height:20px;width:111px;margin-left:3px" class="special-ellipsis"><span class="tool-tip queue-search" title="' + e.getAttribute('data-name') + '">';
				value += e.getAttribute('data-name') + '</span></div></td><td><button class="close"  onclick="remove_song(event,' + me.pid + ',\'' + me.playlist[x] + '\');">&times;</button></td></tr>';
		}
		$(el).append(value);
		$('.special-ellipsis').dotdotdot({
			wrap : 'letter'
		});

	};

	this.remove_song = function(event, id) {
		if(event!=null)
		{
			event.stopPropagation();
		}
		song_index = me.search(me.playlist,id);
		if (song_index == me.position)
		{
			me.stop();
			me.stop_buffering();
		}
		$('#queue_song_'+id).remove();
		//remove song from current playlist in cache and cookie
		var song_el = $('#'+me.playlist[song_index]);
		var song_id = song_el.attr('data-id');
		var song_title = song_el.attr('data-name');		
		$('#'+me.playlist[song_index]).remove();
		if($('#signed_in_flag').length > 0 && me.socket_enabled)
		{
		//invoke_controller_action('', 'POST', "/queue/remove_from_current_queue?id=" + song_id, '');
		socket.emit('remove_from_queue', {token: $.cookie("_validation_token_key"), song: song_id, song_name: song_title, group_listen: me.group_listen });
		}
		else if(!($('#signed_in_flag').length > 0))
		{
		update_cookie("remove",song_id,"queue");
		}
		//For shuffle
		if (me.original_playlist_exists)
			me.original_playlist.splice(me.search(me.original_playlist, me.playlist[song_index]), 1);
		me.playlist.splice(song_index, 1);
		//if the removed song is the currently played song,play the next song
		if (song_index < me.position) {
			//To nullify the effect of removing a song
			me.position--;
		} else if (song_index == me.position) {
			//To nullify the effect of removing a song
			me.position--;
			me.nextSong();
			if (me.playlist.length == 0)
				me.play_button();
		}
		if(me.playlist.length==0)
		{
			me.load_playlist(0);
		}
		var after = '#menu1 tr:nth-child(' + (me.position + 1) + ')';
		$(after).attr('class','link link-text song-indicator');

	};

	this.reset_player = function() {
		me.stop();
		me.play_button();
		//Stop buffering the current song
		me.stop_buffering();
		if(me.loop_value)
			me.loop();
		if(me.shuffle_value)
			me.shuffle();
		for (var i = 0; i < me.playlist.length; i++)
			$('#'+me.playlist[i]).remove();
		me.playlist = [];
		me.original_playlist = [];
		me.shuffle_value = false;
		me.position = -1;
		me.current_audio = null;
		me.current_audio_js = null;
		me.status = 0;
		me.load_playlist(0);
	}

	this.clearPlaylist = function() {
		if (me.position > -1) {
			me.reset_player();
			//clear playlist in cache or cookie
			if ($('#signed_in_flag').length > 0 && me.socket_enabled)
			{
			//invoke_controller_action('', 'POST', "/queue/clear_current_queue", '');
			socket.emit('clear_queue', { token: $.cookie("_validation_token_key"),current_song: $(me.current_audio).attr('data-id'), group_listen: me.group_listen });
			}
			else if (!($('#signed_in_flag').length > 0))
			{
			update_cookie("clear","","queue");
			update_cookie("clear","","song");
			}
			show_flash("The queue has been cleared",false);
		}

	};
	//save current queue as playlist
	this.saveAsPlaylist = function() {
		var queue = [];
		for (var i = 0; i < me.playlist.length; i++) {
			queue.push(document.getElementById(me.playlist[i]).getAttribute('data-id'));
		}
		if ($('#signed_in_flag').length > 0)
		invoke_controller_action('', 'POST', "/playlists/" + me.save_as_playlist_id + '/add_multiple_song?s_id=' + queue.join(','), '');
		else
		show_flash("Please log in to save your queue as playlist",false);	
	};
	//To increase performance during bulk addition of songs to queue
	this.mass_add_songs_to_queue = function(el, mode) {
		var queue = [];
		var initial_length = me.playlist.length;
		var first;
		for (var i = 0; i < el.length; i++) {
			var ael;
			var search = me.search_playlist(el[i].getAttribute("data-id"),true);
			if (i == 0) 
			{
				first = search == -1 ? initial_length : search;
			}
			if (search == -1) 
			{
				ael = document.createElement('audio');
				// Fix for Safari
				//ael.setAttribute('type','audio/mpeg');
				ael.setAttribute('data-src', el[i].getAttribute("data-src"));
				ael.setAttribute('id', el[i].getAttribute("data-identifier"));
				ael.setAttribute('data-name', el[i].getAttribute("data-name"));
				ael.setAttribute('data-art', el[i].getAttribute("data-art"));
				ael.setAttribute('data-album', el[i].getAttribute("data-album"));
				ael.setAttribute('data-id', el[i].getAttribute("data-id"));
				ael.setAttribute('preload', "auto");
				document.getElementById('player' + me.pid).appendChild(ael);
				queue.push(ael.getAttribute("data-id"));
				me.playlist.push(ael.getAttribute("id"));
				if (me.original_playlist_exists)
				{
					me.original_playlist.push(ael.getAttribute("id"));
				}
			}
			//ael.removeEventListener('error', me.resetaudio, false);
			//ael.addEventListener('error', me.resetaudio, false);
		}
		if(queue.length > 0)
		{
			if(initial_length == 0)
				me.load_playlist(0);
			else
				me.load_partial_playlist(initial_length,initial_length + queue.length -1 );
			if ($('#signed_in_flag').length > 0 && me.socket_enabled)
			{
				//invoke_controller_action('', 'POST', "/queue/mass_add_to_current_queue?id=" + queue.join(','), '');
				socket.emit('add_to_queue', {token: $.cookie("_validation_token_key"), song: queue.join(','), group_listen: me.group_listen });
			}
			else if(!($('#signed_in_flag').length > 0))
			{
				update_cookie("mass_add",queue,"queue");
			}
		}
		if (mode == 'play') {
			me.playChosen(me.playlist[first]);
		} 
		else if (initial_length == 0) 
		{
			me.nextSong();
		}
	};
	this.shuffle = function() {
		if(me.playlist.length > 0)
		{
			if (me.shuffle_value) {
				show_flash("Shuffle OFF",false);
				$('#shuffle-queue').removeClass('transparent');
				temp = me.playlist.slice(0);
				me.playlist = me.original_playlist.slice(0);
				me.position = me.search(me.original_playlist, temp[me.position]);
				
				me.load_playlist(0);
			} else {
				show_flash("Shuffle ON",false);
				$('#shuffle-queue').addClass('transparent');
				temp = me.playlist.slice(0);
				me.playlist = me.shuffle_array(me.playlist);
				me.position = me.search(me.playlist, temp[me.position]);
				if (me.original_playlist.length == 0) {
					me.original_playlist = temp;
					if (!me.original_playlist_exists)
						me.original_playlist_exists = true;
				}
				
				me.load_playlist(0);
			}
			me.shuffle_value = !me.shuffle_value;
		}
	};

	this.shuffle_array = function(array) {
		//fisher-yates algorithm
		for (var i = array.length - 1; i > 0; i--) {
        	var j = Math.floor(Math.random() * (i + 1));
        	var temp = array[i];
        	array[i] = array[j];
        	array[j] = temp;
    	}
		return array;
	};
	this.loop = function() {
		if(me.playlist.length > 0)
		{
			if (me.loop_value) {
				show_flash("Loop OFF",false);
				$('#loop-queue').removeClass('transparent');
			} else {
				show_flash("Loop ON",false);
				$('#loop-queue').addClass('transparent');
				}
			me.loop_value = !me.loop_value;
			
		}
	};

	this.updateprogress = function() {
		var bar_width;
		if (me.pid == 1)
			bar_width = 458;
		else
			bar_width = 235;
		var el = '#progress' + me.pid;
		var el1 = '#playtitle' + me.pid;

		me.sid = window.setInterval(function() {
			var audio_js = me.current_audio_js;
			var audio = me.current_audio;
			if (me.position > -1 && typeof audio_js != 'undefined') {
				
				me.current_time = audio_js.currentTime;
				me.isBuffering = true;
				var wid = parseInt((audio_js.currentTime / audio_js.duration) * bar_width);
				if(audio_js.currentTime<audio_js.loadedPercent*audio_js.duration)
				me.isBuffering = false;
				if (me.isBuffering == false) {
					$(el1).text(audio.getAttribute('data-name'));
				} else {
					$(el1).text('Buffering...');
				}
				$(el).width(wid);
				me.updatetime(audio_js);

			}

		}, 500);
	};

	this.enable_group_listen = function() {
		queue_title = '<span class="amuzzin-font"><span class="red">Group</span> Queue</span>';
		show_flash('Enabling group listen',true);
		me.group_listen = true;	
		if(me.position > -1)
		{
			me.reset_player();
		}
		invoke_controller_action('show_flash("Restoring group queue",true)', 'GET', '/queue/get_group_queue', 'load_playlist(1, 0);$("#queue-title").html(queue_title);show_flash("Group listen enabled",false)');
	};

	this.disable_group_listen = function() {
		queue_title = '<span class="amuzzin-font"><span class="red">Current</span> Queue</span>';
		show_flash('Disabling group listen',true);
		me.group_listen = false;	
		if(me.position > -1)
		{
			me.reset_player();
		}
		invoke_controller_action('show_flash("Restoring your queue",true)', 'GET', '/queue/get_current_queue', 'load_playlist(1, 0);$("#queue-title").html(queue_title);show_flash("Group listen disabled",false)');
	};
}

function load_player(identifier) 
{
	player_objects.push(new player());
	player_objects[player_objects.length - 1].pid = identifier;
	player_objects[player_objects.length - 1].updateprogress();
	if (identifier == 1) {
		$('#player1').html('<div style="left:22px;top:2px;position:relative;width:540px"><table><tr><td><div id="song" style="height:25px;width:170px;">' + 
			'<div id="playtitle1" class="special-ellipsis amuzzin-font" style="color:#FFF;height:100%;width:160px;margin-top:3px">Nothing to play</div></div></td>'+
			'<td><button class="btn btn-default btn-player share" data-height="390" data-width="400" data-bpopup="{"contentContainer":".content"}" title="Share this song. \n Shortcut: (\'s\') or (\'S\') )"><i class="icon icon-share-alt"></i></button></td>'+
			'<td class="dropdown"><button class="btn btn-default btn-player info dropdown-toggle link offset3pxl" id="player1_info" title="Song and queue count. \n Shortcut: (\'i\') or (\'I\') )" data-toggle="dropdown"><i class="icon icon-info" style="font-size:17px;"></i></button>'+
			'<div class="dropdown-menu"><div id="info-div" >No info available</div></div></td>'+
			'<td><div id="previous1" title="Previous song. \n Shortcut: <Right arrow key>" onclick="prev(1);" style="margin-left:29px;"><button class="btn btn-default btn-player"><i class="icon icon-fast-backward"></i></button></div></td>'+
			'<td><div  class="link" title="Play/Pause. \n Shortcut: <Space Bar>" onclick="playPause(1);" style="margin-left:5px;"><button class="btn btn-default btn-player" id="play1"><i class="icon icon-play"></i></button></div></td>'+
			'<td><div id="next1" title="Next song. \n Shortcut: <Left arrow key>" onclick="next(1);" style="margin-left:5px;"><button class="btn btn-default btn-player"><i class="icon icon-fast-forward"></i></button></div></td>'+
	//		'<td><div id="change_quality1" onclick="change_quality(1)" title="Switch to High Quality" style="margin-left:20px"><button class="btn btn-default btn-player"><i style="font-size:17px"class="icon icon-cog"></i></button></div></td>'+
			'<td><div id="stop1" onclick="stop(1);" title="Stop playing" style="margin-left:21px"><button class="btn btn-default btn-player"><i class="icon icon-stop"></i></button></div></td>'+
			'<td><div id="fav_icon" class="offset3pxl" title="Add this song to your favourites" ><button class="btn btn-default btn-player"><i class="icon icon-heart"></i></button></div></td>'+
			'<td><button class="btn btn-default btn-player offset3pxl playlist" data-height="270" data-width="290" title="Add this song to playlist"><i class="icon icon-plus" style="font-size:17px;"></i></button></td>'+
			'<td><div id="volume-container"  style="width:125px;height:20px;position:relative" class="offset3pxl"><div class="link pull-left" id="volume-button1" title=" Volume control. \n Shortcut: (\'x\') to mute.">'+
			'<button class="btn btn-default btn-player" onclick="mute(1)" ><i class="icon icon-volume-off" style="font-size:20px;"></i></button></div><div id="volume1" class="pull-left" style="width:65px;height:5px;margin-top:9px;left:10px;"></div></div></td></tr></table>'+
			'<div id="progress-container"><table><tr><td><div style="font-size:10px;color:#fff;font-weight:bold;width:40px"><div id="time1" style="float:left">00:00</div></div></td>'+
			'<td><div id="progressbar1" class="link" onclick="updatebar(event,1);" style="width:458px;height:6px;margin-top:2px;"><div class="progress progress-danger" id="progress-main" style="height:5px;">'+
			'<div class="bar" id="progress1" style="width:0%;height:5px;"></div></div></div></td><td><div style="font-size:10px;color:#fff;font-weight:bold;width:40px"><div id="duration1" style="float:right">00:00</div></div></td></tr></table></div></div>');
		$('#volume1').slider({
			range: 'min',
			value: 39,
			max: 65,
			change: function(event,ui){
				updateVol(event,1);
			}
		});
	} else {
		$('#player' + identifier).html('<div id="play' + identifier + '" class="play" onclick="javascript:playPause(' + identifier + ');"><span class="pl-mini player-play-mini" height="39" width="39"></span></div>' + '<div class="control">' + '<table>' + '<tr>' + '<td><i class="icon-stop icon" id="stop' + identifier + '" onclick="javascript:stop(' + identifier + ');" style="cursor:pointer;"></i></td>' + '<td><i class="icon-backward icon" id="previous' + identifier + '" onclick="javascript:prev(' + identifier + ');" style="cursor:pointer;"></i></td>' + '<td><div class="playtitle special-ellipsis" id="playtitle' + identifier + '">' + 'Nothing to play</div></td>' + '<td><i class="icon-forward icon" id="next' + identifier + '" onclick="javascript:next(' + identifier + ');" style="cursor:pointer;"></i></td>' + '<td><i class="icon-minus icon" id="voldec' + identifier + '" onclick="javascript:dec(' + identifier + ');" style="cursor:pointer;"></i></td>' + '<td>' + '<div class="volumebar" id="volumebar' + identifier + '" onclick="updateVol(event,' + identifier + ');">' + '<div class="volume" id="volume' + identifier + '">' + '&nbsp;' + '</div>' + '</div></td>' + '<td><i class="icon-plus icon" id="volinc' + identifier + '" onclick="javascript:inc(' + identifier + ');" style="cursor:pointer;"></i></td>' + '</tr>' + '</table>' + '</div>' + '<div class="updatecontrol">' + '<table>' + '<tr>' + '<td>' + '<div id="progressbar' + identifier + '" class="progressbar" onclick="updatebar(event,' + identifier + ');">' + '<div id="progress' + identifier + '" class="progress"></div></div></td><td><div id="time' + identifier + '" class="time">' + '00:00' + '</div></td>' + '</tr>' + '</table>' + '</div>');
	}
	map['' + identifier] = id;
	id++;
}



function mass_add_songs_to_queue(player_id, el, mode) 
{
	player_objects[map[player_id] - 1].mass_add_songs_to_queue(el, mode);
	show_flash('Added selected songs to queue',false);
}

function playit(player_id, el) 
{
	if (document.getElementById(el.getAttribute("data-identifier")) == null) {
		var ael = document.createElement('audio');
		player_objects[map[player_id] - 1].favourited = false;
		// Fix for Safari
		//ael.setAttribute('type','audio/mpeg');
		ael.setAttribute('data-src', el.getAttribute("data-src"));
		ael.setAttribute('id', el.getAttribute("data-identifier"));
		ael.setAttribute('data-name', el.getAttribute("data-name"));
		ael.setAttribute('data-art', el.getAttribute("data-art"));
		ael.setAttribute('data-album', el.getAttribute("data-album"));
		ael.setAttribute('data-id', el.getAttribute("data-id"));
		ael.setAttribute('preload', "auto");
		document.getElementById('player' + player_id+'-songs').appendChild(ael);
	}
	player_objects[map[player_id] - 1].play_selected(el.getAttribute("data-identifier"));
	show_flash('Playing selected song',false);
//	send_unity_event(player_objects[map[player_id] - 1]);
}

function addit(player_id, el) 
{

	if (document.getElementById(el.getAttribute("data-identifier")) == null) {
		var ael = document.createElement('audio');
		// Fix for Safari
		//ael.setAttribute('type','audio/mpeg');
		ael.setAttribute('data-src', el.getAttribute("data-src"));
		ael.setAttribute('id', el.getAttribute("data-identifier"));
		ael.setAttribute('data-name', el.getAttribute("data-name"));
		ael.setAttribute('data-art', el.getAttribute("data-art"));
		ael.setAttribute('data-album', el.getAttribute("data-album"));
		ael.setAttribute('data-id', el.getAttribute("data-id"));
		ael.setAttribute('preload', "auto");
		document.getElementById('player' + player_id+'-songs').appendChild(ael);

	}
	player_objects[map[player_id] - 1].add_to_playlist(el.getAttribute("data-identifier"));
	show_flash('Added selected song to queue',false);
}

function playPause(player_id) 
{
	pl = player_objects[map[player_id] - 1];
	pl.play_pause_song();
	//send_unity_event(player_objects[map[player_id] - 1]);
}

function next(player_id) 
{
	player_objects[map[player_id] - 1].nextSong();
	//send_unity_event(player_objects[map[player_id] - 1]);
}

function prev(player_id) 
{
	player_objects[map[player_id] - 1].prevSong();
	//send_unity_event(player_objects[map[player_id] - 1]);
}

function send_unity_event(pl)
{

	try{
		
		if(pl == undefined || pl.current_audio_js == undefined || pl.current_audio_js.source == undefined)
			return;
		
		playerState = {
		 playing: (pl.status==1),
	  title: pl.current_audio_js.source.getAttribute("data-name"),
	  artist: pl.current_audio_js.source.getAttribute("data-album"),
	  albumArt: location.protocol+location.host+pl.current_audio_js.source.getAttribute("data-art").replace('small','big'),
	  favorite: pl.favourited,
		};

			unity.sendState(playerState);
	}
	catch(err)
	{
		console.log('Call to Unity Failed!');
	}
}

function inc(player_id) 
{
	player_objects[map[player_id] - 1].incvolume();
}

function dec(player_id) 
{
	player_objects[map[player_id] - 1].decvolume();
}

function stop(player_id) 
{
	player_objects[map[player_id] - 1].stop_button();
}

function switch_quality_smoothly(player_id)
{
		$(component_id).addClass('blink');
		var audio = document.getElementById(player_objects[0].playlist[player_objects[0].position]);
		//$(audio).attr('src',$(audio).attr('data-src'));
		//create audiojs element
		  if($.cookie('quality') != undefined)
		  {
		  	data_src = $(audio).attr('data-src')+'_'+$.cookie('quality');
		  }
		  else
		  {
		  	data_src = $(audio).attr('data-src');
		  }

		var new_audio = document.createElement('audio');
		//audio.removeEventListener('error', me.resetaudio, false);
		var attrs = $(audio).prop("attributes");
		$.each(attrs, function() {
			if(this.name!='src')
    		$(new_audio).attr(this.name, this.value);
		});

		$(new_audio).attr('src',data_src);
		$(new_audio).attr('preload','auto');

		new_audio.addEventListener('loadstart',function(){
			  current_time = player_objects[0].current_audio_js.currentTime;
			  total_time = player_objects[0].current_audio_js.duration;
			  percent = current_time/total_time;
				player_objects[0].current_audio_js.load(data_src);
				if(player_objects[0].status == 1)
				{
					player_objects[0].play_song(player_objects[0].current_audio_js);
				}
				$(component_id).removeClass('blink');
				player_objects[0].current_audio_js.skipTo(percent);

	 	},false);
}

function change_quality(player_id) 
{
	
	if($.cookie('quality') == undefined)
	{
		$.cookie('quality','original');
		show_flash("Switched to high quality",false);
	}
	else
	{
		$.removeCookie('quality');
		show_flash("Switched to normal quality",false);
	} 
	if(player_objects[map[player_id]-1].position > -1)
	{
		switch_quality_smoothly(player_id);
	}
	set_quality_state();
}

function set_quality_state()
{
	component_id = '#change_quality1';
	if($.cookie('quality') == 'original')
	{
		$(component_id).css('opacity', '0.6');
		$(component_id).css('filter', 'alpha(opacity=60)');
		$(component_id)[0].title="Switch to Normal Quality \n(Recommended for Low Bandwidth)";
	}
	else
	{
		$(component_id).css('opacity', '1');
		$(component_id).css('filter', 'alpha(opacity=1)');
		$(component_id)[0].title="Switch to High Quality \n(Recommended for Crystal Clear Sound)";
	} 
}

function updateVol(event, player_id) 
{
	player_objects[map[player_id] - 1].updateVolume(event);
}

function mute(player_id) 
{
	player_objects[map[player_id] - 1].mute_volume();
}

function updatebar(event, player_id) 
{
	player_objects[map[player_id] - 1].updatebar(event);
}

function load_playlist(player_id, flag) 
{
	player_objects[map[player_id] - 1].load_playlist(flag);
}

function remove_song(event, player_id, id) 
{
	player_objects[map[player_id] - 1].remove_song(event, id);
	show_flash('Song removed from queue',false);
}

function playChosen(player_id, id) 
{
	player_objects[map[player_id] - 1].playChosen(id);
}

function clear_playlist(player_id) 
{
	player_objects[map[player_id] - 1].clearPlaylist();
	show_flash('Cleared queue',false);
}

function shuffle(player_id) 
{
	player_objects[map[player_id] - 1].shuffle();
}

function loop(player_id) 
{
	player_objects[map[player_id] - 1].loop();
}

function stop_running_player() 
{
	//to stop all other running players
	for (var y = 0; y < player_objects.length; y++) {
		if ((player_objects[y].status == 1)) {
			player_objects[y].play_button();
			//var el = document.getElementById(player_objects[y].playlist[player_objects[y].position]);
			player_objects[y].pause_song(player_objects[y].current_audio_js);
			player_objects[y].resume_flag = 1;
		}
	}
}

function resume_running_player() 
{
	//to resume player
	for (var y = 0; y < player_objects.length; y++) {
		if ((player_objects[y].resume_flag == 1)) {
			player_objects[y].pause_button();
			//var el = document.getElementById(player_objects[y].playlist[player_objects[y].position]);
			player_objects[y].play_song(player_objects[y].current_audio_js);
			player_objects[y].resume_flag = 0;
		}
	}
}


function humanize(time) 
{
	var min = Math.floor(time / 60);
	var sec = Math.floor(time % 60);
	if (min < 10)
		min = "0" + min;
	if (sec < 10)
		sec = "0" + sec;
	return min + ":" + sec;
};

//This is called when there is no song to play
function reset_fav_button() 
{
	$('#fav_icon').attr('onclick', '').unbind('click');
	$('#fav_icon').css('opacity', '1');
}

function enable_fav_button() 
{
	player_objects[0].favourited = false;
//	send_unity_event(player_objects[0]);
	$('#fav_icon').attr('onclick', '').unbind('click');
	$('#fav_icon').click(function() 
	{
		save_current_as_fav();
	});
	$('#fav_icon').css('opacity', '1');
	$('#fav_icon').css('filter', 'alpha(opacity=100)');
}

function disable_fav_button() {
	player_objects[0].favourited = true;
	//send_unity_event(player_objects[0]);
	$('#fav_icon').attr('onclick', '').unbind('click');
	$('#fav_icon').click(function() {
		remove_current_from_fav();
	});
	$('#fav_icon').css('opacity', '0.6');
	$('#fav_icon').css('filter', 'alpha(opacity=60)');

}

function save_current_as_fav() 
{
	if (player_objects[0].position > -1) {
		if ($('#signed_in_flag').length > 0)
		{
		invoke_controller_action('disable_fav_button();', 'POST', "/favourite/add_favourite?s_id=" + player_objects[0].current_audio.getAttribute('data-id'), '');
		}
		else
		show_flash('Please log in to favourite this song',false);
	}
}

function remove_current_from_fav() 
{
		//send_unity_event(player_objects[0]);
		player_objects[0].favourited = false;
	if (player_objects[0].position > -1) {
		if ($('#signed_in_flag').length > 0)
		{
		invoke_controller_action('enable_fav_button();', 'POST', "/favourite/remove_favourite?id=" + player_objects[0].current_audio.getAttribute('data-id'), '');	
		}
		else
		show_flash("Please log in to remove this song from favourites",false);
	}
}

Mousetrap.bind('space', function(e) {
	  e.preventDefault();
    playPause(1);
});

Mousetrap.bind(['x','X'], function(e) {
	e.preventDefault();
    mute(1);
});

Mousetrap.bind('right', function(e) {
	e.preventDefault();
    next(1);
});

Mousetrap.bind('left', function(e) {
	e.preventDefault();
    prev(1);
});


Mousetrap.bind(['i','I'], function(e) {
	e.preventDefault();
    $('#player1_info').click();
});

Mousetrap.bind(['s','S'], function(e) {
	e.preventDefault();
    $('.player-share').click();
});

Mousetrap.bind('+', function(e) {
	e.preventDefault();
});

class WelcomeMailer < ActionMailer::Base
	default from: "no-reply@amuzz.in"
	def welcome_email(user)
		@user = user
		mail(:from => "no-reply@amuzz.in",:to => user.email, :subject => "amuzz.in - Email Verification.")
	end
end

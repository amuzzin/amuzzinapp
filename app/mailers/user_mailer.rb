class UserMailer < ActionMailer::Base
  include Devise::Mailers::Helpers
  default from: "no-reply@amuzz.in"
  
  def send_password_for_facebookers(user, pass)
    @pass = pass
    @user = user
    mail(:to => user.email, :subject => "Welcome to amuzz.in - facebook user")
  end

  def reset_password_instructions(record)
    devise_mail(record, :reset_password_instructions)
  end

  def new_uploads user
    @new_songs = user.get_new_friend_uploads(Time.now - 1.week)
    if @new_songs.blank?
      @new_songs = UploadedSong.find(:all,:conditions=>"created_at >='#{Time.now - 1.week}'")
    end
    @new_songs.sort do |a, b|
      case
      when a.song_count < b.song_count
        -1
      when a.song_count > b.song_count
        1
      else
        a.queue_count <=> b.queue_count
      end
    end 
    @new_songs = @new_songs[0..4]
    @user = user
    mail(:to => user.email, :subject => "New Songs in Amuzzin").deliver if @new_songs.present?
  end

  def new_friend_signup(amuzzin_user,new_user)
    @amuzzin_user = amuzzin_user
    @new_user = new_user
    mail(:to => @amuzzin_user.email, :subject => "Your buddy has joined Amuzzin")
  end
end

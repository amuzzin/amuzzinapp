class Group < ActiveRecord::Base
  attr_accessible :started_by_id,:name
  belongs_to :started_by, :class_name => 'User'
  has_many :group_members
  has_many :users, :through => :group_members
  has_many :chat_messages, as: :recipient

  include PublicActivity::Model


  def self.create_group(name,user)
  	group = Group.create(:started_by_id => user.id,:name => name)
    group.users << user
    #Initialize group in redis
    $redis.hset("group_members",group.id,user.id)
    $redis.hset("group_queue",group.id,nil)
    $redis.hset("group_song",group.id,nil)
    $redis.hset("group_log",group.id,nil)
    $redis.hset("user_group",user.id,group.id)
    $redis.hset("group_enabled",user.id,true)
    msg = { 'user' => user.get_details_json,
            'type' => 'join',
            'group' => group.id
          }
    group.add_to_log(msg)
    group
  end

  def join(user)
    already_present = GroupMember.exists?(["group_id = ? and user_id = ?",self.id,user.id])
    if !already_present
      switch(user)
      $redis.hset("group_enabled",user.id,true)
      self.users << user
      Group.clear_group_invite(user.id);
      members = $redis.hget("group_members",self.id)
      members ? members += ",#{user.id}" : members = user.id.to_s
      $redis.hset("group_members",self.id,members)
      msg = { 'user' => user.get_details_json,
              'type' => 'join',
              'group' => self.id
            }
      add_to_log(msg)
    end
  end

  def switch(user)
    old_group = user.current_group
    $redis.hset("user_group",user.id,self.id)
    $redis.hset("group_enabled",user.id,true)
    if old_group
      msg = { 'user' => user.get_details_json,
              'type' => 'switch_from',
              'group' => old_group.id
            }
      add_to_log(msg)
    end
    msg = { 'user' => user.get_details_json,
            'type' => 'switch_to',
            'group' => self.id
          }
    add_to_log(msg)
  end

  def leave(user)
    already_present = GroupMember.exists?(["group_id = ? and user_id = ?",self.id,user.id])
    if already_present
    	$redis.hdel("user_group",user.id)
      $redis.hdel("group_enabled",user.id)
      group_member = GroupMember.first(:conditions => "user_id=#{user.id} and group_id=#{self.id}")
      group_member.destroy
    	members = $redis.hget("group_members",self.id)
    	if members
     	  members = members.split(",")
    	  members.delete(user.id.to_s)
    	  $redis.hset("group_members",self.id,members.join(","))
        msg = { 'user' => user.get_details_json,
              'type' => 'leave',
              'group' => self.id
            }
    	  add_to_log(msg)
    	end
    end
  end

  def enable(user)
    group_id = $redis.hget("user_group",user.id)
    $redis.hset("group_enabled",user.id,true) if group_id.present?
    msg = { 'user' => user.get_details_json,
            'type' => 'enable',
            'group' => self.id
          }
    add_to_log(msg)
  end

  def disable(user)
    group_id = $redis.hget("user_group",user.id)
    $redis.hdel("group_enabled",user.id) if group_id.present?
    msg = { 'user' => user.get_details_json,
            'type' => 'disable',
            'group' => self.id
          }
    add_to_log(msg)
  end

  def self.set_group_invite(invited_by,invited,group)
    $redis.hset("invite_group",invited,group)
    $redis.hset("invited_by",invited,invited_by)
  end

  def self.get_invite_group(user)
    $redis.hget("invite_group",user)
  end

  def self.get_invited_by(user)
    $redis.hget("invited_by",user)
  end

  def self.clear_group_invite(user)
    $redis.hdel("invite_group",user)
    $redis.hdel("invited_by",user)
  end


  def add_to_log(message)
    message[:time] = (Time.now.to_f * 1000).to_i
    if message[:type] == "chat"
      $redis.lpush("group:#{self.id}:log.chat",message.to_json)
    else
  	  $redis.lpush("group:#{self.id}:log.other",message.to_json)
    end
    members = $redis.hget("group_members",message['group']);
    if members.present?
      members = members.split(",")
      user = message['user']['user_id'];
      members.each do |m|
        member = User.find_by_id(m)
        if member && member.current_group.id == message['group'].to_i && member.is_group_listen_enabled?
          (member.id.to_s == user.to_s) ? message['user']['current_user'] = true : message['user']['current_user'] = false
          $redis.publish("realtime_#{member.id}","{\"type\":\"group_log\",\"log\":#{message.to_json}}")
        end
      end
    end
  end

  def log
  	log = $redis.hget("group_log",self.id)
  end

  def other_log index,size
    $redis.lrange("group:#{self.id}:log.other",index,index+size)
  end

  def chat_log index,size
    $redis.lrange("group:#{self.id}:log.chat",index,index+size)
  end

  def previous_page(type,index,size)
    length = $redis.llen("group:#{self.id}:log.#{type}")
    if length>index
      case type
      when 'chat'
        chat_log(index,size)
      when 'other'
        other_log(index,size)
      end
    else
      nil
    end
  end 	
end

class Favourite < ActiveRecord::Base
  include PublicActivity::Model
  tracked owner: ->(controller, model) { controller && controller.current_user }
  tracked recipient: ->(controller, model) { model && model.uploaded_song.user }

  # attr_accessible :title, :body
  belongs_to :uploaded_song
  belongs_to :user

  after_create :inform_owner
  before_destroy :destroy_feeds 

  def destroy_feeds
    cleanup_query = "delete from activities where trackable_type='Favourite' and trackable_id='#{self.id}'" 
    ActiveRecord::Base.connection.execute cleanup_query
  end 

  def inform_owner
    $redis.publish("realtime_#{self.uploaded_song.user_id}","{\"id\":#{self.user.id},\"type\":\"favourite\",\"name\":\"#{self.user.name}\",\"song\":\"#{self.uploaded_song.title}\",\"icon\":\"#{self.user.get_profile_pic}\",\"song_id\":\"#{self.uploaded_song_id}\"}")
  end 
end

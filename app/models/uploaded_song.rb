require 'id3lib'
require 'digest/sha1'
require 'net/http'
require 'iconv'

class UploadedSong < ActiveRecord::Base
  attr_accessible :name,:title, :genre, :album, :lyrics, :artist, :youtube_url, :image , :remote_image_url, :language,:year
  belongs_to :user
  belongs_to :song_master
  has_many :cut_songs
  has_many :playlist_songs
  has_many :playlists, :through => :playlist_songs
  has_many :favourites
  has_many :users, :through => :favourites
  has_many :profile_songs
  has_many :group_logs, as: :event_data
  has_many :user_histories
  has_property :new_song, :name => "new", :position => 3
  has_property :old_song, :name => "old", :position => 3
  has_property :newly_uploaded, :name => "newly uploaded", :position => 1
  has_property :unheard, :name => "unheard", :params => true, :position => 2
  has_property :most_heard, :name => "most heard", :params => true, :position => 2
  has_property :least_heard, :name => "least heard", :params => true, :position => 2
  has_property :song_language, :name => "language", :value => true, :position => 4
  has_property :song_genre, :name => "Genre", :value => true, :position => 5
  has_owner :friend, :name => "friend", :params => true, :value => true
  mutually_exclusive [:most_heard,:unheard,:least_heard]
  mutually_exclusive [:new_song,:old_song]
  set_display_name :song

  letsrate_rateable "music", "lyrics"
  mount_uploader :image, ImageUploader

  include PublicActivity::Model
# tracked owner: ->(controller, model) { controller && controller.current_user }
  after_update :create_activities

  def create_activities
    if self.lyrics_changed? and self.lyrics_was.blank? and self.lyrics.present? and self.lyrics.size > 20
      self.create_activity key: 'uploaded_song.lyrics', owner: self.user
    end
  end

  scope :active, where("song_master_id is not null")
  opinio_subjectum
  # Contains user defined search related stuffs
  include ProcessSong::UploadedSongSearcher
  # Youtube related libs
  include ProcessSong::ProcessYoutube
  include ProcessSong::ProcessSoundcloud


  # Convert from ogg , Split mp3 , Format time
  include ProcessSong::ConvertMetrics

  # Layer 1 => Reads directly from ID3 tags of mp3
  # Layer 2 => Gathers information from Webservices
  include ExtractionLayer::Layer1Extraction
  include ExtractionLayer::Layer2Extraction

  # Checks copyright of master song and deletes it
  include ExtractionLayer::DuplicateCopyRight
  include SemanticSearch::UploadedSongSearch

  #======================================#
  # Hash the song with its content - SHA1
  # Save it in public/audios file
  #======================================#
  def save_song(upload)
    # get the name of file from http
    name =  upload.original_filename
    # set song directory
    self.name = name
    self.save!
    directory = "public/audios"
    # create the file path
    path = File.join(directory, name)
    # write the file temporarily
    File.open(path, "wb") { |f| f.write(upload.read) }
    # generate hash for a given file
    file_hash = Digest::SHA1.hexdigest(path).to_s
    # check whether the entry already exists in DB
    create_or_load_master_song(file_hash,path)
    return self
  end

  def show_language
    if self.language and !self.language.eql?("Not Available")
      return "(#{self.language})"
    end
  end

  def get_user
    @user = User.find(self.user_id)
    return @user.name
  end

  def src_path
    return "#{MP3_CDN}#{self.path.gsub('public','')}"
  end

  def stamp_title_album
    #if title and album are still blank, replace them with file name and 'Not Available' respectively
    @uploaded_song = UploadedSong.find(self.id)
    p "Stamping title and album"
    if @uploaded_song.title.blank? and @uploaded_song.name.present?
      @uploaded_song.title = @uploaded_song.name.gsub('.mp3','')
    end

    if @uploaded_song.album.blank?
      @uploaded_song.album = 'Not Available'
    end

    @uploaded_song.save!
  end

  def self.search_title(name,page,count,star=false)
    UploadedSong.search "@title #{name}", :page => page, :per_page => count
  end

  def self.search_album(name,page,count,star=false)
    UploadedSong.search "@album #{name}", :page => page, :per_page => count
  end

  def self.search_lyrics(name,page,count,star=false)
    UploadedSong.search "@lyrics #{name}" , :page => page, :per_page => count
  end

  def self.search_genre(name,page,count,star=false)
    UploadedSong.search "@genre #{name}" , :page => page, :per_page => count
  end

  def song_count
    $redis.hget("song_count",self.id).to_i
  end

  def queue_count
    $redis.hget("queue_count",self.id).to_i
  end
end

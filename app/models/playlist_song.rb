class PlaylistSong < ActiveRecord::Base
  belongs_to :uploaded_song
  belongs_to :playlist
end

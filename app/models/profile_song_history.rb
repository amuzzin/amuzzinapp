class ProfileSongHistory < ActiveRecord::Base
  attr_accessible :uploaded_song_id, :user_id, :set_at, :updated_at , :status
  belongs_to :user
  belongs_to :uploaded_song
  include PublicActivity::Model
end

class AccessToken < ActiveRecord::Base
  attr_accessible :provider, :secret, :token, :user_id
end

class UserHistory < ActiveRecord::Base
  attr_accessible :uploaded_song_id, :user_id
  belongs_to :user 
  belongs_to :uploaded_song
  #include PublicActivity::Model
  #tracked owner: ->(controller, model) { controller && controller.current_user }
end

class ChatMessage < ActiveRecord::Base
  attr_accessible :sender_id, :recipient_id, :recipient_type
  has_many :group_logs, as: :event_data
  belongs_to :recipient, polymorphic: true
end

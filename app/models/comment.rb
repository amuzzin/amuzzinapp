class Comment < ActiveRecord::Base
  opinio
  include PublicActivity::Model
  tracked owner: ->(controller, model) { controller && controller.current_user }
  tracked recipient: ->(controller, model) { model && model.commentable.user }

  after_create :inform_owner 
  before_destroy :destroy_feeds 

  def destroy_feeds
    cleanup_query = "delete from activities where trackable_type='Comment' and trackable_id='#{self.id}'" 
    ActiveRecord::Base.connection.execute cleanup_query
  end 

  def inform_owner 
  	if commentable_type == "UploadedSong"
  		up = UploadedSong.find(commentable_id)
  		return if up.name.blank?
    	$redis.publish("realtime_#{up.user_id}","{\"id\":#{self.owner_id},\"type\":\"comment\",\"name\":\"#{User.find(self.owner_id).name}\",\"song\":\"#{up.name.strip.squish}\",\"body\":\"#{self.body}\",\"icon\":\"#{self.owner.get_profile_pic}\",\"song_id\":\"#{self.commentable_id}\"}")
    end
  end
end

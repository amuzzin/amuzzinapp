class Playlist < ActiveRecord::Base
  attr_accessible :name
  has_many :playlist_songs
  has_many :uploaded_songs, :through => :playlist_songs
  belongs_to :user
  
 
  include PublicActivity::Model
  tracked owner: ->(controller, model) { controller && controller.current_user }
  
  scope :public, where("public='true'") 

  def self.search_name(name,page,count,star=false)
    self.search  "@name #{name} ",:page => page, :per_page => count # TODO add public label
  end

  def art(size)
    playlist_songs = self.uploaded_songs
    if playlist_songs.blank?
      return '/default_'+size+'.jpg'
    else
      return playlist_songs[0].art(size)
    end
  end
  
  def status
    if public
      return 'public'
    else
      return 'private'
    end
  end

  def is_public?
    public
  end
  
end

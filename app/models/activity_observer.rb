class ActivityObserver < ActiveRecord::Observer
  observe PublicActivity::Activity

  def after_create(activity)
  	return if activity.owner_id.blank?
	user = User.find(activity.owner_id) 
  	user.get_friend_ids.each do |e|
  	  $redis.publish("realtime_#{e}","{\"type\":\"feed\",\"feed_id\":\"#{activity.id}\",\"id\":\"#{activity.owner_id}\"}")
  	end
  end
end
class ProfileSong < ActiveRecord::Base
  attr_accessible :uploaded_song_id, :user_id, :status

  belongs_to :user
  belongs_to :uploaded_song
  
  after_save :set_profile_history

  def set_profile_history
    if self.uploaded_song_id
      history_record = ProfileSongHistory.new
      history_record.status = self.status
      history_record.uploaded_song_id = self.uploaded_song_id
      history_record.set_at = self.created_at
      history_record.updated_at = Time.now
      history_record.user_id = self.user_id
      history_record.save!
      history_record.create_activity key: 'user.set_profile_song', owner: self.user
    end
  end
end

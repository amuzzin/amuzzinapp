class GroupLog < ActiveRecord::Base
	attr_accessible :user_id, :group_id, :event_type
	belongs_to :user
	belongs_to :group
	belongs_to :event_data, polymorphic: true
end

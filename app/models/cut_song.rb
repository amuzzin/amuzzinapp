require 'digest/md5'

class CutSong < ActiveRecord::Base
  attr_accessible :start_duration, :end_duration 
  belongs_to :uploaded_song
  belongs_to :user
  has_many :shared_cuts
  #=======================#
  # Parse seconds to min:sec 
  # input : 134
  # output 2.14
  #=======================# 
  
  after_create :split
  
  def self.shares(user_id)
    g_count = " select count(*) from shared_cuts sc 
    inner join cut_songs ct on ct.id=sc.cut_song_id and ct.user_id=#{user_id}"
    ActiveRecord::Base.connection.select_rows(g_count).first.first.to_i
  end 

  def set_shortened_url(host)
    url = "http://"+host+"/ct/"+self.url
    cmd = "curl https://www.googleapis.com/urlshortener/v1/url -H 'Content-Type: application/json'  -d    '{\"longUrl\": \"#{url}\"}'"
    json_output = `#{cmd}`
    require 'json'
    self.shortened_url= JSON.parse(json_output)["id"].split("http://").last
    puts " Setting Shortened URL: #{self.shortened_url} for cut_song: #{self.id}"
    self.save!
 end 

  def split
    unless (self.start_duration.eql?(0) and self.end_duration.eql?(self.uploaded_song.duration))
      self.mp3splt(self.start_duration.to_i,self.end_duration.to_i)
    else
      self.path = self.uploaded_song.path
      self.duration = self.uploaded_song.duration
    end
    self.url = self.generate_url
    self.save!
  end
  
  def parse(duration)
    min = duration / 60
    sec = duration % 60
    return "#{min}.#{sec}"
  end
  
  def humanize(duration)
    min = duration / 60
    min = '0'+min.to_s if min < 10 
    sec = duration % 60
    sec = '0'+sec.to_s if sec < 10
    return "#{min}:#{sec}"
  end
  
  def max(first,second)
    if first > second
      return first
    else
      return second
    end
  end
  
  def min(first,second)
    if first < second
      return first
    else
      return second
    end
  end
  

  #=====================================#
  # Executing mp3splt and saving the output
  # output is in same audios folder 
  # TODO: Handle edge cases
  #====================================#
 def mp3splt(start_duration,end_duration)
    # resolving issues on reverse cut
    self.start_duration = min(start_duration,end_duration)
    self.end_duration = max(start_duration,end_duration)
    self.duration = self.end_duration - self.start_duration
    self.save!
    # execute the mp3splt command
    command = `mp3splt \"#{self.uploaded_song.path}\" #{parse(start_duration)} #{parse(end_duration)}`
    # read cut file name from the log
    cut_song_path = command.split('"')[1]
    self.path = cut_song_path if cut_song_path.include? ".mp3"
    self.save!
  end
  
  
  def generate_url 
    digest =  Digest::MD5.hexdigest("#{id}#{start_duration}#{end_duration}#{uploaded_song_id}#{path}#{created_at}#{finger_print}#{comments}")
    return digest
  end
  
  # return either the comment or the song title of the uploaded song
  def get_comment
    if self.comments.present?
      return self.comments
    else
      return self.uploaded_song.title
    end
  end
  
  def get_user
    if self.user.has_fb_auth?
      self.user.authentications.find_by_provider('facebook').uid
    end
  end

  def src_path
    self.path.gsub('public','')
  end
end

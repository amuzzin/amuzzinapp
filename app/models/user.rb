require 'digest/md5'
require 'geokit'
class User < ActiveRecord::Base

  include Amistad::FriendModel
  include Artist::Insight 
  include PublicActivity::Model

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:confirmable
  has_many :authentications, dependent: :destroy
  # Setup accessible (or ted) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :city, :image
  # attr_accessible :title, :body
  has_many :uploaded_songs
  has_many :cut_songs
  has_many :playlists
  has_many :favourites
  has_many :favourite_songs, :through => :favourites, :source => :uploaded_song
  has_many :groups, :foreign_key => 'started_by_id'
  has_one :profile_song
  has_many :profile_song_histories
  has_many :group_members
  has_many :groups, :through => :group_members
  has_many :chat_messages, as: :recipient
  has_many :user_histories

  letsrate_rater

  mount_uploader :image, ImageUploader

  #geocoded_by :address => :current_sign_in_ip , :latitude  => :lat, :longitude => :lng # ActiveRecord
  reverse_geocoded_by  :lat  , :lng  do |obj,results|
    if geo = results.first
      obj.city    = geo.city
      obj.state    = geo.state
      obj.country_code = geo.country_code
      obj.address = geo.address
    end
    obj.save!
    "#{obj.city},#{obj.state},#{obj.country_code}"
  end

  validates :name , :length => { :minimum => 6 , 
    :maximum => 30 , 
    :too_short => " should have atleast 6 characters",
    :too_long => " cannot exceed 30 characters"
  }

  validates_each :name do |record,attr,values|
    unless values =~ /[a-zA-Z]+\s+[a-zA-Z]/
      record.errors.add(attr,' should have both first and last name')
    end
  end

  def self.cut_off 
    return 100
  end 

  def enable_upload?
    return (enable_upload and (song_count < User.cut_off))
  end

  def get_location_str 
    return "#{self.city},#{self.country_code}"
  end  

  def self.search_name(name,page,count,star=false)
    User.search "@name #{name}", :page => page, :per_page => count
  end

  def self.active
    where("last_seen > ?", Time.now - 2.minutes)
  end
  
  def generate_password
    self.password = Devise.friendly_token
  end

  def apply_omniauth(omniauth)
    provider = omniauth['provider']
    if provider == 'facebook' or provider == 'github'
      self.email = omniauth['extra']['raw_info']['email'] if email.blank?
      self.name  = omniauth['extra']['raw_info']['name'] if name.blank?
    elsif provider == 'google_oauth2' 
      self.email = omniauth['info']['email'] if email.blank?
      self.name  = omniauth['info']['name'] if name.blank?
    end
    authentications.build(provider: omniauth['provider'], uid: omniauth['uid'])
  end
  
  def get_gravatar
    digest = Digest::MD5.hexdigest(self.email.strip.downcase)
    return "http://www.gravatar.com/avatar/#{digest}?s=200"
  end
  
  def fb_dp(type)
    auth = self.authentications.where(:provider => 'facebook')
    if auth.blank?
      return nil
    else
      uid = auth[0].uid 
      return "http://graph.facebook.com/#{uid}/picture?type=#{type}"
    end
  end
  
  def get_profile_pic(type='large')
      # For carrierwave & imagemagick
      if self.image_url
        if type.eql?("original")
          return "#{IMG_CDN}#{self.image_url}"
        elsif type.eql?('large')
          return "#{IMG_CDN}#{self.image_url('big')}"
        end
        return "#{IMG_CDN}#{self.image_url(type.to_sym)}"
      end
    return fb_dp(type) || get_gravatar
  end


  def get_details_json
    user = {
      'user_id' => self.id,
      'user_name' => self.name,
      'user_pic' => self.get_profile_pic,
      }
  end
  
  def update_geo 
    if self.lat.blank?
      require 'json'
      res = `curl https://dazzlepod.com/ip/#{self.current_sign_in_ip}.json`
      val = JSON.parse(res)
      self.lat = val["latitude"] 
      self.lng = val["longitude"]
      self.city = val["city"]
      self.provider = val["organization"] 
      self.country_code = val["country"] 
      self.state = val["region"] 
      self.save!
    end
  end 

  def current_song
    group = Group.find_by_id($redis.hget("user_group",self.id))
    if group.present? && is_group_listen_enabled?
      return UploadedSong.find_by_id($redis.hget("group_song",group.id))
    else
      return UploadedSong.find_by_id($redis.hget("current_song",self.id))
    end
  end

  def current_queue
    group = Group.find_by_id($redis.hget("user_group",self.id))
    if group.present? && is_group_listen_enabled?
      return UploadedSong.find_all_by_id($redis.hget("group_queue",group.id).to_s.split(',')).compact
    else
      return UploadedSong.find_all_by_id($redis.hget("current_queue",self.id).to_s.split(',')).compact
    end
  end
  
  def get_loc
   location = Geokit::Geocoders::MultiGeocoder.geocode(self.city)
   return location
  end
  
  def has_fb_auth?
    self.authentications.find_by_provider('facebook').present?
  end

  def send_reset_password_email
    generate_reset_password_token!
    UserMailer.reset_password_instructions(self).deliver
  end
  
  def get_fb_id
    fb_auth = self.authentications.find_by_provider('facebook')                                                                                       
    fb_auth.present? ? fb_auth.uid : nil
  end

  def get_friends
    user_friend_ids = get_friend_ids
    User.find_all_by_id(user_friend_ids)
  end

  def get_friend_ids
    friends = $redis.hget("friends",self.id)
    user_friends = friends.present? ? eval(friends) : []
    user_friends.map! { |f| f.split('realtime_')[1] }
  end

  def current_group
    group_id = $redis.hget('user_group',self.id)
    Group.find(group_id) if group_id 
  end

  def is_group_listen_enabled?
    $redis.hget('group_enabled',self.id).present?
  end

  def get_intersect_friend_ids 
    online_ppl = $redis.pubsub("channels")
    friends = $redis.hget("friends",self.id)
    if online_ppl.blank? or friends.blank?
      return [] 
    else 
      friends = eval(friends)
      return [] if friends.class != Array
     # online_ppl = online_ppl.split.select{|e| e =~ /^realtime_/ } 
      intersect = friends & online_ppl 
    end
  end

  def get_online_count 
    return get_intersect_friend_ids.size
  end 

  def get_online_friends
    get_intersect_friend_ids.collect do |e|
      User.find(e.split('realtime_')[1])
    end 
  end

  def is_friend?(user)
    get_friend_ids.include?(user.to_s)
  end

  def get_friends_json 
    friends_status = {}
    online_friends = get_intersect_friend_ids
    self.get_friends.each do |e|
      if online_friends.include?("realtime_#{e.id}")
        friends_status[e.id] = 1
      else 
        friends_status[e.id] = 0 
      end 
    end 
    return friends_status.to_json
  end 

  def sync_facebook_friends(access_token,uid=nil)
    uid = self.authentications.find_by_provider("facebook").uid if uid.nil?
    user_fb = FbGraph::User.fetch(uid,
         :access_token => access_token) 
    tfriends = user_fb.friends
    tfriends.each do |tf|
      auth = Authentication.find(:first,
        :conditions => " provider='facebook' and uid='#{tf.identifier}' ")
      next unless auth
      self.invite(auth.user)
      auth.user.approve(self)
    end
    publish
    return true
  end

  def notify_new_friend_signup
    self.get_friends.each do |f|
      UserMailer.new_friend_signup(f,self).deliver
    end
  end

  def publish
    friend_ids = self.friends.collect{|e| "realtime_#{e.id}"}
    $redis.hset("friends",self.id,friend_ids.to_s)
  end

  def notify_signup(origin) 
    HIPCHAT_CLIENT['amuzzin-signups'].send('amuzzinapp', "#{self.email} Signed up using #{origin}.", :notify => true , :color => 'green')
  end

  def get_unread_message_count
    restrict_conditions = "recipient_id = #{self.id} and (owner_id is null or owner_id != #{self.id}) and is_read = false" 
    unread = PublicActivity::Activity.count(:all,:conditions => restrict_conditions) 
  end

  def set_profile_song(song_id,status="")
    ProfileSong.find_or_create_by_user_id(self.id)
    .update_attributes({     
        :uploaded_song_id => song_id,
        :status => status})
  end

  def get_new_friend_uploads time
    friends = get_friend_ids
    if friends.present?
      UploadedSong.all(:conditions =>"created_at >= '#{time}' and user_id in (#{friends.join(',')})")
    else
      []
    end
  end
end

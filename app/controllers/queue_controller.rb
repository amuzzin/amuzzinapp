class QueueController < ApplicationController

  def mass_add_to_current_queue
    unless current_user.blank?
      @mass_current_queue = []
      @queue = params[:id].split(',')
      # increase queue count of song
      @queue.each { |x| $redis.hincrby("queue_count",x.to_s,1) }
      value = $redis.hget("current_queue",current_user.id.to_s)
      final_list=(Array(value)+@queue).uniq
      $redis.hset("current_queue",current_user.id.to_s,final_list)
    end
    respond_to do |format|
      format.json { render :nothing =>  true }
    end
  end
  
  def add_to_current_queue
    # user's email is the key for current queue
    unless current_user.blank?
      @current_queue = []
      # increase queue count of song
      $redis.hincrby("queue_count",params[:id],1)
      value=$redis.hget("current_queue",current_user.id.to_s)
      @current_queue = value unless value.blank?
      @current_queue.push(params[:id].to_i) unless @current_queue.include?(params[:id].to_i)
      $redis.hset("current_queue",current_user.id.to_s,@current_queue)
    end
    respond_to do |format|
      format.json { render :nothing =>  true }
    end
  end
  
  def remove_from_current_queue
    # user's email is the key for current queue
    unless current_user.blank?
      # reduce queue count of song
      prev_q_count = $redis.hget("queue_count",params[:id])
      if prev_q_count && prev_q_count !=0
        $redis.hincrby("queue_count",params[:id],-1)
      end   
      queue=$redis.hget("current_queue",current_user.id.to_s)
      queue.delete(params[:id].to_i)
      #decrease song count if it is the only song in queue
      unless queue.any?
        $redis.hdel("current_song",current_user.id.to_s)
        prev_s_count = $redis.hget("song_count",params[:id])
        if prev_s_count && prev_s_count !=0
          $redis.hincrby("song_count",params[:id],-1)
        end  
        $redis.hdel("current_queue",current_user.id.to_s)
      else
        $redis.hset("current_queue",current_user.id.to_s,queue)
      end
      
    end
    flash.now[:notice]="Song removed from queue"  
    render :template=>'layouts/show_generic_flash'
  end
  
  def clear_current_queue
    #clear current queue,current song and decrease their counts
    unless current_user.blank?
      queue= $redis.hget("current_queue",current_user.id.to_s)
      queue.each do |x|
        prev_q_count = $redis.hget("queue_count",x)
        if prev_q_count && prev_q_count !=0 
          $redis.hincrby("queue_count",x.to_s,-1)
        end
      end
      current_song=$redis.hget("current_song",current_user.id.to_s)
      prev_s_count = $redis.hget("song_count",current_song.to_s)
      if prev_s_count && prev_s_count !=0
        $redis.hincrby("song_count",current_song.to_s,-1)
      end  
      $redis.hdel("current_queue",current_user.id.to_s)
      $redis.hdel("current_song",current_user.id.to_s)
    end
    flash.now[:notice]="Cleared queue"  
    render :template=>'layouts/show_generic_flash'
  end
  
  def get_current_queue
    unless current_user.blank?
      @current_queue=$redis.hget("current_queue",current_user.id.to_s)
      if @current_queue.blank? 
        return 
      end
      @current_queue=@current_queue.split(',')
      p @current_queue
      @current_queue.map! {|x| UploadedSong.find_by_id(x)}
      song=$redis.hget("current_song",current_user.id.to_s)
      @current_song=song if song.present?
    end
  end
  
  def get_queue_from_cookie
      @current_queue=params[:queue].split(',')
      if @current_queue.blank? 
        return 
      end
      @current_queue.map! {|x| UploadedSong.find_by_id(x)}
      song=params[:song]
      @current_song=song if song.present?
  end

  def get_group_queue
    unless current_user.blank?
      @group = Group.find_by_id($redis.hget("user_group",current_user.id))
      if @group.present?
        @current_queue=$redis.hget("group_queue",@group.id)
        if @current_queue.blank? 
          return 
        end
        @current_queue=@current_queue.split(',')
        p @current_queue
        @current_queue.map! {|x| UploadedSong.find_by_id(x)}
        song=$redis.hget("group_song",@group.id)
        @current_song=song if song.present?
      else
        return
      end
    end
  end

  def add_songs_to_queue
    unless current_user.blank?
      @queue = params[:id].split(',').map! {|s| UploadedSong.find_by_id(s)}
      @user  = params[:user]
      @group = $redis.hget("user_group",current_user.id.to_s)
      @group_song = $redis.hget("group_song",@group)
    end
  end
  
end
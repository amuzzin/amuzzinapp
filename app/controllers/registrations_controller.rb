class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
  end

  def update
    super
  end

  protected
  def after_inactive_sign_up_path_for(resource)
    resource.delay.notify_signup("Native")
  end
end

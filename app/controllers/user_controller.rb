class UserController < ApplicationController
  
  def profile
    @user = User.find_by_id(params[:id])
    if @user
      @user_songs = @user.uploaded_songs.where("song_master_id is not null").paginate(:page => params[:song_page], :per_page => 5)
      @user_playlists = @user.playlists.public.paginate(:page => params[:playlist_page], :per_page => 5)
    end
  end
  
  def update_last_seen
    unless current_user.blank?
      current_user.last_seen = Time.now
      current_user.save!
    end 
    respond_to do |format|
      format.json { render :nothing =>  true }
    end
  end
  
  def my_song
    unless current_user.blank?
      @my_songs = current_user.uploaded_songs.where("song_master_id is not null").order("updated_at desc").paginate(:page => params[:page], :per_page => 12) 
    end
  end

  def all_friends 
    unless current_user.blank?
      @friends = current_user.friends.order("name asc").paginate(:page => params[:page], :per_page => 15)
    end  
  end
  
  def friends_stream
    unless current_user.blank?
      @mapped_friends_geojson = {}
      @online_friends = current_user.get_online_friends
      @online_friends.each {|user| 
        geojson_det = {}
        geojson_det["name"] = user.name 
        geojson_det["lat"] = user.lat
        geojson_det["lng"] = user.lng
        geojson_det["img"] = user.get_profile_pic("small")
        @mapped_friends_geojson[user.id] = geojson_det.clone
       }
       
      @friends_current_song = []
      @online_friends.each {|user| 
        if user.current_group && user.is_group_listen_enabled?
          csong = UploadedSong.find_by_id($redis.hget("group_song",user.current_group.id))
        else
          csong = UploadedSong.find_by_id($redis.hget("current_song",user.id))
        end
        if csong.blank?
          csong_tit = "Currently Idle"
          csong_url = "#"
        else 
          csong_tit = csong.title[0..20]
          csong_tit << "..." if csong.title.size > 20
          csong_url = "/s/#{csong.id}"
        end
        @friends_current_song << csong
        @mapped_friends_geojson[user.id]["current_song"] = csong_tit
        @mapped_friends_geojson[user.id]["current_song_url"] = csong_url

      }

      @mapped_friends_geojson.clone.each{|k,v| 
        if v["lat"].blank? or v["lng"].blank?
          @mapped_friends_geojson.delete(k)
        end 
      }
      @mapped_friends_geojson = @mapped_friends_geojson.to_json
      @pending_requests_count = current_user.pending_invited_by.count rescue 0 #paginate(:page => params[:page], :per_page => 7)


      p @friends_current_song
    end
  end

  def add_friend 
    if current_user
      friend = User.find(params[:user])
      if current_user.invited? friend 
        flash.now[:notice]="Friend Request already sent to #{friend.name} "
      elsif current_user.friend_with? friend 
        flash.now[:notice]="You are already in #{friend.name}'s Friend Stream "
      else
        current_user.invite friend
        flash.now[:notice]="Friend Request sent to #{friend.name} "
      end 
      current_user.publish
      friend.publish
      current_user.create_activity key: 'user.add_friend', recipient: friend  
      $redis.publish("realtime_#{friend.id}","{\"type\":\"add_friend\",\"from\":\"#{current_user.name}\",\"icon\":\"#{current_user.get_profile_pic}\"}")
      render :template=>'layouts/show_generic_flash'
    end
  end

  def block_friend 
    friend = User.find(params[:user])
    current_user.block friend
    flash.now[:notice]="#{friend.name} blocked"
    current_user.publish
    friend.publish
    render :template=>'layouts/show_generic_flash'
  end   

  def unblock_friend 
    friend = User.find(params[:user])
    current_user.unblock friend
    flash.now[:notice]="#{friend.name} unblocked"
    current_user.publish
    friend.publish
    render :template=>'layouts/show_generic_flash'
  end  


  def accept_friend
    if params[:activity]
      activity = PublicActivity::Activity.find_by_id(params[:activity],:conditions => "recipient_id = #{current_user.id}")
      if activity.present?
        friend = activity.trackable 
        activity.parameters = {:used => true}
        activity.save!
      end
    else
      friend = User.find(params[:user])
    end
    if friend
      current_user.approve friend
      flash.now[:notice]="#{friend.name} is now your friend"
      current_user.publish
      friend.publish
      current_user.create_activity key: 'user.accept_friend', recipient: friend 
      $redis.publish("realtime_#{friend.id}","{\"type\":\"accept_friend\",\"from\":\"#{current_user.name}\",\"icon\":\"#{current_user.get_profile_pic}\"}") 
    else
      flash.now[:notice]="Your request cannot be processed"
    end
    render :template=>'layouts/show_generic_flash'
  end  

  def link_youtube_url
  #  require 'youtube_it'
  #  client = YouTubeIt::Client.new
  #  video = client.video_by(params[:youtube_url])
    usong = UploadedSong.find_by_id(params[:id])
    if usong.blank? or usong.user_id != current_user.id 
      render :text => "Invalid operation"
    else 
      usong.youtube_url = params[:original_url]
      usong.video_url = params[:youtube_url]
      usong.video_img = params[:youtube_img_url]
      usong.save!
      flash.now[:notice]="Successfully linked Youtube video to your Song"
      render :template=>'layouts/show_generic_flash'
    end
  end
  
  def friend_songs
    friend = User.find_by_id(params[:id])
    if friend.current_group && friend.is_group_listen_enabled?
      friend_queue_ids = $redis.hget("group_queue",friend.current_group.id)
    else
      friend_queue_ids = $redis.hget("current_queue",friend.id)
    end
    unless friend_queue_ids.blank?
      friend_queue_ids = friend_queue_ids.split(',')
      @friend_queue = friend_queue_ids.map{|song| UploadedSong.find_by_id(song)}
    end
  end

  def pending_friend_requests
    if current_user
      @pending_requests = current_user.pending_invited_by #.paginate(:page => params[:page], :per_page => 7)
    end
    render :partial => "pending_friend_requests" , :locals => {:notifications => @pending_requests}
  end

  def set_profile_song 
    if current_user
      current_user.set_profile_song(params[:song_id],params[:status])
      flash.now[:notice]="Your profile song has been updated successfully! Click on 'My Profile' link at top right corner for preview."
    else
      flash.now[:notice]="You need to Login to perform this action!"
    end
    render :template=>'layouts/show_generic_flash'
  end 

  def location 
    if current_user.blank?
      render :text => "Invalid"
    else
      lat = BigDecimal.new params[:latitude]
      lng = BigDecimal.new params[:longitude]
      user = current_user
      user.lat = lat 
      user.lng = lng 
      user.save!
      result = user.reverse_geocode
      render :text => result
    end
  end

  def generate_new_password_email
    user = User.find_by_email(params[:email])
    if user.present?
      flash.now[:notice] = "Reset password instructions have been sent to #{user.email}."
      user.send_reset_password_email
    else
      flash.now[:notice] = "User with the email '#{params[:email]}' doesn't exist in Amuzzin. \n Please signup!"
    end
    render :template=>'layouts/show_generic_flash'
  end


  def notify_missed_call 
    friend = User.find_by_id(params[:user_id])
    group = Group.find_by_id(params[:group_id])
    ActiveRecord::Base.observers.disable :activity_observer
    group.create_activity key: 'group.missed_call', owner: friend , recipient: current_user
    $redis.publish("realtime_#{current_user.id}","{\"type\":\"missed_call\",\"from\":\"#{friend.name}\",\"icon\":\"#{friend.get_profile_pic}\"}")
    ActiveRecord::Base.observers.enable :activity_observer
    render :nothing => true
  end

  def my_groups 
    #@gh = Group.select("group_id,max(left_at) as left_at")
    #.where("user_id ='#{current_user.id}'")
    #.group("group_id").
    #order("left_at desc").
    #limit(5)

    @groups = Group.joins("INNER JOIN group_members gm on gm.group_id=groups.id and gm.user_id=#{current_user.id}").
    order("created_at desc")
    #.limit(5)
    render :partial => "my_groups"
  end 

  # Artist stats JSON responders

  def timeline_insight
    render :text => current_user.artist_insight_data.to_json
  end

  def favourite_insight
    render :text => current_user.favourite_insight.to_json
  end

  def listend_insight
    render :text => current_user.listend_insight.to_json
  end

  def queued_insight
    render :text => current_user.queued_insight.to_json
  end
end

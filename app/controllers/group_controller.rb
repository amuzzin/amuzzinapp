class GroupController < ApplicationController	

  
  def group_listen
    if current_user
      group = $redis.hget("user_group",current_user.id)
      if group
        @group = Group.find_by_id(group)
        @group_members =  @group.users
        @online_friends = current_user.get_online_friends
      end
    end
    render :group_listen
  end

  def group_log
    @id = params[:id]
    @type = params[:type]
    @index = params[:index].to_i
    @size = params[:size].to_i
    @group = current_user.current_group
  end

  def start_group_listen
    if current_user
      Group.create_group(params[:name],current_user)
    end
    group_listen
  end


  def group_request
    begin 
      @leave_group_id = $redis.hget("user_group",current_user.id)
      @group = Group.find(params[:id])
      @user = User.find_by_id(params[:user_id])
      @request_user = @user.name rescue "Unknown User"
      render :partial => "group_request"
    rescue
      render :text => "Invalid or No access to this Group"
    end 
  end

  def leave_group
  	if current_user
  	  @group = Group.find_by_id($redis.hget("user_group",current_user.id))
  	  if @group
        @group.leave(current_user)  		    
  	    flash.now[:notice] = 'You left the group'
        render :template => 'layouts/show_flash'
        return
  	  end
  	end
    render :nothing => true
  end

  def join_group
    if current_user
      if params[:id]
        activity = PublicActivity::Activity.find_by_id(params[:id],:conditions => "recipient_id = #{current_user.id}")
        if activity.present?
          group = activity.trackable 
          activity.parameters = {:used => true}
          activity.save!
        end
      else  
        group = Group.find_by_id(Group.get_invite_group(current_user.id))
      end
      #current_user.groups.each {|g| g.leave(current_user)}
      group.join(current_user) if group.present?
    end
    render :nothing => true
  end


  def switch_group
    if current_user
      if params[:id]
        group = Group.find_by_id(params[:id])
        if group.present? && current_user.groups.include?(group)
          group.switch(current_user)
        end
      end
    end
    render :nothing => true
  end

  def enable_group
    if current_user     
      group = $redis.hget("user_group",current_user.id)
      if group
        group = Group.find_by_id(group)
        group.enable(current_user)
      end
    end
    render :nothing => true
  end

  def disable_group
    if current_user     
      group = $redis.hget("user_group",current_user.id)
      if group
        group = Group.find_by_id(group)
        group.disable(current_user)
      end
    end
    render :nothing => true
  end

  def invite_to_group
    if current_user
      group_id = $redis.hget("user_group",current_user.id)
      invite_user_group = $redis.hget("user_group",params[:invite_user])
      #If user is not in a group or if invited user is already in a group drop the invite
      message = {
        "type" => "group_listen_request",
        "group_id" => group_id,
        "user" => current_user.get_details_json
      }
      if group_id.present?
        Group.set_group_invite(current_user.id,params[:invite_user],group_id)
        $redis.publish("realtime_#{params[:invite_user]}",message.to_json)
      end
    end
    render :nothing => true
  end

  def invite_offline_friends
    if current_user
      @offline_friends = (current_user.get_friends - current_user.get_online_friends) - current_user.current_group.users
    end
  end

  def call_offline_friend
    if current_user && current_user.get_friend_ids.include?(params[:id])
      ActiveRecord::Base.observers.disable :activity_observer
      current_user.current_group.create_activity key: 'group.missed_call', owner: current_user , recipient: User.find(params[:id])
      ActiveRecord::Base.observers.enable :activity_observer
    end
    render :nothing => true
  end


  def ignore_group_request
    if current_user
      group_id = Group.get_invite_group(current_user.id)
      if group_id.present?
        invited_by = Group.get_invited_by(current_user.id)
        Group.clear_group_invite(current_user.id)
        message = {
            "type" => "group_listen_request_ignored",
            "user" => current_user.get_details_json
          }
        $redis.publish("realtime_#{invited_by}",message.to_json)
      end
    end
    render :nothing => true
  end

end

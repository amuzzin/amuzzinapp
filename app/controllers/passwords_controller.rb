class PasswordsController < ApplicationController
  include ActionView::Helpers::JavaScriptHelper

  #before_filter :authenticate_user!, :except => [:request_password_reset, :reset_password_through_email]
  def edit
    render :layout => false
  end
  
  def request_password_reset
    if request.method == 'POST'
      unless params[:email]
        flash.now[:alert] = "Please enter a valid email"
      else  
        user = User.find_by_email(params[:email])
        if user
          user.send_reset_password_email
          flash.now[:notice] = "We have sent the email with password instructions. Please check your inbox"
        else
          flash.now[:alert] = "No such email exists. Check for spelling errors!"
        end
      end
      render :template=>'layouts/show_generic_flash'
    end
  end

  def validate_and_reset_password
    @user = User.find(params[:id])
    if @user.reset_password!(params[:password], params[:password_confirmation])
      msg = "Your password has been reset successfully"
      sign_in User.find(params[:id].to_i), :bypass => true
      render_fn = "disable_onbeforeunload();show_flash('#{escape_javascript(msg)}',false);window.location.href='/'"
    else
      msg = parse_errors @user
      render_fn = "show_flash('#{escape_javascript(msg)}',false);"
    end
    render :js => render_fn
  end

  def reset
    if request.method == 'POST'
      if check_token
        validate_and_reset_password
      else
        render :js => "show_flash('Cannot reset password')"
      end
    else 
      @valid = check_token  
      @id = params[:id]
      @token = params[:token]  
    end
  end

  def update
    @user = current_user
    hash = {}
    hash[:current_password] = params[:user][:current_password]
    hash[:password] = params[:user][:password]
    hash[:password_confirmation] = params[:user][:password_confirmation]
    if @user.update_with_password(hash)
      msg = 'Your Password has been updated'
      sign_in @user, :bypass => true
      render_fn = "disable_onbeforeunload();show_flash('#{escape_javascript(msg)}',true);location.reload();"
    else
      msg = parse_errors @user
      render_fn = "show_flash('#{escape_javascript(msg)}',false);"
    end
    render :js => render_fn
  end

  private
  def check_token
    return false if params[:id].blank? || params[:token].blank?
    user = User.find_by_id(params[:id])
    if user && user.reset_password_token == params[:token]
      true
    else
      false
    end
  end
end

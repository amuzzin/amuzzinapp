class CtController < ApplicationController

  before_filter :authenticate_user! , :except => [:handle_url]

  def handle_upload_song_url 
    #TODO: Add appropriate code for share url
  end 

  def set_user
    if @cut_song.user.present?
      @username = @cut_song.uploaded_song.user.email.gsub('@',' [at] ') #if @cut_song.user.present? and @cut_song.user.email?
    else 
      @username = "Anonymous"
    end
  end

  def set_authorization
    if @cut_song.shared.eql? 'private' 
      @authorized = is_authorized?(current_user,@cut_song)
      if @authorized
        @shared_user_pic = current_user.get_profile_pic
      end
    elsif @cut_song.shared.eql? 'public' or @cut_song.user.eql? current_user
       @authorized = true 
    end
  end

  def handle_url
    @cut_song = CutSong.find_by_url(params[:id])
    p " cut song is #{@cut_song}"
    # Checked in application layout to either show menus or show login page
    @cut_page = true 

    set_user
    set_authorization
    
    respond_to do |format|
      format.js
    end
  end
  
  def is_authorized?(user,ct)
    auth = user.authentications.find_by_provider('facebook') if user.present? and user.authentications.present?
    if auth.present?
      if ct.shared_cuts.find_by_uid(auth.uid).present?
        return true
      end
    end
    if ct.shared_cuts.blank? or ct.uploaded_song.user_id.eql? current_user.id
      return true 
    end
    return false
 end

    
end

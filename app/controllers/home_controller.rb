class HomeController < ApplicationController
  require 'json'
  require 'uri'
  def index
    @top20 = $redis.get('heap')
    @history=[]
    unless @top20.blank?
      @top20 = (JSON.parse @top20).select{|e| e if e["song"].present? }
      @history = current_user.blank? ? (URI.decode(cookies['QUEUE']) rescue [] ): $redis.hget("current_queue",current_user.id.to_s)
      @history = @history.blank? ? @top20.collect{|x| x['song']}.compact : @history.split(',').compact
      @top20.each {|x| x['song']=UploadedSong.find_by_id(x['song'])}
      @top20.sort! { |a,b| compare(a,b) }
      @top20 = @top20[0..19]
    end
    
    recommended = @history.map {|h| Recommender.for(h.to_s)}
    recommended.flatten!
    @recommended = recommended.map { |r| r.item_id}
    @recommended.uniq!
    @recommended = @recommended - @history
    @recommended = @recommended[0..19]
  end
  
  def compare(a,b)
    if a['song_count'].to_i!=b['song_count'].to_i
      return  b['song_count'].to_i - a['song_count'].to_i
    else
      return  b['queue_count'].to_i - a['queue_count'].to_i
    end
  end
  
end
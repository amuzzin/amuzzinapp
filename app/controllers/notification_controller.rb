class NotificationController < ApplicationController
  
  def notification
    if current_user
      restrict_conditions = " recipient_id = #{current_user.id} and (owner_id is null or owner_id != #{current_user.id})" 
  	  @notifications = PublicActivity::Activity.where(restrict_conditions).order("created_at desc").paginate( :page => params[:page],
  	 	 :per_page => 30)
      set_as_read
    end

  end

  def hover
  	notification
  end

  def set_as_read
    if current_user
      query = "update activities set is_read = true where recipient_id = #{current_user.id} and (owner_id is null or owner_id != #{current_user.id})"
      ActiveRecord::Base.connection.execute(query)
    end
  end

end

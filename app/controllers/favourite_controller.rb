class FavouriteController < ApplicationController
  

  def add_favourite
    unless current_user.blank?
      check_unique_and_add(params[:s_id])
    else
      flash.now[:notice]='Please log in to favourite this song'
    end
    respond_to do |format|
        format.js
    end
  end
  
  def check_unique_and_add(song)
    unique=Favourite.find(:all,:conditions=>{:uploaded_song_id=>song,:user_id=>current_user})
    if unique.blank?
      @fav_song=UploadedSong.find(song)
      current_user.favourite_songs<<@fav_song
      flash.now[:notice]='Song added to your favourites'
    else
      flash.now[:notice]='Song is already in your favourites'
    end
  end
  
  def my_favourite
    unless current_user.blank?
      @favourites=current_user.favourite_songs.order("updated_at desc").paginate(:page => params[:page], :per_page => 7)
    end
  end
  
  def remove_favourite
    unless current_user.blank?
      @song_remove_id=params[:id]
      @record=Favourite.find(:all,:conditions=>{:uploaded_song_id=>params[:id],:user_id=>current_user.id})
      @record.each do |r|
        r.delete
      end
      flash.now[:notice]='Song has been removed from your favourites'
    else
      flash.now[:notice]='Please log in to favourite this song'
    end
    respond_to do |format|
        format.js
    end
  end

  def friends_list
    if current_user
      id = params[:id]
      favs = []
      friend_ids = current_user.get_friends.collect{|e| e.id}.join(',')
      if friend_ids
        favs = Favourite.find(:all,
          :conditions => " uploaded_song_id=#{id} and user_id in (#{friend_ids}) and user_id != #{current_user.id}",
          :order => "created_at desc")
      end 
      render :partial => "fav_friends" , :locals => {:favs => favs}
    end
  end

end

class SongDiscoveryController < ApplicationController
	def index
    @keywords = UploadedSong.keywords
	end

  def semantic_search
    keywords = params[:keywords].split(',').map{|k| k.to_sym} if params[:keywords].present?
    values = params[:values].symbolize_keys if params[:values].present?
    names = params[:display_names]
    begin
      if keywords.present?
        arguments = {:page=>params[:page],:most_heard => current_user,:least_heard => current_user,:unheard => current_user,:friend => current_user}
        @sentence = UploadedSong.semantic_sentence(keywords,:values => values,:display_names => names)
        @result = UploadedSong.semantic_search(keywords,:arguments => arguments,:values => values)
      end
    rescue Exception => e
      p e.backtrace
      @error = e.message
    end
  end
end

class AuthenticationsController < ApplicationController
  
  def index
    @authentications = current_user.authentications if current_user
  end

  def create_authentication(omniauth)
    if current_user
      authentications = current_user.authentications.find_by_provider(omniauth['provider'])
      unless authentications
        authentication = current_user.authentications.create(provider: omniauth['provider'], uid: omniauth['uid'])
      end
      return authentication
    end
  end

  def create
    omniauth = request.env["omniauth.auth"]
    session[:fb_token] = omniauth.credentials[:token]
    authentication = create_authentication omniauth

    if omniauth.blank?
      native_user(authentication)
    else
      fb_user(omniauth)
    end 
  end

  # User is authenticating through our native login
  def native_user(authentication)
    authentication.user.delay.update_geo
    flash[:notice] = "Login successful"
    authentication.user.delay.publish
    sign_in_and_redirect(:user, authentication.user)
  end

  #  User logs in for first time with facebook 
  #  Create a new entry for him 
  def fb_user(omniauth)
    user = User.find_by_email(get_email(omniauth))
    mail = false
    if user.blank?
      user = User.new 
      pass = user.generate_password
      user.skip_confirmation! 
      mail = true
    end 
    user.apply_omniauth(omniauth) 
    begin     
      if user.save!
        user.delay.update_geo  
        if omniauth['provider'] == 'facebook'
          user.sync_facebook_friends(session[:fb_token],omniauth['uid'])
        end 
        if mail
          UserMailer.send_password_for_facebookers(user, pass).deliver 
          user.delay.notify_signup('facebook') if Rails.env.production?
          user.delay.notify_new_friend_signup
        end 

        flash[:notice] = "Login with facebook successful"
        user.delay.publish
        sign_in_and_redirect(:user, user)
      else 
        log(user.errors) if Rails.env.production?
        flash[:alert] = "Some Error Occurred. Please signup using amuzzin"
        redirect_to root_path
      end
    rescue Exception=>e
      log(e) if Rails.env.production?
      raise e
      flash[:alert] = "Some Error Occurred. Please signup using amuzzin"
      redirect_to root_path
    end
  end
  
  def destroy
    @authentication = current_user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = "Log out successful"
    redirect_to authentications_url
  end

  protected
 
  # This is necessary since Rails 3.0.4
  # See https://github.com/intridea/omniauth/issues/185
  # and http://www.arailsdemo.com/posts/44
  def handle_unverified_request
    true
  end

  private
  def get_email(omniauth)
    provider = omniauth['provider']
    omniauth['extra']['raw_info']['email']
  end
 end

class ApplicationController < ActionController::Base

  rescue_from 'Exception' do |exception|
    log(exception) and return if Rails.env.production?
    raise exception
  end 

  protect_from_forgery
  require_dependency 'uploaded_song'
  include PublicActivity::StoreController

#  before_filter :refresh_session
#  before_filter :set_online_users

  # this is needed to prevent XHR request from using layouts
  # i.e., To prevent rendering application layout during ajax calls
  after_filter :store_location
  before_filter :prepare_for_mobile
  before_filter :set_cache_buster
  before_filter proc { |controller| (controller.action_has_layout = false) if controller.request.xhr? }
  before_filter proc { |controller| p controller.request.format;(render :file => 'layouts/application') if controller.request.format.try(:html?) && !controller.request.xhr? && !controller.devise_controller? && !controller.controller_name.eql?('authentications') && !controller.controller_name.eql?('passwords') && !controller.controller_name.eql?('registrations') && !(controller.controller_name.eql?('users')&&controller.action_name.eql?('sign_out')) }
  before_filter :set_last_seen_at, if: proc { |p| user_signed_in? && (session[:last_seen] == nil || session[:last_seen] < 15.minutes.ago) }

  private
  def set_last_seen_at
    current_user.update_attribute(:last_seen, Time.now)
    session[:last_seen] = Time.now
  end

  def store_location
    # store last url as long as it isn't a /users path
    session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
  end

  def after_sign_in_path_for(resource_or_scope)
   #store session to redis
   if current_user
     # an unique MD5 key
     cookies["_validation_token_key"] = Digest::MD5.hexdigest("#{session[:session_id]}:#{current_user.id}")
     # store session data or any authentication data you want here, generate to JSON data
     stored_session = JSON.generate({"user_id"=> current_user.id, "user_email"=>current_user.email, "user_name"=>current_user.name, "user_icon"=>current_user.get_profile_pic})
     $redis.hset(
       "session",
       cookies["_validation_token_key"],
       stored_session
       )
   end
   request.env['omniauth.origin'] || session[:previous_url] || root_path
  end

  def after_sign_out_path_for(resource_or_scope)
   #expire session in redis
   if cookies["_validation_token_key"].present?
     $redis.hdel("session", cookies["_validation_token_key"])
   end
   super
  end

  def parse_errors(object)
    errors = object.errors.messages
    msgs = ""
    errors.each do |k,v|
      v.each {|value| msgs += "#{k.to_s.tr('_', ' ').capitalize} #{value}<br>"}
    end
    msgs
  end 

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def get_complete_url
    "#{request.protocol}#{request.host}:#{request.port}"
  end

  def sanitize_params(params)
   params.each do |key,value|
       # if it’s a hash, we need to check each value inside it…
       if value.is_a?(Hash)
         value.each do |hash_key,hash_value|
           params[key][hash_key] = Sanitize.clean(hash_value)
         end
       elsif value.is_a?(String) || value.is_a?(Integer)
         params[key] = Sanitize.clean(value)
       end
     end
     return params
   end

   Warden::Manager.after_authentication do |user,auth,opts|
    if auth.raw_session[:song_ids].present?
      UploadedSong.find(:all, :conditions => "id in (#{auth.raw_session[:song_ids].join(',')})").each do |s|
        s.user = user
        s.save!
      end
    end
    ActiveRecord::Base.logger.debug "Authenticated user : #{user.name}"

  end


  def share_link(hash)
    'http://'+request.host+':'+request.port.to_s+'/ct/'+hash
  end

  def validate_youtube_url(url)
    begin
      require 'cgi'
      video = (CGI.parse(URI.parse(url).query))['v'][0]
      return video  
    rescue Exception
      return nil
    end
  end

  def filter_backtrace(e)
    filtered_trace = []
    filtered_trace << e.backtrace.shift.strip
    e.backtrace.each do |line|
      line.lstrip!
      filtered_trace << line if !line.include?("/usr")
    end
    filtered_trace[0..5]
  end


  def log(e)
    exception = '' 
    exception << " Class Name: #{e.class.to_s} \n" 
    exception << " Message: #{e.message} \n" 
    exception << " Stacktrace: #{filter_backtrace(e)} \n"
    exception << " Request URL: #{request.original_url} \n"
    user_name = "Unknown User"
    user_name = current_user.email if current_user
    exception << " User: #{user_name} \n"
    HIPCHAT_CLIENT['amuzzin-errors'].send(user_name[0..14], exception, :notify => true , :color => 'red' , :message_format => 'text')
    yield and return if block_given?
    render :file=>'public/500.html'
  end
  
  MOBILE_BROWSERS = ["android", "ipod", "opera mini", "blackberry", "palm",
    "hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", 
    "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link",
    "mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle",
    "mobile","pda","psp","treo"]

    def mobile_user_agent?
  #  layout = selected_layout
  #  return layout if layout
  agent = request.headers["HTTP_USER_AGENT"].downcase
  MOBILE_BROWSERS.each do |m|
    return true if agent.match(m)
  end
  return false
end

def prepare_for_mobile
    #session[:mobile_param] = params[:mobile] if params[:mobile]
    request.format = :mobile if params[:mobile] and params[:mobile] == "true"
  end

end

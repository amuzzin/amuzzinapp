class ActivitiesController < ApplicationController
  def index
    allowed_activities = ["'Comment'", "'Favourite'", "'ProfileSongHistory'", "'UploadedSong'", "'Playlist'"]
    if current_user
      friends = current_user.get_friends
      @activities = []
      restrict_conditions = " trackable_type in (#{allowed_activities.join(',')})"
      unless friends.blank?
        restrict_conditions += " and owner_id in (#{friends.collect{|e| e.id}.join(",")}) and owner_type='User' "      
      else
        restrict_conditions += ' and 1=0 '
      end
	   @activities = PublicActivity::Activity.where(restrict_conditions).order("created_at desc").paginate( :page => params[:page],
	 	 :per_page => 10)
    #Reject activities with id greater than last id
      if params[:last_id]
        @activities.reject! { |a| a.id >= params[:last_id].to_i }
      end
    end
  end

  def load_diff 
  	require 'json'
    if current_user
      if params[:feed_ids].blank?
        @activities = [] 
      else
       friends = current_user.get_friends.collect {|f| f.id}
  	   diff_ids = JSON.parse(params[:feed_ids])
  	   @activities = PublicActivity::Activity.where({:id => diff_ids}).order("created_at desc").paginate( :page => 1,:per_page => diff_ids.size)
       @activities.select! { |a| friends.include?(a.owner_id) }
      end
    end
  end
end

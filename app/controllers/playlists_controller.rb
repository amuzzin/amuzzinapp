class PlaylistsController < ApplicationController  
  
  def index
    if current_user
      @playlists = current_user.playlists.order("updated_at desc").paginate(:page => params[:page], :per_page => 7)
      @playlist = Playlist.new
    end
  end

  def create
    if current_user
      if request.method == "POST"
        @playlist = Playlist.new
        list = Sanitize.clean(params[:playlist][:name])
        @current_song = UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
        unless list.blank?
          @playlist.name = list
          @playlist.user = current_user
          @playlist.save!
          flash.now[:notice] = 'Playlist created'
          render :template => 'playlists/show_update',:locals => {:current_song => @current_song,:playlist => @playlist,:song => params[:song]}
        end
      end
    end
  end  

  def destroy
    if current_user
      @playlist = Playlist.find_by_id(params[:id])
      if @playlist && @playlist.user_id.eql?(current_user.id)
        @playlist.delete
        @record = PlaylistSong.find(:all,:conditions=>{:playlist_id=>params[:id]})
        @record.each{ |r| r.delete }
        flash.now[:notice]='Deleted playlist'
      else
        flash.now[:notice]='The delete action could not be completed'
      end
      @current_song=UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
      render :template=>'playlists/show_update',:locals=>{:current_song=>@current_song,:song => nil}
    end
  end
  
  def public_playlist
    @public_playlist= Playlist.find(:first,:conditions=>{:id => params[:id], :public => 'true'}) 
    if @public_playlist.blank?
      @public_playlist_songs = []
    else
      @public_playlist_songs = @public_playlist.uploaded_songs.paginate(:page => params[:page], :per_page => 10,:order => 'updated_at desc')   
    end
  end

  def unique_song_check
    if current_user
      unique = check_song_in_playlist(params[:s_id],params[:id])
      if unique.blank?
        @current_song = UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
        @song = UploadedSong.find(params[:s_id])
        @playlist.uploaded_songs<< @song
        flash.now[:notice]='Song added to playlist'
        render :template=>'playlists/show_update',:locals=>{:current_song=>@current_song,:song => nil}
      else
        flash.now[:notice]='Song is already in the playlist'
        render :template=>'layouts/show_flash'
      end
    end
  end

  def add_song
    if current_user
      #An user can add songs to only his playlists
      @playlist = Playlist.find(params[:id],:conditions=>{:user_id=>current_user.id})
      unless @playlist.blank?
        #A song should be present in a playlist only once
        unique_song_check
      end
    end
  end
  
  def check_song_in_playlist(song,playlist)
    check = PlaylistSong.find(:all,:conditions=>{:uploaded_song_id=>song,:playlist_id=>playlist})
    return check
  end

  def save_current_queue_as_playlist
    if current_user
      @current_song = UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
      @queue = params[:s_id].split(',')
      @queue.each do |el|
        #A song should be present in a playlist only once
        unique = check_song_in_playlist(el,params[:id])
        if unique.blank?
          song = UploadedSong.find(el)
          @playlist.uploaded_songs << song
        end
      end
    end
  end

  def add_multiple_song
    #An user can add songs to only his playlists
    if current_user
      @playlist = Playlist.find(params[:id], :conditions=>{:user_id=>current_user})
      unless @playlist.blank?
        save_current_queue_as_playlist
      end
      flash.now[:notice]='Current queue has been saved as playlist'
      render :template=>'playlists/show_update',:locals=>{:current_song=>@current_song,:song => nil}
    end
  end  

  def remove_song
    if current_user
      playlist = Playlist.find_by_id(params[:id])
      @current_song = UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
      if playlist && playlist.user_id == current_user.id       
        @songs = PlaylistSong.find(:all,:conditions=>{:playlist_id=>params[:id],:uploaded_song_id => params[:s_id]})
        @songs.each do |song|
          song.delete
        end
        flash.now[:notice]='Song removed from playlist'
      else
        flash.now[:notice]='Song cannot be removed from playlist'
      end
      render :template=>'playlists/show_update',:locals=>{:current_song=>@current_song,:song => nil}
    end
  end
  
  def make_public
    if current_user
      @public = params[:id].split(',')
      @public.each do |p|
        @public_pl = Playlist.find(p)
        if @public_pl.user.id == current_user.id
          unless @public_pl.public
            @public_pl.public = true
            @public_pl.save!
          end
        end
      end
      flash.now[:notice]='Selected playlists have been made public'
      render :template=>'playlists/show_status'
    end
  end
  
  def make_private
    if current_user
      @private = params[:id].split(',')
      @private.each do |p|
        @private_pl = Playlist.find(p)
        if @private_pl.user.id == current_user.id
          if @private_pl.public
            @private_pl.public = false
            @private_pl.save!
          end
        end  
      end
      flash.now[:notice]='Selected playlists have been made private'
      render :template=>'playlists/show_status'
    end
  end

  def save_queue
    if current_user
      list=params[:save_as_playlist_name]
      @current_song=UploadedSong.find_by_id($redis.hget("current_song",current_user.id.to_s))
      @playlist=Playlist.new
      @playlist.name=list
      @playlist.user=current_user
      @playlist.save!
      render :template=>'playlists/show_queue',:locals=>{:playlist=>@playlist.id,:current_song=>@current_song}
    end
  end  
  
  def available_playlists
    if current_user
      sql = 'select id,name from playlists where id in ((select id from playlists where user_id='+current_user.id.to_s+') except (select playlist_id from playlist_songs where uploaded_song_id='+params[:id]+'))'
      @playlists = Playlist.find_by_sql(sql)
      @song = params[:id]
    end
  end
  
  def playlist_songs
    if current_user
      @playlist = Playlist.find_by_id(params[:id])
      if @playlist && (@playlist.user_id == current_user.id || @playlist.is_public? || @playlist.user.is_friend?(current_user.id))
        @playlist_songs = @playlist.uploaded_songs
      end
    end
  end
  
  def play_qr_playlist
    @playlist = Playlist.find(params[:id])
    render :partial => "play_qr_playlist" , :layout => false
  end
end

class UsersController < ApplicationController

  respond_to :html, :xml  

  def index  
    @users = User.all  
    respond_with @users  
  end  
    
  def show  
    @user = User.find(params[:id])  
    render :text => @user.id
  end  
  
  def new  
    @user = User.new  
    respond_with @user  
  end  
    
  def create  
    @user = User.new(params[:user])  
    if @product.save  
      flash[:notice] = "Successfully created User."  
    end  
    respond_with(@user)  
  end  
  
  def edit  
    @produuserct = User.find(params[:id])  
    respond_with(@user)  
  end  
  
  def update  
    @user = User.find(params[:id])  
    if @user.update_attributes(params[:user])  
      flash[:notice] = "Successfully updated User Details."  
    end  
    render :text => "updated"
  end  
    
  def destroy  
    @user = User.find(params[:id])  
    @user.destroy  
    flash[:notice] = "Successfully destroyed User."  
    respond_with(@user)  
  end  

  def sign_out
    reset_session
    redirect_to '/home'
  end
end
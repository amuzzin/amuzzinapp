class CutSongController < ApplicationController
  
  def get_cut_songs_for_ticker
    cut_songs = CutSong.find(:all, :conditions => "id > #{session[:last_share_id].to_i}", 
      :limit => 1, :include => [{:uploaded_song => :song_master}])
    session[:last_share_id] = cut_songs.last.id
    return cut_songs
  end

  def check_new_shares
    cut_songs = get_cut_songs_for_ticker
    cut_songs.each do |c|
      c['art'] = c.uploaded_song.art("small")
      c['name'] = c.uploaded_song.name
      c['popover_text'] = generate_popover_text(c)
    end
    render :json => cut_songs.to_json
  end

  def shared_cuts
    unless current_user.blank?
      if current_user.has_fb_auth?
        @cut_songs = CutSong.paginate(:page => params[:page], :per_page => 10 , 
          :conditions => "id in (select cut_song_id from shared_cuts where uid = '#{current_user.get_fb_id}') ",
          :order => "updated_at desc") 
      end
    end
  end

  def generate_popover_text(cut)
    ret_str = "<div style='float:left;margin-right:10px;'><img src='#{cut.user.get_profile_pic}' height='50' width='50' alt='#{cut.user.email}'/></div>"
    ret_str << "<div id='ticker_user_details' style='float:left;'>#{cut.user.email}<br> Has #{cut.user.cut_song.size} shares</div>"
    return ret_str + "<div id='ticket_song_details' style='clear:both'>#{cut.comments}</div>"  
  end  
  
  def delete_cut_song
    cut_song = CutSong.find(params[:id])
    cut_song.destroy 
    flash.now[:notice]="Deleted cut song"  
    render :template=>'layouts/show_generic_flash'
  end

  def cut_song
    unless current_user.blank?
      @cut_songs = current_user.cut_songs.paginate(:page => params[:page], :per_page => 10,
        :order => "updated_at desc") 
    end
  end
end

require 'soundcloud'
require 'twitter'

class UtilityController < ApplicationController
  include ActionView::Helpers::TextHelper

  def autocomplete
    term = params[:term]
    #search uploaded songs, users and playlists. Add an attribute which represents the type
    @songs,@users,@playlists = search_all(term)
    
    @autocomplete = @songs + @users + @playlists
    @autocomplete.shuffle!
    respond_to do |format|
      format.json { render :json => @autocomplete}
    end
  end
  
  def search_all(term)
    combined_terms = " #{term} | *#{term}* "
    auto_songs = UploadedSong.search  " @title #{combined_terms} ", :page => params[:page], :per_page => 10
    auto_users = User.search combined_terms , :page => params[:page], :per_page => 10
    auto_playlists =  Playlist.search combined_terms , :page => params[:page], :per_page => 10 #TODO Handle :public => true

    songs = auto_songs.map do |s|
      {:type => 'song',:picture => s.art("small"), :name => truncate(s.title, :length=>50), :url => '/s/'+s.id.to_s}      
    end
    users = auto_users.map do |u|
      {:type => 'user',:picture => u.get_profile_pic, :name => truncate(u.name, :length=>50), :url => '/u/'+u.id.to_s}
    end
    playlists = auto_playlists.map do |p|
      {:type => 'playlist',:picture => p.art("small"), :name => truncate(p.name, :length=>50), :url => '/playlists/public_playlist/'+p.id.to_s}
    end
    return songs,users,playlists
  end

  
  def related_songs
    if current_user
      current_queue = current_user.current_queue
    else
      current_queue = UploadedSong.find_all_by_id(URI.decode(cookies['QUEUE']).to_s.split(",")).compact
    end
    @related_songs = []
    if params[:id]
      @related_songs = (UploadedSong.find_all_by_id(Recommender.for(params[:id]).map{|x| x.item_id}).compact - current_queue).first(50)
    end
  end
  
  def search_results
    @query=params[:query]
    count = 10
    star = false
    unless @query.blank?
      @tmp_query = " #{@query} | *#{@query}* "
      @users = User.search_name(@tmp_query,params[:search_users_page],count,star)
      @songs =  UploadedSong.search_title(@tmp_query,params[:search_song_page],count,star)
      @albums =  UploadedSong.search_album(@tmp_query,params[:search_album_page],count,star)
      @lyrics =  UploadedSong.search_lyrics(@tmp_query,params[:search_lyrics_page],count,star)
      @genre =  UploadedSong.search_genre(@tmp_query,params[:search_genre_page],count,star)
      @playlists =  Playlist.search_name(@tmp_query,params[:search_playlists_page],count,star)
    end 
  end
  
  def stream
   song = UploadedSong.find_by_id params[:id]
   send_file "#{Rails.root}/" + song.path, :x_sendfile => true
  end
  
  def current_song
    #user's id is the key for current song
    if params[:id].eql? '0'
      @current_song=nil
      @suggested_tracks = [[],nil]
    else
      @current_song = UploadedSong.find_by_id(params[:id])
      if @current_song
        @song_query = get_song_query(@current_song)
        @current_song.lyrics = @current_song.lyrics.gsub("\r\n","<br>") if @current_song.lyrics.present?
        unless current_user.blank?
          @owner = @current_song.user_id == current_user.id
          np = UserHistory.find_or_create_by_user_id_and_uploaded_song_id(current_user.id, @current_song.id)
          np.count = np.count + 1
          np.updated_at = Time.now
          np.save!
        end
      end
    end
  end
  
  def sound_cloud 
    @current_song = UploadedSong.find_by_id(params[:id])
    if @current_song 
      @suggested_tracks = get_soundcloud_songs(@current_song)
      render :partial => 'sound_cloud' , :locals => {:suggested_tracks => @suggested_tracks}
    else
      render :text => " No Tracks Found !"
    end 
  end

  def clear_song_title(song)
    title = song.title
    if title.blank?
      return '' 
    else
      title = title.split('-').first 
      arr = title.split(/[^a-zA-Z\.\-_]/)
      tit = ''
      arr.each do |i|
        if i =~ /http|\.com/i
          next 
        end 
        tit << i + ' ' 
      end 
      return tit 
    end 
  end 

  def twitter_talk 
    @tweets = [] 
    begin 
      @current_song = UploadedSong.find_by_id(params[:id])
      if @current_song 
        Twitter.configure do |config|
          config.consumer_key        = TWITTER_KEY
          config.consumer_secret     = TWITTER_SECRET
          config.oauth_token        = TWITTER_OAUTH_TOKEN
          config.oauth_token_secret = TWITTER_OAUTH_TOKEN_SECRET
        end
        client = Twitter::Client.new
        q = clear_song_title(@current_song)
        if q.present?
         @tweets = client.search(q, :count => 10).results
        end
        render :partial => 'twitter_talk' , :locals => {:tweets => @tweets}
      else 
        render :text => "No Tracks Found !" 
      end 
    rescue Exception => e  
        render :partial => 'twitter_talk' , :locals => {:tweets => @tweets}
    #    render :text => 'Some exception occured'
    end
  end 
  
  def get_song_query(song) 
    query = ''
    if song 
      query = "#{clear_song_title(song)}" 
      query << " #{clear_song_title(song)}" if song.album and !song.album.include?('Not Available')
    end 
    return query
  end 

  def download_soundcloud(track_id)
  end

  def get_soundcloud_songs(song)
    query = get_song_query(song)
    # create a client object with your app credentials
    client = Soundcloud.new(:client_id => SOUNDCLOUD_CLIENT_ID)

    # find all sounds of buskers licensed under 'creative commons share alike'
    tracks = client.get('/tracks', :q => query , :limit => 5 )
    return tracks,query
  end 

  def set_curr_song_and_fav_elig
    unless current_user.blank?
      # get previous song and reduce its song count
      prev_song=$redis.hget("current_song",current_user.id.to_s)
      if prev_song.present?
        $redis.hincrby("song_count",prev_song,-1)
      end        
      # set current song  and increase song count
      $redis.hincrby("song_count",params[:id],1)        
      $redis.hset("current_song",current_user.id.to_s,params[:id])
    
      @eligible_for_fav=Favourite.
      find(:all,:conditions=>{:uploaded_song_id=>params[:id],:user_id=>current_user})
    end
  end
end

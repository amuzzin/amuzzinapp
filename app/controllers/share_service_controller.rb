class ShareServiceController < ApplicationController
  
  def share_link(url)
    cmd = "curl https://www.googleapis.com/urlshortener/v1/url -H 'Content-Type: application/json'  -d    '{\"longUrl\": \"#{get_complete_url}?/ct/#{url}\"}'"
    json_output = `#{cmd}`
    require 'json'
    return shortened_url = JSON.parse(json_output)["id"].split("http://").last
 end 

  def popup_cutsong
    @song = UploadedSong.find(params[:id])
    @song_path = @song.path.gsub('public','')
		
		if request.env["HTTP_USER_AGENT"]  =~ /Firefox/i
			@song_path = @song.convert_to_ogg.gsub('public','')
		end

    # refer issue #3 in github
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"

		# derive song_path from song master object
    render :layout => false
  end

  def create_cut_song
    cut_song = CutSong.where(
        :start_duration => params[:start_duration],
        :end_duration => params[:end_duration],
        :uploaded_song_id => UploadedSong.find(params[:id]),
        :user_id => current_user.blank? ? '' : current_user.id,
        :comments => params[:comments],
      ).first_or_create
  end

  def valid_fb_share
    current_user.present? and current_user.has_fb_auth? and params[:fb_share].eql? 'true'
  end

  def valid_twt_share
    current_user and current_user.has_tweet_auth? and params[:tw_share].eql? 'true'
  end

  def share_in_facebook(cut_song)
    if valid_fb_share
      uid = current_user.authentications.find(:first,:conditions => "provider='facebook'").uid
      me = FbGraph::User.me(session[:fb_token])
      me.feed!(
        :message => cut_song.comments,
        :picture => "#{get_complete_url}#{cut_song.uploaded_song.art("medium")}" ,
        :link => share_link(cut_song.generate_url),
        :name => cut_song.uploaded_song.title,
        :description => 'Cut. Share. Show your love.'
      )
      cut_song.shared_cuts.create({:provider => "facebook" , :uid => uid} )
    end
  end

  def share_in_twitter(cut_song)
   if valid_twt_share
      access_token = AccessToken.find_by_provider_and_user_id('twitter',current_user.id)
      if access_token
          twitter_user = Twitter::Client.new(
            :consumer_key => TWITTER_KEY,
            :consumer_secret => TWITTER_SECRET,
            :oauth_token => access_token.token,
            :oauth_token_secret => access_token.secret)
          twitter_user.update("#{share_link(cut_song.generate_url)}  #{cut_song.comments[0..80]} via @MusicCutter")
      end
      cut_song.shared_cuts.create({:provider => "twitter" , :uid => current_user.id})
    end
  end

  def draft_song
    create_cut_song
    flash.now[:notice]="Drafted successfully"  
    render :template=>'layouts/show_generic_flash'
  end 
  
  def share_song
    cut_song = create_cut_song
    share_in_facebook cut_song
    share_in_twitter cut_song
    flash.now[:notice]= "Shared successfully"
    render :template=>'layouts/show_generic_flash'
  end
  
  def share_draft_song
    cut_song = CutSong.find_by_id params[:id]
    share_in_facebook cut_song
    share_in_twitter cut_song
    flash.now[:notice]= "Shared successfully"
    render :template=>'layouts/show_generic_flash'
  end
    
  def share_upload_song
    uploaded_song = UploadedSong.find_by_id params[:id]
    if uploaded_song.present?
        shortened_url = uploaded_song.shortened_url
        render :text => URI::encode(shortened_url)
    end
 end
 
 def share_popup
   @song = UploadedSong.find_by_id params[:id]
 end
end

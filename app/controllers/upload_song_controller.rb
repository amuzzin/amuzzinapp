
class UploadSongController < ApplicationController
  def index

  end

  def get_uploaded_songs_from_session
    songs = UploadedSong.where("id in (#{session[:song_ids].join(',')})") if session[:song_ids]
    res = []
    unless songs.blank?
      songs.each do |usong|
        if usong.youtube_mp3 && usong.song_master.blank?
          res << {'name' => usong.name, 'special' => true, 'id' => usong.id , 'url' => usong.youtube_url , 'art' => usong.art('small')}
        else
          res << {'name' => usong.name, 'id' => usong.id , 'art' => usong.art('small')}
        end
      end
    else
      songs=[]
    end
    return songs,res 
  end

  def handle_single_file(path)
    song = UploadedSong.new
    if user_signed_in?
      song.user = current_user
      song.save!
    end
    
    PublicActivity.enabled = false
    usong = song.save_song(path)
    usong.set_shortened_url(get_complete_url)
    add_to_session(song.id)
    PublicActivity.enabled = true

    usong.create_activity key: 'uploaded_song.create', owner: current_user
    cleanup_query = "delete from activities where key='uploaded_song.lyrics' and trackable_type='UploadedSong' and trackable_id='#{usong.id}'" 
    ActiveRecord::Base.connection.execute cleanup_query

    return {'name' => usong.name,
      'id' => usong.id,
      'path' => "/audios/"+usong.file_hash.to_s+".mp3",
      'album_art' => usong.art("medium") 
      }
  end

  def upload_song_from_multiple_files
    json_out = []
    multiple_files = params[:upload]['datafile']
    multiple_files.each do |path|
      json_out << handle_single_file(path)
    end
    return  { files: json_out }
  end

  def upload_song_from_single_file
    json_out = []
    single_file = params[:upload]['datafile']
    json_out << handle_single_file(single_file)    
    return  { files: json_out }
  end

  def upload_song
    if request.method == 'POST'
      json_out = upload_song_from_single_file
      if request.xhr?
        render :json => json_out
      else  
        render :text => "File has been uploaded successfully"
      end
    else
     songs = get_uploaded_songs_from_session
    end

    if songs.present?
      respond_to do |format|
        format.json { render json: {files: songs.second} }
      end  
    end
  end

  def add_to_session(id)
    session[:song_ids].blank? ? session[:song_ids] = [id]  : session[:song_ids] << id 
  end
  
  
  def youtube_upload
    unless params[:youtubeurl].blank?
      process_youtube_upload(params[:youtubeurl])    
    else
      flash.now[:notice]="No video was submitted!!!"  
    end
    render :template=>'layouts/show_generic_flash'  
  end
  
  def process_youtube_upload(param)
    if validate_youtube_url(param).present?  
      create_song_from_youtube(param)
      flash.now[:notice]="The YouTube video is being processed!"  
    else
      flash.now[:notice]="There is some problem with the youtube URL you submitted"  
    end
  end
  
  def add_soundcloud_song 
    if params[:id] 
      @song = UploadedSong.new
      @song.user = current_user
      @song.save!
      @song.delay.extract_mp3_from_soundcloud(params[:id])
      flash.now[:notice]="The Soundcloud audio is being processed!"  
      #add_to_session(@song.id)  
      render :template=>'layouts/show_generic_flash'

    else
      render :text => "Id can't be blank"
    end
  end

  def create_song_from_youtube(param)
    PublicActivity.enabled = false
    @song = UploadedSong.new
    @song.user = current_user
    @song.save!
    @song.delay.extract_mp3_from_youtube(param)
    
    @song.set_shortened_url("#{request.protocol}://#{request.host}:#{request.port}/")
    add_to_session(@song.id)  
    PublicActivity.enabled = true
  end
  
  def check_youtube_progess
    song = UploadedSong.find(params[:id])
    if song.song_master.present?
      render :json => [{'done' => 1}]
    else
      render :json => [{'done' => 0}]
    end
  end
  
  def set_youtube_video(url)
    song = UploadedSong.find(params[:id])
    unless url.blank?
      video = validate_youtube_url(url)
      if video.present?
        song.video_url = "http://www.youtube.com/embed/"+video
        song.video_img = "http://img.youtube.com/vi/"+video+"/0.jpg"
        song.save!
      else
        return "There is some problem with the youtube URL you entered"
      end
    else
      song.video_url = ""
      song.video_img = ""
      song.save!      
    end
    return "The song has been saved"  
  end 

  def edit_song
    if current_user
      @edit_song = UploadedSong.find_by_id(params[:id])
      if @edit_song && @edit_song.user_id.eql?(current_user.id)
        process_edit_song
      else
        @edit_song = nil
      end
    end
  end
  
  def process_edit_song
    if request.method == "GET"
      respond_to do |format|
        format.js
      end
    elsif request.method == "POST"
      save_edit_song_attrs(@edit_song,params[:uploaded_song])
    end
  end
  
  def save_edit_song_attrs(edit_song,params)
    params= sanitize_params(params)
    edit_song.update_attributes(params)
    msg = set_youtube_video(params[:youtube_url])
    flash.now[:notice] = msg
    render :template => '/upload_song/show_edit_flash' ,:locals => {:edit_song => @edit_song}
  end
  
  def song_page
    @song = UploadedSong.find_by_id(params[:id])
    if @song
      @song_count = $redis.hget("song_count",@song.id.to_s)
      @song_count = 0 if @song_count.blank? 
      @queue_count = $redis.hget("queue_count",@song.id.to_s) 
      @queue_count = 0 if @queue_count.blank?
    end
  end

  def play_qr_song
    @uploaded_song = UploadedSong.find(params[:id])
    render :partial => "play_qr_song" , :layout => false
  end
end

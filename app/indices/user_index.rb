ThinkingSphinx::Index.define :user, :with => :active_record, :delta => ThinkingSphinx::Deltas::DelayedDelta do
  indexes name
  indexes email
    
  set_property :enable_star => 1
  set_property :min_infix_len => 3
end

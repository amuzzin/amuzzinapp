 ThinkingSphinx::Index.define :uploaded_song, :with => :active_record, :delta => ThinkingSphinx::Deltas::DelayedDelta  do
   indexes title
   indexes lyrics
   indexes artist
   indexes album
   indexes genre
   indexes year
    
   set_property :enable_star => 1
   set_property :min_infix_len => 3
  end

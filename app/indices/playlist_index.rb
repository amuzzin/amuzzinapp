ThinkingSphinx::Index.define :playlist, :with => :active_record, :delta => ThinkingSphinx::Deltas::DelayedDelta do 
  indexes name
  where "public=true"
    
  set_property :enable_star => 1
  set_property :min_infix_len => 3
end

module UserHelper
	def friend_stream_button(user,button_size,msg)
		html_str = ''
		return html_str if user.blank? or user.id == current_user.id 
		if current_user.blocked? user 
			html_str << "<div style='margin-top:0px'>#{'(Currently hidden from your Friend Stream)<br/>' if msg}"
			#html_str << "<a href='' onclick=/user/unblock_friend?user=#{user.id}'>"
	    	html_str << "<button "
	    	html_str << "onclick='invoke_controller_action(\"\",\"POST\",\"/user/unblock_friend?user=#{user.id}\",\"refresh_current_page()\")' "
	    	html_str << " class='btn #{button_size == 'large' ? 'btn-sm' : 'btn-xs'} btn-success'>"
			html_str << " Unblock Friend </button> "
			#html_str << "</a>"
			html_str << "</div>"

	    elsif current_user.friend_with? user
	    	html_str << "<div style='margin-top:0px'>#{'(Currently showing in your Friend Stream)<br/>' if msg}"
	    	html_str << "<button  "
	    	html_str << "onclick='invoke_controller_action(\"\",\"POST\",\"/user/block_friend?user=#{user.id}\",\"refresh_current_page()\")' "
	    	html_str << "class='btn #{button_size == 'large' ? 'btn-sm' : 'btn-xs'} btn-danger'>"
			html_str << " Block Friend </button> "
			html_str << "</div>"
		elsif current_user.invited? user 
			html_str << "<div style='margin-top:20px'><button  class='btn #{button_size == 'large' ? 'btn-sm' : 'btn-xs'} btn-success btn-disabled'>"
			html_str << "+ Friend Request Pending</button> "
			html_str << "</div>"
		elsif current_user.invited_by? user 
			html_str << "<div style='margin-top:0px'>#{'(This user has already sent you a friends\' stream request.)<br/>' if msg}"
			html_str << "<button  class='btn #{button_size == 'large' ? 'btn-sm' : 'btn-xs'} btn-danger' "
			html_str << "onclick=\"invoke_controller_action('','POST','/user/accept_friend?user=#{user.id}','refresh_current_page()')\""
			html_str << ">"
			html_str << "+ Approve Friend Request</button> "
			html_str << "</div>"
	    else 
			html_str << "<div style='margin-top:20px'><button  "
	    	html_str << "onclick='invoke_controller_action(\"\",\"POST\",\"/user/add_friend?user=#{user.id}\",\"refresh_current_page()\")' "
			html_str << " class='btn #{button_size == 'large' ? 'btn-sm' : 'btn-xs'} btn-success add-friend'>"
			html_str << " + Friend</button> "
			html_str << "</div>"
		end
		render(:inline => html_str)
	end


	def get_profile_song_content(user)
		html_str = "No Profile Song Set <br/><br/>"
		profile_song = user.profile_song 
		if profile_song
			current_song = profile_song.uploaded_song 
			if current_song
				html_str = "<a href=\"/s/#{current_song.id}\" class=\"ajax-link\" data-title=\"Amuzzin-#{truncate(current_song.title, :length=> 20)}\"><span class=\"tool-tip red\" title=\"#{current_song.title}\"> #{truncate(current_song.title, :length=> 40)}</span></a>"
			end 

			html_str << "<br/> <p style=\"font-size: 10px;font-weight: normal\" class=\"time-ago\"><em> ( profile song last updated <abbr class=\"tool-tip bold timeago\" title=\"#{profile_song.updated_at.iso8601}\"><b style=\"color: #909090\">#{profile_song.updated_at.strftime("%B %d at %I:%M %p")} UTC</b></span></em> )</p>"
		end 
		render(:inline => html_str)
	end
end 
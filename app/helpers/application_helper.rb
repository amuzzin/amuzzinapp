module ApplicationHelper
  @@anonymous_pic = "<div style='float:left;margin-right:10px;'><img src='/anonym.png' height='50' width='50'/></div>"
  def fb_dp(uid)
    return "http://graph.facebook.com/#{uid}/picture?type=small"
  end

  def user_profile_pic(uid)
    begin 
      user = User.find(uid)
      return "<div style='float:left;margin-right:10px;'><img src='#{user.get_profile_pic}' height='50' width='50' alt='#{user.email}'/></div>"
    rescue
      return @@anonymous_pic
    end
  end

  def generate_random(prefix=nil)
    rand(36**8).to_s(36)
  end

  def ticker_box_text(cid,uid)
    begin 
      user = User.find(uid)
      ret_str = "<div id='ticker_user_details' style='float:left;'>#{user.email}<br> Has #{user.cut_song.size} shares</div>"
      return ret_str + "<div id='ticket_song_details' style='clear:both'>#{CutSong.find(cid).comments}</div>"
    rescue
      return @@anonymous_pic
    end
  end


  def get_time(time,format=nil)
    if format.nil?
      format = '%d %b %Y %I:%M %p'
    end
    return time.strftime(format)
  end

  def share_link(hash)
    'http://'+request.host+':'+request.port.to_s+'/ct/'+hash
  end

  def share_link_encoded(hash)
    URI::encode(share_link(hash))
  end

  def friends_json 
    friends = current_user.get_friends_json 
    render(:inline => friends)
  end

  def get_tooltip_details(user)
    if current_user.blank?
      return ""
    end
    tooltip = "#{user.name} #{"(Me)" if current_user.eql?(user)}\n\n"
    tooltip << "#{user.uploaded_songs.count} Creation(s)\n"
    tooltip << "#{user.playlists.public.count} Public Playlist(s)\n\n"  
    if current_user.eql?(user)
      tooltip << "click to view your public profile"
    elsif current_user.friend_with? user
      tooltip << "added in your friends' stream"
    else
      tooltip << "click to know more about this user"
    end

    render(:inline => tooltip)
  end 

  def get_tooltip_for_song(song)
    if song.blank?
      return ''
    end
    song_count = $redis.hget("song_count",song.id.to_s);
    song_count = 0 if song_count.blank? 
    queue_count = $redis.hget("queue_count",song.id.to_s); 
    queue_count = 0 if queue_count.blank?
    tooltip = "#{song.title}\n\n"
    tooltip << "#{song_count} Users currently Playing this Song\n"
    tooltip << "#{queue_count} Users currently have this Song in Queue\n"
    tooltip << "#{song.favourites.count} Users Favourited this song\n\n"
    tooltip << "click here to view the public page of this song."
    render(:inline => tooltip)
  end

  def popup_title(title)
    html=''
    title = title.split(' ',2)
    html+="<span style='display:block'>"
    html+="<span class='amuzzin-font'><span class='red'>#{title[0]}</span> #{title[1]}</span>"
    html+="<button style='float:right;' class='popup-close close'>&times</button></span>"
    render(:inline => html)
  end
end

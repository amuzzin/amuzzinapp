module GroupHelper
  
  def parse_log log
  	log_html = ""
  	if log.present?
      p log
  	  log.each do |l|
        l = JSON.parse(l)
        html = ""
  	  	case l['type']
  	  	when 'join'
		      html += '<div class="group-log">'+l['user']['user_name']+' <strong>joined</strong> the group</div>'
		    when 'leave'
		      html += '<div class="group-log">'+l['user']['user_name']+' <strong>left</strong> the group</div>'
		    when 'chat'
          if l['time'].present?
		        html += '<div style="margin-top:5px"><image class="small-image queue-image" src="'+l['user']['user_icon']+'"/>&nbsp;&nbsp;&nbsp;<strong>'+l['user']['user_name']+'</strong>: '+l['message']
            html += '<br><div class="pull-right offset10pxr"><p class="time-ago"><i class="icon icon-time"></i>&nbsp;<abbr class="timeago" title="'+Time.at(l['time'].to_i/1000).iso8601.to_s+'">'
            html += Time.at(l['time'].to_i/1000).strftime("%B %d at %I:%M %p").to_s+' UTC</abbr></p></div></div><br clear="both">'
          else
            html += '<div style="margin-top:5px"><image class="small-image queue-image" src="'+l['user']['user_icon']+'"/>&nbsp;&nbsp;&nbsp;<strong>'+l['user']['user_name']+'</strong>: '+l['message']+'</div>'
          end
        when 'set_curr_song'
          html += '<div class="group-log">'+l['user']['user_name']+' <strong>changed</strong> the song to: <span class="red">'+l['song_name']+'</span></div>';          
        when 'clear_queue'
          html += '<div class="group-log">'+l['user']['user_name']+' <strong>cleared</strong> the group queue</div>';         
        when 'remove_from_queue'
          html += '<div class="group-log">'+l['user']['user_name']+' <strong>removed</strong> the song : <span class="red">'+l['song_name']+'</span> from the group queue</div>';         
        when 'add_to_queue'
          html += '<div class="group-log">'+l['user']['user_name']+' <strong>added</strong> the following songs to group queue : <span class="red">'

          songs = l['song'].split(',').map! {|s| UploadedSong.find_by_id(s)}
          songs.each do |s|
            if s.present?
              html += "<p>#{s.title}</p>"
            end
          end
          html += '</span></div>'
        end
        log_html = html + log_html
      end
    end
    log_html.html_safe
  end

  def group_invite_button user,group
    html = ""
    activity = PublicActivity::Activity.first(:conditions => "trackable_id=#{group.id} AND 
      trackable_type='Group' AND owner_id=#{current_user.id} AND recipient_id = #{user.id}",:order=>"created_at DESC")
    if activity.blank? || (activity && activity.parameters[:used])
      html += "<button class='btn btn-xs btn-danger invite-offline-friend' data-id=#{user.id}>"
      html += "<i class='icon icon-plus'></i> Invite</button>"
    else
      html += "<button class='btn btn-xs btn-success disabled'>"
      html += "<i class='icon icon-plus'></i> Invited</button>"
    end
    html.html_safe
  end

end

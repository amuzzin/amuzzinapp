#!/bin/bash
ls|grep .j > image_files.txt
while read line
do
name=$(echo "$line" | cut -f1 -d ".")
ext=$(echo "$line" | cut -f2 -d ".")
cmd="convert "$name"."$ext" -resize 130x130 "$name"_big."$ext
`$cmd`
cmd="convert "$name"."$ext" -resize 70x70 "$name"_medium."$ext
`$cmd`
cmd="convert "$name"."$ext" -resize 30x30 "$name"_small."$ext
`$cmd`
done < image_files.txt
#!/bin/sh
# http://hints.macworld.com/article.php?story=20031024013757927
# chmod 700 /etc/rsync_daily.sh
# will give this file the appropriate permissions it needs.

# rsync_daily.sh
# daily local rotating backup with remote script using rsync
#
# changes:
# Oct 17 2003 - JF Paradis - creation
#
# the process is:
# 1. rotate local backups 
# rm -rf backup.3
# mv backup.2 backup.3 
# mv backup.1 backup.2
# cp -al backup.0 backup.1
# 2. maintain a local copy using rsync
# rsync -a --delete source_directory/ backup.0/ 
# 3. maintain a remote copy using rsync
# rsync -a --delete source_directory/ remote_user@remote_host::target_share/ 

FOLDER=encore;

LOCAL_SOURCE=/home;
LOCAL_TARGET=/home1;

REMOTE_HOST=10.0.1.202;
REMOTE_SHARE=encore;
REMOTE_USER=root;

# make sure we're running as root
# id options are effective (u)ser ID
if (( `id -u` != 0 )); then 
{ echo "Sorry, must be root. Exiting..."; exit; } 
fi;

# Rotating backups:

# step 1: delete the oldest backup, if it exists
# rm options are (r)ecursive and (f)orce
if [ -d $LOCAL_TARGET/$FOLDER.3 ] ; then 
rm -rf $LOCAL_TARGET/$FOLDER.3 ; 
fi; 

# step 2: shift (rename) the middle backup(s) back by one, if they exist
if [ -d $LOCAL_TARGET/$FOLDER.2 ] ; then 
mv $LOCAL_TARGET/$FOLDER.2 $LOCAL_TARGET/$FOLDER.3 ; 
fi;

if [ -d $LOCAL_TARGET/$FOLDER.1 ] ; then 
mv $LOCAL_TARGET/$FOLDER.1 $LOCAL_TARGET/$FOLDER.2 ; 
fi; 

# step 3: make a hard-link-only copy of the latest backup, if it exists
# cpio options are single (p)ass, create dir and (l)ink files
if [ -d $LOCAL_TARGET/$FOLDER.0 ] ; then 
# the next 2 lines are for AIX
cd $LOCAL_TARGET/$FOLDER.0 && find . -print | 
cpio -pdl $LOCAL_TARGET/$FOLDER.1 ; 
# the next line is for GNU cp
# cp -adl $LOCAL_TARGET/$FOLDER.0 $LOCAL_TARGET/$FOLDER.1 
fi; 

# step 4: create backup by updating previous
# rsync options are (a)rchive and (delete) extra
rsync 
-a --delete 
$LOCAL_SOURCE/$FOLDER/ 
$LOCAL_TARGET/$FOLDER.0/ ; 

# step 5: update backup.0 to reflect the backup date and time
touch $LOCAL_TARGET/$FOLDER.0 ; 

# Remote backup
# rsync options are (a)rchive, (z) compress and (delete) extra 
rsync 
-e ssh 
-az --delete 
$LOCAL_TARGET/$FOLDER.0/ 
$REMOTE_USER@$REMOTE_HOST::$SHARE ;
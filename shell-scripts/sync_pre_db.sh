DATABASE=am_dev
PGUSER=postgres
PGPASSWORD=postgres

bufilename=`cd /home/amuzzin/backup/data/ && ls -1 | tail -n 1`

echo "Dropping database $DATABASE"
dropdb -U $PGUSER $DATABASE
echo "Creating database $DATABASE"
createdb -U $PGUSER $DATABASE
echo "Restoring database $DATABASE from $bufilename"
pg_restore --verbose --username=$PGUSER --dbname=$DATABASE --format=t $bufilename
echo "Done!"
root@vps:/# cat /etc/init.d/sync_pre_db.sh
DATABASE=am_dev
PGUSER=postgres
PGPASSWORD=postgres

bufilename=`cd /home/amuzzin/backup/data/ && ls -1 | tail -n 1`

echo "Dropping database $DATABASE"
dropdb -U $PGUSER $DATABASE
echo "Creating database $DATABASE"
createdb -U $PGUSER $DATABASE
echo "Restoring database $DATABASE from $bufilename"
pg_restore --verbose --username=$PGUSER --dbname=$DATABASE --format=t $bufilename
echo "Done!"
DATABASE=amz_prod
PGUSER=postgres
PGPASSWORD=postgres
BACKUPDIR=/home/amuzzin/Dropbox/backup/data
BACKUPTIME="7"

# backup
echo "Backup database $DATABASE."
bufilename=$BACKUPDIR/backup_`date '+%Y%m%d'`.gz
pg_dump --file=$bufilename --format=t --username=$PGUSER $DATABASE
echo "Done."

#delete
find_files=`find $BACKUPDIR -maxdepth 1 -mtime +$BACKUPTIME`
echo "Looking for backups older then $BACKUPTIME days."
echo "----"
if [ -z "$find_files" ]; then
echo "None Found!"
else
echo "$find_files"
fi
echo "----"
find $BACKUPDIR -mtime +$BACKUPTIME -delete
echo "Done."
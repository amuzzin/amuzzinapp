module ExtractionLayer
  module Layer1Extraction

    #================================#
    #Layer 1 extraction on metadata
    #reads directly from mp3 ID3 Tags 
    #================================#

    def clear_junk(attr)
      tag = ID3Lib::Tag.new(self.path)
      str = tag.frame_text(attr)
      unless str.blank?
        str = str.encode('utf-8', 'binary', :invalid => :replace, :undef => :replace)
        return str
      end
    end

    def get_art_mime_type
      tag = ID3Lib::Tag.new(self.path)
      if tag.frame(:APIC).present?
        type = tag.frame(:APIC)[:mimetype].split('/') 
      end
      if type.is_a? Array
       image_type = type[1]
       return image_type
      end
      return nil
    end

    def get_path_without_extension
      self.path.split('.')[0]
    end

    def write_album_art(art)
      File.open(art,'wb') do |f|
        tag = ID3Lib::Tag.new(self.path)
        f.write tag.frame(:APIC)[:data] 
      end
      self.update_attribute("album_art",art)
    end

    def thumbnail_processing
      #post processing 
      image_type = get_art_mime_type
      if image_type.present?
        art = "#{get_path_without_extension}.#{image_type}"
        write_album_art art
        generate_sizes(get_path_without_extension,image_type)
      end
    end
    
    def generate_sizes(path,type)
      system("convert #{path}.#{type} -resize 250x250 #{path}_share.#{type}")
      system("convert #{path}.#{type} -resize 130x130 #{path}_big.#{type}")
      system("convert #{path}.#{type} -resize 70x70 #{path}_medium.#{type}")
      system("convert #{path}.#{type} -resize 30x30 #{path}_small.#{type}")
    end

    def read_metadata
      # extract all meta information from ID3lib
      self.title = clear_junk(:TIT2)
      self.artist = clear_junk(:TPE1)
      self.album = clear_junk(:TALB)
      self.track = clear_junk(:TRCK)
      self.year = clear_junk(:TYER)
      self.comment1 = clear_junk(:COMM)
      self.genre = clear_junk(:TCON)
      self.save!
      thumbnail_processing
    end

    def clear_metadata
      tmp_path = self.file_hash
      system("cp #{self.path} /tmp/#{tmp_path}_temp.mp3")
      #system("ffmpeg -i /tmp/#{path} -map_metadata -1 /tmp/#{path}_temp.mp3")
      system("ffmpeg -i  /tmp/#{tmp_path}_temp.mp3 /tmp/#{tmp_path}_temp.wav")
      system("ffmpeg -i  /tmp/#{tmp_path}_temp.wav -f mp3 -aq 9 /tmp/#{tmp_path}_temp2.mp3")
      system("mv #{self.path} #{self.path}_original")
      system("cp /tmp/#{tmp_path}_temp2.mp3 #{self.path}")
      system("rm /tmp/#{tmp_path}_temp*")
    end

    def art(size)
      # For carrierwave & imagemagick
      if self.image_url
        if size.eql?("original")
          return "#{IMG_CDN}/#{self.image_url}"
        end
        return "#{IMG_CDN}/#{self.image_url(size.to_sym)}"
      end

      # if source is soundcloud 
      if self.soundcloud_mp3 
        return self.soundcloud_art 
      end 

      # If source is Youtube
      if self.youtube_mp3 and self.album_art and self.album_art.include? "default"
        return self.video_img 
      end

      # If source is Native 
      if self.album_art.present?
        if self.album_art.include? "default" or !(self.album_art.include? "public")
          return "#{IMG_CDN}/default.jpg"
        else
          if size == "original"
           return "#{IMG_CDN}#{self.album_art.gsub('public','')}"
          end
          return "#{IMG_CDN}#{self.album_art.gsub('.','_'+size+'.').gsub('public','')}"
        end
      else
        return "#{IMG_CDN}/default.jpg"
      end  
    end

  end


  module Layer2Extraction
    #======================================#
    # Hash the song with its content - SHA1
    # Save it in public/audios file 
    #======================================#
    def update_song(path)
      directory = "public/audios"

      # get size of a file
      self.size = File.stat(path).size

      # generate hash for a given file
      file_hash = Digest::SHA1.hexdigest(path).to_s

      # Write the temp file to a new hash file and delete the temp file
      hash = File.join(directory, file_hash )
      File.open(hash, "wb") { |f| f.write(File.open(path, "r").read) }
      File.open("#{hash}_original", "wb") { |f| f.write(File.open(path, "r").read) }

      # delete the file after saving it in a hash file
      File.delete(path)

      #path has file name appended with hash
      self.path = hash
      # have file_hash seperately for future purpose
      self.file_hash= file_hash

      self.album_art = '/default.jpg' 
      self.save
      # delay metadata extraction to later stage using delayed_job
      #self.delay.read_metadata
      self.read_metadata
      self.stamp_title_album
      self.set_fingerprint
      #clear all id3tags as its causing chrome to throw error in some cases
      self.delay.clear_metadata
    end

    #========================================#
    # calculate the FINGERPRINT using fpcalc 
    # fpcalc pasted in /usr/bin for conveneince
    # save the finger print in song table itself
    #=========================================#
    def set_fingerprint
      result = `fpcalc "#{Rails.root}/#{self.path}"`
      self.duration = result.split('DURATION=')[1].split('FINGERPRINT=')[0].chomp!
      self.finger_print = result.split('FINGERPRINT=')[1].chomp!
      self.save
    end

    #======================================#
    # Layer 2 extraction on metadata
    # Lookup the acoustid for exact metadata
    # TODO:
    # Parse the resultant string and update the ActiveRecord
    #=======================================#
    def get_ws_info
      uri = URI("http://api.acoustid.org/v2/lookup?client=8XaBELgH&meta=recordings+releasegroups+compress&duration=#{self.duration}&fingerprint=#{self.finger_print}")
      result = Net::HTTP.get(uri) # => String
      puts result
    end
  end

  module DuplicateCopyRight
    def check_for_copyright_violation(master_song,path)
      #Check if master song is copyrighted. If yes delete the uploaded song, display message and mark user
      if master_song.is_copyrighted.blank?
        self.song_master_id = master_song.id
        self.save!
        self.update_song(path)
      else
        File.delete(path)
        self.destroy
        #TODO: display message and mark the user as copyright violator
      end
    end

    def create_or_load_master_song(file_hash,path)
      master_song = SongMaster.find_by_file_hash(file_hash)
      if master_song.blank?
        # create a new master song entry
        master_song = SongMaster.new
        master_song.file_hash = file_hash
        master_song.save!
        self.song_master_id = master_song.id
        self.save!
        self.update_song(path)
      else
        check_for_copyright_violation(master_song,path)
      end
      return master_song
    end
  end

  def self.list_of_genres
      genres = "Not Available
      Acid Jazz
      Alt-Country
      Ambient
      Ancient Greek Music
      Andean
      Art Song
      Baroque
      Bhangra
      Black Metal
      Bluegrass
      Blues
      Boogie Woogie
      Bossa Nova
      Breakbeat
      Cajun and Zydeco
      Calypso
      Candombe
      Caribbean
      Celtic
      Children's
      Chinese Opera
      Christian
      Christian Country
      Christian Punk
      Christian Rap and Hip Hop
      Christian Rock and Pop
      Christian Ska
      Christian Worship
      Classical
      Classical Modern
      Classical Romantic
      Country
      Dancehall Reggae
      Death Metal
      Disco
      Dixieland
      Doo Wop
      Drum and Bass
      Dub
      Early Music
      Electro
      Electronic
      Enka
      Eurodance
      Filk
      Flamenco
      Folk and Traditional
      Free Jazz
      Freestyle
      Funk
      Garage
      Go-Go
      Gospel
      Gothic
      Gregorian Chant
      Gypsy
      Hardcore
      House
      IDM
      Industrial
      Industrial Noise
      Jazz
      Jewish
      Junkanoo
      Klezmer
      Kompa
      Latin
      Latin Freestyle
      Latin Jazz
      Latin Pop
      Lounge
      March
      Mariachi
      Medieval
      Merengue
      Metal
      Microtonal
      Minimalism
      Musicals
      Musique Concr?te
      New Age
      Novelty
      Opera
      Operetta
      Pagan
      Polka
      Progressive Rock
      Punk and Hardcore
      R&B and Soul
      Ragtime
      Rap and Hip-Hop
      Reggae
      Religious and Devotional
      Renaissance
      Rock and Pop
      Rockabilly
      Salsa
      Samba
      Ska
      Skiffle
      Soca
      Southern Gospel
      Spirituals
      Surf Rock
      Synthpop
      Tango
      Techno
      Tejano
      Trance
      Vallenato
      Western
      World Fusion".split("\n")
      return genres.collect{|e| [e,e]}
    end

    def self.list_of_languages
      languages = "Not Available
                  Afrikaans
                  Albanian
                  Arabic
                  Armenian
                  Azerbaijani
                  Basque
                  Belarusian
                  Bengali
                  Bosnian
                  Bulgarian
                  Catalan
                  Cebuano
                  Chinese
                  Croatian
                  Czech
                  Danish
                  Dutch
                  English
                  Esperanto
                  Estonian
                  Filipino
                  Finnish
                  French
                  Galician
                  Georgian
                  German
                  Greek
                  Gujarati
                  Haitian Creole
                  Hausa
                  Hebrew
                  Hindi
                  Hmong
                  Hungarian
                  Icelandic
                  Igbo
                  Indonesian
                  Irish
                  Italian
                  Japanese
                  Javanese
                  Kannada
                  Khmer
                  Korean
                  Lao
                  Latin
                  Latvian
                  Lithuanian
                  Macedonian
                  Malay
                  Maltese
                  Maori
                  Marathi
                  Mongolian
                  Nepali
                  Norwegian
                  Persian
                  Polish
                  Portuguese
                  Punjabi
                  Romanian
                  Russian
                  Serbian
                  Slovak
                  Slovenian
                  Somali
                  Spanish
                  Swahili
                  Swedish
                  Tamil
                  Telugu
                  Thai
                  Turkish
                  Ukrainian
                  Urdu
                  Vietnamese
                  Welsh
                  Yiddish
                  Yoruba
                  Zulu".split("\n")
        return languages.collect{|e| [e,e]}
    end
end

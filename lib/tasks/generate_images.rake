namespace :image do
  desc 'Resizes the images to small,medium,share image categories'
  task :generate , [:host] => [:environment] do |t,args|
    UploadedSong.find_each(:conditions => "youtube_mp3 is null and album_art not like '%default%'") do |u|
      type = u.album_art.split('.').last
      path = "#{Rails.root}/#{u.album_art}".gsub('.'+type,'')
      u.generate_sizes(path,type)                  
      p "Generating image sizes for #{path}.#{type}"
    end
  end
end

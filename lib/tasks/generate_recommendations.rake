namespace :mp3 do
  desc "Generates recommendations from the current queues of users assuming songs in current queue are similar"
  task :generate_recommendations, [:host] => [:environment] do |t,args|
    User.all.each do |u|
      begin
        current_queue = $redis.hget("current_queue",u.id.to_s)
      rescue
        current_queue = nil
      end
      unless current_queue.blank?
        current_queue = current_queue.split(',')
        Recommender.uploaded_songs.add_set(u.id.to_s,current_queue) unless current_queue.empty?
      end              
    end
    Recommender.process!
  end
end
namespace :migration do
  desc "Migrates from memcached to redis"
  task :migrate_from_memcached_to_redis, [:host] => [:environment] do |t,args|
    User.all.each do |u|
      #migrate friends
      friends = $redis.get("friends_#{u.id}")
      if friends
        $redis.hset("friends",u.id,friends)
     #   $redis.del("friends_#{u.id}")
      end
      
      #migrate current queue
      begin
        queue = CACHE.get(u.email)
      rescue
        queue = nil
      end
      if queue
        $redis.hset("current_queue",u.id,queue)
      end
      
      #migrate current song
      begin
        song = CACHE.get(u.id)
      rescue
        song = nil
      end
      if song
        $redis.hset("current_song",u.id,song)
      end              
    end
    puts "migrated friends"
    puts "migrated current queue"
    puts "migrated current song"
    #migrate session
    $redis.rename("mySessionStore","session");
    puts "migrated session"
    #migrate heap
    begin
      heap = CACHE.get("heap")
    rescue Exception => e
      heap=nil
    end
    if heap
      $redis.set("heap",(JSON.parse heap)['nodes'].to_json)
    end
    puts "migrated heap"
    UploadedSong.all.each do |u|
      #migrate song count
      begin
        song_count = CACHE.get("#{u.id}song_count")
      rescue
        song_count = nil
      end
      if song_count
        $redis.hset("song_count",u.id,song_count)
      end
      
      #migrate queue count
      begin
        queue_count = CACHE.get("#{u.id}queue_count")
      rescue
        queue_count = nil
      end
      if queue_count
        $redis.hset("queue_count",u.id,queue_count)
      end
      
    end
    puts "migrated song count"
    puts "migrated queue count"
  end
end
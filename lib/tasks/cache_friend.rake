namespace :cache do
	desc 'Caches all the friendship in redis-server'
	task :friends , [:host] => [:environment] do |t,args|
		User.find_each do |u|
			p "Publishing for user #{u.name}"
			u.publish 
		end
	end
end

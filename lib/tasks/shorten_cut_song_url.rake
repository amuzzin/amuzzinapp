namespace :ct do
	desc 'Shorten all Cut Song urls. Usage: rake ct:shorten_all["amuzz.in"]'
	task :shorten_all , [:host] => [:environment] do |t,args|
		CutSong.find(:all,:conditions => "shortened_url is null").each do |ct|
			ct.set_shortened_url(args[:host])
		end
		UploadedSong.find(:all,:conditions => "shortened_url is null").each do |ct|
			ct.set_shortened_url(args[:host])
		end
	end
end

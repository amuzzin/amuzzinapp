namespace :mp3 do
	desc 'renames all the mp3 files and sets path accordingly'
	task :reset , [:host] => [:environment] do |t,args|
		UploadedSong.find_each(:conditions => " path like '%.mp3'") do |u|
                        new_path = u.path.gsub(/\.mp3$/,'')
                        `mv #{Rails.root}/#{u.path} #{Rails.root}/#{new_path}`
			p "Reseting path #{u.path} to #{new_path} for #{u.title}"
			u.path = new_path 
                        u.save!
		end
	end
end

 namespace :node do
	desc 'Restart node.js server with workers'
	task :restart , [:host] => [:environment] do |t,args|
		puts "Restarting node.js server"
		system("cd #{Rails.root}/node.js && ./node_modules/streamline/bin/_node -c server._js");
		system("cd #{Rails.root}/node.js && ./node_modules/streamline/bin/_node -c worker._js");
		system("cd #{Rails.root}/node.js && ./node_modules/forever/bin/forever stop server.js");
		system("cd #{Rails.root}/node.js && ./node_modules/forever/bin/forever stop worker.js");
		system("cd #{Rails.root}/node.js && ./node_modules/forever/bin/forever start server.js development");
		system("cd #{Rails.root}/node.js && ./node_modules/forever/bin/forever start worker.js development");
		puts "Done restart of node.js"
	end
end


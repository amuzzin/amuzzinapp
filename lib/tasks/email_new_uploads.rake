namespace :email do
  desc 'Send new uploads email to inactive users'
  task :new_uploads , [:host] => [:environment] do |t,args|
    User.find_each(:conditions => "last_seen < '#{Time.now - 2.week}'") do |u|
      puts "Emailing user #{u.name}"
      UserMailer.new_uploads(u)
    end
  end
end

namespace :mp3 do
  desc "Removes metadata from all songs"
  task :clear_metadata, [:host] => [:environment] do |t,args|
    UploadedSong.find_each(:conditions => "path is not null") do |u|
      path = "#{Rails.root}/#{u.path}".gsub('.mp3','')
      p "Removing metadata for #{u.title}"
      u.clear_metadata(path)                  
    end
  end
end
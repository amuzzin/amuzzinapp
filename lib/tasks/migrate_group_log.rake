namespace :migration do
  desc "Migrates group log from hash to list for pagination"
  task :migrate_group_log, [:host] => [:environment] do |t,args|
    Group.all.each do |g|
      p "Migrating group '#{g.name}' with id #{g.id}"
      log = JSON.parse(g.log)
      log.each do |l|
        if l['type'] =='chat'
          $redis.lpush "group:#{g.id}:log.chat",l.to_json
        else
          $redis.lpush "group:#{g.id}:log.other",l.to_json
        end
      end 
      p "Done"
    end  
  end
end
module ProcessSong
	
	module UploadedSongSearcher


		def self.search_title(title,page,count)
			UploadedSong.search :conditions => {:title => title}, :star => true , :page => page, :per_page => count
		end

		def self.search_album(album,page,count)
			UploadedSong.search :conditions => {:album => title}, :star => true , :page => page, :per_page => count
		end

		def self.search_lyrics(lyrics,page,count)
			UploadedSong.search :conditions => {:lyrics => lyrics}, :star => true , :page => page, :per_page => count
		end

		def self.search_genre(genre,page,count)
			UploadedSong.search :conditions => {:genre => genre}, :star => true , :page => page, :per_page => count
		end

	end

	module ProcessYoutube

		def extract_meta_data_from_youtube(file)
			file = file.gsub(' ','').gsub('\n','').gsub(/[^\w]/, '')
			json_str = File.read("/tmp/#{file}.info.json")
			@parsed = JSON.parse(json_str)
			self.title = @parsed["title"]
			self.duration = @parsed["duration"]
			self.video_img = @parsed["thumbnail"]
		    if self.album_art == '/default.jpg' 
               self.album_art = self.video_img 
            end
			self.video_url = "http://www.youtube.com/embed/"+@parsed["id"]
			self.album = "Not Available"
			self.save!
		end

		def extract_mp3_from_youtube(url)
			outfile = eval("%x[youtube-dl -e #{url}]").gsub('.flv','').gsub('.mp4','').gsub('.avi','')
			self.name = outfile
			self.youtube_mp3 = true
			self.youtube_url = url
			self.save!
			self.download_youtube_mp3(url,outfile)
		end


		def spawn_process_for_download(url,outfile)
			a = IO.popen("cd /tmp && youtube-dl -o \"#{outfile}\" --write-description --write-info-json #{url}")
			a.readlines()
			system("cd /tmp && ffmpeg -i #{outfile} -vn -ar 44100 -ac 2 -ab 192k -f mp3 -y #{outfile}")
			path = "public/audios/#{outfile}"
			File.open(path, "wb") { |f| f.write(File.open("/tmp/#{outfile}","r").read) }
			return path
		end

		def download_youtube_mp3(url,outfile)
			outfile = outfile.gsub(' ','').gsub('\n','').gsub(/[^\w]/, '')
			path = spawn_process_for_download(url,outfile)
			# generate hash for a given file
			file_hash = Digest::SHA1.hexdigest(path).to_s
			self.path = path
			self.file_hash = file_hash
			# check whether the entry already exists in DB
			self.song_master = create_or_load_master_song(file_hash,path)
			self.save!
			self.extract_meta_data_from_youtube(outfile)
			self.create_activity key: 'uploaded_song.import', owner: self.user
			cleanup_query = "delete from activities where key='uploaded_song.lyrics' and trackable_type='UploadedSong' and trackable_id='#{self.id}'" 
			ActiveRecord::Base.connection.execute cleanup_query
		end
	end

    module ProcessSoundcloud 
    	require 'soundcloud'

		def extract_mp3_from_soundcloud(track_id)
		    client = Soundcloud.new(:client_id => SOUNDCLOUD_CLIENT_ID)
		    track = client.get('/tracks/'+track_id)
		    self.title = track.title.to_s
		    self.duration = track.duration 
		   	self.album_art = track.artwork_url.to_s
		   	self.album = track.track_type.to_s
		   	self.year = track.release_year
		   	self.lyrics = track.description.to_s
		   	self.genre = track.genre.to_s
		   	self.artist = track.user.username.to_s
		   	self.video_url = track.video_url.to_s 

		   	self.soundcloud_mp3 = true 
		   	self.soundcloud_url = track.permalink_url
		   	self.soundcloud_art = track.artwork_url.to_s 
		   	self.soundcloud_user_url = track.user.permalink_url 
		   	self.soundcloud_artist = track.user.username  
  
		    data = client.get(track.download_url)
		    File.open("/tmp/soundcloud#{track_id}", 'wb') { |f| f.write(data) }

		   	path = "public/audios/soundcloud#{track_id}"
		    `mv /tmp/soundcloud#{track_id} #{path}`

		    file_hash = Digest::SHA1.hexdigest(path).to_s
			self.path = path
			self.file_hash = file_hash
			# check whether the entry already exists in DB
			self.song_master = create_or_load_master_song(file_hash,path)
			self.save!
			self.create_activity key: 'uploaded_song.import_soundcloud', owner: self.user
			cleanup_query = "delete from activities where key='uploaded_song.lyrics' and trackable_type='UploadedSong' and trackable_id='#{self.id}'" 
			ActiveRecord::Base.connection.execute cleanup_query
		end

    end 

	module ConvertMetrics

		#====================================#
		# create a child from it original song
		# call actual cut method - mp3splt  
		#====================================#
		def cut(start_duration,end_duration)
			new_cut = self.cut_songs.new
			new_cut.uploaded_song_id = self.id
			# making cut as delayed_job
			new_cut.mp3splt(start_duration,end_duration)
			return new_cut
		end

		def convert_to_ogg(mp3_path=self.path)
			ogg_path = "#{mp3_path}.ogg"
			out = `ffmpeg -i #{mp3_path} -acodec libvorbis #{ogg_path}`
			return ogg_path
		end

		def humanize(duration=self.duration)
			if duration.blank?
			  return "00:00"
			else
			  min = duration / 60
			  min = '0'+min.to_s if min < 10 
			  sec = duration % 60
			  sec = '0'+sec.to_s if sec < 10
			  return "#{min}:#{sec}"
			end
		end

		def set_shortened_url(url)
return
			url << "/up/#{self.file_hash}/"
			cmd = "curl https://www.googleapis.com/urlshortener/v1/url -H 'Content-Type: application/json'  -d    '{\"longUrl\": \"#{url}\"}'"
			json_output = `#{cmd}`
			require 'json'
			shortened_url = JSON.parse(json_output)["id"].split("http://").last
			self.update_attribute("shortened_url",shortened_url)
		end 
	end
end

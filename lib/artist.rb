module Artist
  module Insight
    def received_favourites_count
      PublicActivity::Activity.count(:conditions => "recipient_id=#{self.id} and trackable_type='Favourite' AND owner_id != #{self.id}")
    end

    def fans_count 
      query = "SELECT COUNT(distinct(owner_id)) FROM activities WHERE (recipient_id=#{self.id} AND trackable_type='Favourite' AND owner_id != #{self.id})"
      result = ActiveRecord::Base.connection.select_value(query)
    end

    def listening_songs_count 
      listened_songs = $redis.hgetall("current_song")
      return 0 if listened_songs.blank?
      p listened_songs
      listened_songs.delete(self.id.to_s)
      user_songs = self.uploaded_songs.collect {|u| u.id.to_s}
      listened_songs.values.select {|l| user_songs.include?(l)}.length
    end

    def queued_songs_count 
      queued_songs = $redis.hgetall("current_queue")
      return 0 if queued_songs.blank?
      user_songs = self.uploaded_songs.collect {|u| u.id.to_s}
      queue_count = 0
      queued_songs.each do |user_id,value|
        next unless user_id != self.id.to_s
        songs = value.split(',')
        queue_count += 1 if (songs - user_songs).length < songs.length
      end 
      queue_count
    end

    def artist_insight_data

      result = [['Month','Favourites','Comments']]
      start_date = (Date.today-1.year)
      end_date = Date.today

      date_keys = []
      tmp_date = start_date
      
      13.times do 
        date_keys << Date::MONTHNAMES[tmp_date.month][0..2]+" "+tmp_date.year.to_s
        tmp_date = tmp_date.next_month
      end

      query = "SELECT  DATE_PART('YEAR', created_at),
      DATE_PART('MONTH', created_at),
      sum(case when trackable_type='Favourite' then 1 else 0 end),
      sum(case when trackable_type='Comment' then 1 else 0 end) 
      FROM    activities
      WHERE   created_at >= '#{start_date}'
      AND     created_at <= '#{end_date}'
      AND recipient_id=#{self.id} AND owner_id != #{self.id}
      GROUP BY DATE_PART('YEAR', created_at), DATE_PART('MONTH', created_at);"
      tmp_result = ActiveRecord::Base.connection.select_rows(query)

      tmp_result.each do |tmp|
        key = Date::MONTHNAMES[tmp[1].to_i][0..2]+" "+tmp[0]
        result << [key,tmp[2].to_i,tmp[3].to_i]
      end

      if result.size == 1
        date_keys.each do |d|
          result << [d,0,0]
        end
      end

      return result
    end

    def favourite_insight
      result = [["Song","Favourites"]]
      query = "SELECT up.title,
        count(fav.id)
        FROM favourites fav inner join uploaded_songs up 
        ON up.id=fav.uploaded_song_id 
        AND up.user_id = #{self.id} AND fav.user_id != #{self.id}
        GROUP BY up.id" 
      tmp_result = ActiveRecord::Base.connection.select_rows(query)
      tmp_result.each do |tmp|
        result << [tmp[0],tmp[1].to_i]
      end
      return result
    end

    def listend_insight
      result = [["Song","Online Listeners"]]
      listened_songs = $redis.hgetall("current_song")
      song_count = Hash.new(0)
      song_ids = UploadedSong.select(:id).where("user_id=#{self.id}").collect{|u| u.id}
      listened_songs.each do |user_id,song_id|
        next unless song_ids.include?(song_id.to_i) && user_id != self.id.to_s
        song_count[song_id] += 1
      end

      song_count.each do |song_id,listeners|
        result << [UploadedSong.find(song_id).title,listeners.to_i]
      end

      return result
    end

    def queued_insight
      result = [["Song","Queued Listeners"]]
      listened_songs = $redis.hgetall("current_queue")
      song_count = Hash.new(0)
      song_ids = UploadedSong.select(:id).where("user_id=#{self.id}").collect{|u| u.id}
      listened_songs.each do |user_id,tsong_ids|
        next unless user_id != self.id.to_s
        tsong_ids.split(",").each do |song_id|
          next unless song_ids.include?(song_id.to_i)
          song_count[song_id] += 1
        end
      end

      song_count.each do |song_id,listeners|
        result << [UploadedSong.find(song_id).title,listeners.to_i]
      end

      return result
    end
  end
end
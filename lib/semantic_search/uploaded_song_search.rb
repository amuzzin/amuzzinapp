module SemanticSearch
  module UploadedSongSearch

    def UploadedSong.new_song
      {
        :conditions => "uploaded_songs.year > #{Time.now.year - 5}",
        :order => "uploaded_songs.year DESC"
      }
    end

    def UploadedSong.old_song
      {
        :conditions => "uploaded_songs.year between 1900 and #{Time.now.year - 5}",
        :order => "uploaded_songs.year ASC"
      }
    end

    def UploadedSong.newly_uploaded
      {
        :order => "uploaded_songs.created_at DESC"
      }
    end

    def UploadedSong.unheard user
      if user == 'all'
        return {:conditions => "uploaded_songs.id NOT IN (select uploaded_song_id from user_histories)", :order => "created_at DESC"}
      else
        return {:conditions => "uploaded_songs.id NOT IN (select uploaded_song_id from user_histories where user_id = #{user.id})", :order => "created_at DESC"}
      end
    end

    def UploadedSong.most_heard user
      if user == 'all'
        return {:include => "user_histories",:order => "user_histories.count DESC"}
      else
        return {:include => "user_histories",:conditions => "user_histories.user_id = #{user.id}",:order => "user_histories.count DESC"}
      end
    end

    def UploadedSong.least_heard user
      if user == 'all'
        return {:include => "user_histories",:order => "user_histories.count ASC"}
      else
        return {:include => "user_histories",:conditions => "user_histories.user_id = #{user.id}",:order => "user_histories.count ASC"}
      end
    end

    def UploadedSong.friend value,user
      if value == "all"
        return {:conditions => "uploaded_songs.user_id IN (#{user.get_friend_ids.join(',')})"}
      else
        return {:conditions => ["uploaded_songs.user_id = ?",value]}
      end
    end

    def UploadedSong.song_language value
      {
        :conditions => ["uploaded_songs.language = ?",value]
      }
    end

    def UploadedSong.song_genre value
      {
        :conditions => ["uploaded_songs.genre = ?",value]
      }
    end

    def UploadedSong.friend_value user
      friends = []
      user.get_friends.each {|f| friends << [f.name,f.id]}
      [['all friends','all']] + friends
    end

    def UploadedSong.song_language_value user
      ExtractionLayer::list_of_languages
    end

    def UploadedSong.song_genre_value user
      ExtractionLayer::list_of_genres
    end

  end
end

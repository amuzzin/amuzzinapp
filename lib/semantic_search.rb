module SemanticSearch
	def form_semantic_sentence key_words,model,keyword_display_names,values
		properties,owners = get_properties_and_owners(model,key_words)
		properties = properties.sort_by {|k,v| v[:position]}.map {|k,v| get_name(k,v,keyword_display_names,values,"property")}
		owners = owners.map {|k,v| get_name(k,v,keyword_display_names,values,"owner")}
		display_name = model.display_name ? model.display_name.to_s.pluralize : model.to_s.tableize.humanize.downcase
		sentence = "List the #{properties.join(' ')} #{display_name} "
		sentence += "of #{owners.to_sentence} " if owners.present?
		sentence
	end

	def execute_semantic_query key_words,model,params,values
		properties,owners = get_properties_and_owners(model,key_words)
		arr = properties.merge owners
		conditions = []
		order_by = []
		includes = []
		arr.each do |k,v|
			raise MissingKeywordArgument if v[:params] && (params.blank? || params[k].blank?)
			raise MissingKeywordArgument if v[:value] && (values.blank? || values[k].blank?)
			args = []
			args << values[k] if v[:value]
			args << params[k] if v[:params]
			hash = args.present? ? model.send(k,*args) : model.send(k)
			conditions << hash[:conditions] if hash[:conditions]
			order_by << hash[:order] if hash[:order]
			includes << hash[:include] if hash[:include]
		end
		p "#{model}.paginate(:all,:include => '#{includes.join(',')}', :conditions => #{join_conditions(conditions)}, :order => '#{order_by.join(',')}')"
		model.paginate(:page => params[:page],:per_page=>5,:include => "#{includes.join(',')}", :conditions => join_conditions(conditions), :order => "#{order_by.join(',')}")
	end

	private

	def get_properties_and_owners model,key_words
		properties = {}
		owners = {}
		key_words.each do |k|
			prop = model.properties[k]
			own = model.owners[k]
			raise MutuallyExclusiveKeywords if model.has_mutually_exclusive_keywords key_words
			if prop
				properties[k] = prop
			elsif own
				owners[k] = own
			else
				raise NoSuchKeyword
			end
		end
		return properties,owners
	end

	def get_name k,v,keyword_display_names,values,type
		if v[:value]
			raise KeywordValueNotFound if values[k].blank?
      return keyword_display_names[k]
		end
		if type=="property"
			return v[:name].present? ? v[:name].to_s.humanize.downcase : k.to_s.humanize.downcase
		elsif type=="owner"
			return k.to_s.tableize.humanize.downcase
		end
	end

  def join_conditions conditions
    c = []
    args = []
    conditions.each do |a|
      if a.class == Array
        c << a[0]
        args << a[1..a.length-1]
      else
        c << a
      end
    end
    p ([c.join(' and ')] + args).flatten
    p "=========================="
    ([c.join(' and ')] + args).flatten
  end 


	class MutuallyExclusiveKeywords < Exception
    def initialize(msg = "Mutually Exclusive keywords cannot be used in semantic search")
      super(msg)
    end
  end

	class MissingKeywordArgument < Exception
    def initialize(msg = "Keyword expects arguments to be passed")
      super(msg)
    end
  end

  class NoSuchKeyword < Exception
    def initialize(msg = "The model doesn't have the specified keyword")
      super(msg)
    end
  end

  class KeywordValueNotFound < Exception
    def initialize(msg = "The keyword expects a value")
      super(msg)
    end
  end

end
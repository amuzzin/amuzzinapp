module ActiveRecordExtension

  extend ActiveSupport::Concern

  # add your static(class) methods here
  module ClassMethods
    include SemanticSearch

    @@properties = {}
    @@owners = {}
    @@mutually_exclusive = []
    @@display_name = nil

    def has_property name,options={}
      @@properties[name] = options
    end

    def has_owner name,options={}
      @@owners[name] = options
    end

    def set_display_name name
      @@display_name = name
    end

    def mutually_exclusive keywords
      @@mutually_exclusive << keywords
    end

    def owners
      @@owners
    end

    def properties
      @@properties
    end

    def mutually_exclusive_keywords
      @@mutually_exclusive
    end

    def display_name
      @@display_name
    end

    def keywords
      @@properties.merge(@@owners)
    end

    def semantic_search keywords,params={}
      p keywords
      p params[:arguments]
      p params[:values]
      p '==================================================='
      execute_semantic_query keywords,self,params[:arguments],params[:values]
    end

    def semantic_sentence keywords,params={}
      form_semantic_sentence keywords,self,params[:display_names],params[:values]
    end

    def has_mutually_exclusive_keywords key_words
      mutually_exclusive_keywords.each do |a|
        return true if (a & key_words).length > 1
      end
      false
    end
  end
end

# include the extension
ActiveRecord::Base.send(:include, ActiveRecordExtension)